void ShakerSort(int A[], size_t N)
{
    size_t left = 0, right = N - 1;
    bool swapped = true;

    while (left < right && swapped) {
        swapped = false;
        for (auto i = left; i < right; i++) {
            if (A[i] < A[i+1]) {
                std::swap(A[i], A[i+1]);
                swapped = true;
            }
        }
        right--;
        for (auto i = right; i > left; i--) {
            if (A[i-1] < A[i]) {
                std::swap(A[i-1], A[i]);
                swapped = true;
            }
        }
        left++;
    }
}
