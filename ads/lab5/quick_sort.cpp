#include <iostream>
#include <algorithm>
#include <vector>

#include <ctime>
#include <cstdlib>

void print_vector(const std::vector<int> &array);
void quick_sort(std::vector<int> &array, int left, int right);

int main(void)
{
    std::vector<int> numbers;
    std::srand(std::time(nullptr));

    for (int i = 0; i < 10; i++) {
        numbers.push_back(std::rand() % 100);
    }

    print_vector(numbers);
    quick_sort(numbers, 0, numbers.size() - 1);
    print_vector(numbers);

}

void print_vector(const std::vector<int> &array)
{
    for (auto &x : array) {
        std::cout << x << " ";
    }
    std::cout << '\n';
}

void quick_sort(std::vector<int> &array, int left, int right)
{
    int i = left, j = right;
    int pivot = array[(left + right) / 2]; 

    do {
        while (array[i] < pivot) i++;
        while (array[j] > pivot) j--;

        if (i <= j) {
            std::swap(array[i], array[j]);
            i++; j--;
        }
    } while (i < j);
    
    std::cout << "Pivot: " << pivot << '\n';
    print_vector(array);

    if (j > left) quick_sort(array, left, j);
    if (i < right) quick_sort(array, i, right);
}
