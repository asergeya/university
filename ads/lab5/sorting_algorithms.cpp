#include <iostream>
#include <algorithm>
#include <chrono>
#include <vector>
#include <cstdlib>
#include <ctime>

#define SIZE_UPPER_BOUND 1000000
#define SIZE_HARDCODED 10
#define NUM_UPPER_BOUND 10000

void random_input_vector(std::vector<int> &vec);
void print_vector(const std::vector<int> &vec);
void heapify(std::vector<int> &array, int n, int i);
void heap_sort(std::vector<int> &array);
void stupid_sort(std::vector<int> &array);
void merge_sort(std::vector<int> &array, size_t left, size_t right);
void merge(std::vector<int> &array, size_t left, size_t middle, size_t right);

int main(void)
{
    using namespace std::chrono;
    std::srand(std::time(nullptr));
    std::vector<int> numbers1;
    std::vector<int> numbers2;
    std::vector<int> numbers3;
    steady_clock::time_point start, end;

    random_input_vector(numbers1); 
    numbers3 = numbers2 = numbers1;
    std::cout << "Vector size: " << numbers1.size() << std::endl;

    start = steady_clock::now(); 
    stupid_sort(numbers2);
    end = steady_clock::now(); 
    std::cout << "[Stupid sort] Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    start = steady_clock::now(); 
    merge_sort(numbers1, 0, numbers1.size()-1);
    end = steady_clock::now(); 
    std::cout << "[Merge sort] Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    std::cout << '\n';
    print_vector(numbers3);
    start = steady_clock::now(); 
    heap_sort(numbers3);
    end = steady_clock::now(); 
    std::cout << "[Heap sort] Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;
    print_vector(numbers3);
}

void random_input_vector(std::vector<int> &vec)
{
    int size; // = 100000;
    
    // Generate size of vector
    do {
        size = std::rand() % SIZE_UPPER_BOUND;
    } while (size <= 0);

#ifdef SIZE_HARDCODED
    size = SIZE_HARDCODED;
#endif

    // Fill vector with numbers
    for (int i = 0; i < size; i++) {
        vec.push_back(rand() % NUM_UPPER_BOUND);
    }
}

void print_vector(const std::vector<int> &vec)
{
    for (auto x : vec) {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}

void heapify(std::vector<int> &array, int n, int i)
{
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && array[left] > array[largest]) {
        largest = left;
    }
    if (right < n && array[right] > array[largest]) {
        largest = right;
    }
    if (largest != i) {
        std::swap(array[i], array[largest]);
        heapify(array, n, largest);
    }
}

void heap_sort(std::vector<int> &array)
{
    auto n = array.size();

    for (int i = n / 2 - 1; i >= 0; i--) {
        heapify(array, n, i);
    }
    for (int i = n - 1; i > 0; i--) {
        std::swap(array[0], array[i]);
        heapify(array, i, 0);
    }
}

void stupid_sort(std::vector<int> &array) 
{
    for (int i = 0; i < array.size() - 1; i++) {
        if (array[i] > array[i+1]) {
            std::swap(array[i], array[i+1]);
            i = -1;
        }
    }
}

void merge_sort(std::vector<int> &array, size_t left, size_t right)
{
    if (left >= right) {
        return;
    }
    size_t middle = left + (right - left) / 2;

    merge_sort(array, left, middle);
    merge_sort(array, middle + 1, right);
    merge(array, left, middle, right);

}

void merge(std::vector<int> &array, size_t left, size_t middle, size_t right)
{
    size_t left_size = middle - left + 1;
    size_t right_size = right - middle;
    std::vector<int> left_array, right_array; 

    for (size_t i = 0; i < left_size; i++) {
        left_array.push_back(array[left + i]);
    } 
    for (size_t i = 0; i < right_size; i++) {
        right_array.push_back(array[middle + 1 + i]);
    } 

    size_t left_index, right_index, merged_index; 
    left_index = right_index = 0;
    merged_index = left;
    while (left_index < left_size && right_index < right_size) {
        if (left_array[left_index] <= right_array[right_index]) {
            array[merged_index++] = left_array[left_index++];
        }
        else {
            array[merged_index++] = right_array[right_index++];
        }
    }
    while (left_index < left_size) {
        array[merged_index++] = left_array[left_index++];
    }
    while (right_index < right_size) {
        array[merged_index++] = right_array[right_index++];
    }
}
