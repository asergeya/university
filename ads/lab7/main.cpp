#include <iostream>
#include <fstream>
#include <climits>
#include <algorithm>

int main(void)
{
    std::ifstream in("adjacency_matrix.txt");
    int n;

    in >> n;
    int **adj = new int * [n];
    int **distance = new int * [n];
    for (int i = 0; i < n; i++) {
        adj[i] = new int[n];
        distance[i] = new int[n];
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            in >> adj[i][j]; 
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if (i == j) {
                distance[i][j] = 0;
            }
            else if (adj[i][j]) {
                distance[i][j] = adj[i][j];
            }
            else {
                distance[i][j] = 1000;
            }
        }
    }
    for (int k = 0; k < n; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                distance[i][j] = std::min(distance[i][j],
                                          distance[i][k] + distance[k][j]);
            }
        }
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            std::cout << distance[i][j] << " ";
        }
        std::cout << '\n';
    }

    int start, finish;
    std::cout << "Enter number of start node:\n";
    do {
        std::cin >> start;
    } while (start < 1);
    std::cout << "Enter number of finish node:\n";
    do {
        std::cin >> finish;
    } while (finish < 1 || finish > n);
    
    std::cout << "The longest path is " << distance[start-1][finish-1] << '\n';
}
