#include <fstream>
#include <iostream>
#include <string>
#include <chrono>
#include <cstdlib>
#include <ctime>

#define LEFT_FILE "left.txt"
#define RIGHT_FILE "right.txt"
#define DEFAULT_FILE "numbers.txt"
#define SEPARATOR '\n'
#define NUM_UPPER_BOUND 10000
#define SIZE 1000

size_t GetLength(std::string filename);
void Divide(std::string source_file, size_t group_size);
void Merge(std::string target_file, size_t group_size);
void ExternalSort(std::string filename);
int GetByIndex(std::string filename, size_t index);
void GenerateListOfNumbers(std::string filename, size_t size);

int main(int argc, char *argv[])
{
    using namespace std::chrono;
    std::srand(std::time(nullptr));
    steady_clock::time_point start, end;
    
    GenerateListOfNumbers("input.txt", SIZE);

    if (argc == 1) {
        ExternalSort(DEFAULT_FILE);
        long long index;
        do {
            std::cout << "Enter index:\n";
            std::cin >> index;
            std::cout << GetByIndex(DEFAULT_FILE, index) << '\n';
        } while (index >= 0);
    }
    else if (argc == 2) {
        start = steady_clock::now(); 
        ExternalSort(argv[1]);
        end = steady_clock::now(); 
        std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;
    }
}


size_t GetLength(std::string filename)
{
    std::ifstream in(filename, std::ios::in);

    if (!in.is_open()) {
        std::cerr << "Can't open: " << filename << '\n';
        exit(1);
    }

    int tmp;
    size_t length = 0;
    while (in >> tmp) {
        length++;
    }
    in.close();

    return length;
}

void Divide(std::string source_file, size_t group_size)
{
    std::ifstream source(source_file);
    std::ofstream left(LEFT_FILE);
    std::ofstream right(RIGHT_FILE);
    int tmp;

    if (!source.eof()) {
        source >> tmp;
    }
    while (!source.eof()) {
        for (size_t i = 0; i < group_size && !source.eof(); i++) {
            left << tmp << SEPARATOR;
            source >> tmp;
        }
        for (size_t i = 0; i < group_size && !source.eof(); i++) {
            right << tmp << SEPARATOR;
            source >> tmp;
        }
    }
    source.close();
    left.close();
    right.close();
}

void Merge(std::string target_file, size_t group_size)
{
    std::ofstream target(target_file);
    std::ifstream left(LEFT_FILE);
    std::ifstream right(RIGHT_FILE);
    int first, second;

    if (!left.eof()) {
        left >> first;
    }
    if (!right.eof()) {
        right >> second;
    }
    while (!left.eof() && !right.eof()) {
        size_t l1 = 0, l2 = 0;

        while (l1 < group_size && !left.eof() &&
               l2 < group_size && !right.eof()) {

            if (first < second) {
                target << first << SEPARATOR;
                left >> first;
                l1++;
            } 
            else {
                target << second << SEPARATOR;
                right >> second;
                l2++;
            }
        }
        while (l1 < group_size && !left.eof()) {
            target << first << SEPARATOR;
            left >> first;
            l1++;
        }
        while (l2 < group_size && !right.eof()) {
            target << second << SEPARATOR;
            right >> second;
            l2++;
        }
    }
    while (!left.eof()) {
        target << first << SEPARATOR;
        left >> first;
    }
    while (!right.eof()) {
        target << second << SEPARATOR;
        right >> second;
    }
    target.close();
    left.close();
    right.close();
}

void ExternalSort(std::string filename)
{
    auto length = GetLength(filename);

    for (size_t i = 1; i < length; i *= 2) {
        Divide(filename, i);
        Merge(filename, i);
    }
}

int GetByIndex(std::string filename, size_t index)
{
    std::ifstream in(filename, std::ios::in);

    if (!in.is_open()) {
        std::cerr << "Can't open: " << filename << '\n';
        exit(1);
    }

    int tmp;
    size_t i;
    for (i = 0; !in.eof() && i <= index; i++) {
        in >> tmp;
    }
    in.close();

    if ((i - 1) == index) {
        return tmp;
    } 
    return -1;
}

void GenerateListOfNumbers(std::string filename, size_t size)
{
    std::ofstream out(filename);

    for (int i = 0; i < size; i++) {
        out << rand() % NUM_UPPER_BOUND << '\n';
    }
}
