#include "stack.h"

Stack::Stack()
{
    head = nullptr;
    stack_size = 0;
}

Stack::Stack(const Stack &stack)
{
    Node *current = stack.head;

    head = nullptr;
    stack_size = stack.stack_size;
    while (current != nullptr) {
        Node *newnode = new Node;
        Node *temp;

        newnode->next = nullptr;
        newnode->data = current->data;

        if (head == nullptr) {
            head = newnode;
            temp = head;
        }
        else {
            temp->next = newnode; 
            temp = temp->next;
        }
        current = current->next; 
    }  
}

void Stack::push(int data)
{
    Node *newnode = new Node; 

    newnode->next = head;
    newnode->data = data;

    head = newnode;
    stack_size++;
}

void Stack::pop(void)
{
    Node *temp = head;
    
    head = head->next;
    delete temp;
    stack_size--;
}

int Stack::top(void)
{
    if (stack_size != 0) {
        return head->data;
    }
}

std::size_t Stack::size(void)
{
    return stack_size;
}

bool Stack::empty(void)
{
    if (stack_size == 0) {
        return true;
    }
    return false;
}

Stack::~Stack()
{
    while (head != nullptr) {
        Node *temp = head;

        head = head->next; 
        delete temp;
        stack_size--;
    }  
}
