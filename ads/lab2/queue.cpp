#include <iostream>
#include <chrono>
#include <queue>
#include <ctime>
#include <cstdlib>

#define START 0
#define STOP 1000000000
#define STEP 11

void print_queue(std::queue<int> queue);
void generate_arithmetic_sequence_with_mistake(std::queue<int> &sequence, int start, int stop, int step);
std::queue<int> fixed_arithmetic_sequence(std::queue<int> sequence);
int find_missed_term(std::queue<int> sequence, int step);

int main(void)
{
    using namespace std::chrono;

    std::srand(std::time(nullptr));
    steady_clock::time_point start, end;
    std::queue<int> numbers;

    generate_arithmetic_sequence_with_mistake(numbers, START, STOP, STEP);
    //print_queue(numbers);
    std::cout << "Size: " << numbers.size() << std::endl;

    start = steady_clock::now(); 
    fixed_arithmetic_sequence(numbers);
    //print_queue(fixed_arithmetic_sequence(numbers));
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    return 0;
}

void print_queue(std::queue<int> queue)
{
    std::cout << "Size: " << queue.size() << std::endl;
    while (!queue.empty()) {
        std::cout << queue.front() << " ";
        queue.pop();
    }
    std::cout << std::endl;
}

void generate_arithmetic_sequence_with_mistake(std::queue<int> &sequence, int start, int stop, int step)
{
    // There find out last term, because possible situation
    // when stop is not last term, so 
    // last_term = stop - redundant part
    // redundant_part = remainder of (stop - start) divided by step
    // Remainder = (stop - start) % step
    int last_term = stop - (stop - start) % step;
    // Number of terms in arithmetic sequence is 
    // (last_term - first_term) / difference + 1 
    // I need missed term in the middle of sequence, so I need to exclude 
    // first term and last term from missing.
    int number = (last_term - start) / step - 1;
    int missed_term = start + (std::rand() % number + 1) * step;

    std::cout << "Arithmetic sequence generator: missing " << missed_term << std::endl;

    for (int x = start; x <= stop; x += step) {
        if (x == missed_term) {
            continue;
        }
        sequence.push(x);
    }
}

std::queue<int> fixed_arithmetic_sequence(std::queue<int> sequence_with_mistake)
{
    int first_term = sequence_with_mistake.front();
    int last_term = sequence_with_mistake.back();
    int step = (last_term - first_term) / sequence_with_mistake.size();
    int missed_term = find_missed_term(sequence_with_mistake, step);

    std::cout << "Found missed term: " << missed_term << std::endl;

    std::queue<int> fixed_sequence; 
    for (int x = first_term; x <= last_term; x += step) {
        fixed_sequence.push(x); 
    }
    return fixed_sequence;
}

// TODO: rewrite
int find_missed_term(std::queue<int> sequence, int step)
{
    int popped;

    while (!sequence.empty()) {
        popped = sequence.front();
        sequence.pop();
        if (sequence.front() - popped > step) {
            return popped + step;
        }
    }
}
