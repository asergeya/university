#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include <cstddef>

struct Node {
    int data;
    Node *next = nullptr;
};

class Stack {
private:
    Node *head;
    std::size_t stack_size;
public:
    Stack();
    Stack(const Stack &stack);
    void push(int data);
    void pop(void);
    int top(void);
    std::size_t size(void);
    bool empty(void);
    ~Stack();
};

#endif
