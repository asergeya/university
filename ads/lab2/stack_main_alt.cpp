#include <iostream>
#include <chrono>
#include <ctime>
#include <cstdlib>

#include "stack.h"

#define START 0
#define STOP 1000
#define STEP 11

void print_stack(Stack stack);
void generate_arithmetic_sequence_with_mistake(Stack &sequence, int start, int stop, int step);
Stack fixed_arithmetic_sequence(Stack sequence_with_mistake);
int find_step(Stack sequence_with_mistake);

int main(void)
{
    using namespace std::chrono;

    std::srand(std::time(nullptr));
    steady_clock::time_point start, end;
    Stack numbers;

    generate_arithmetic_sequence_with_mistake(numbers, START, STOP, STEP);

    start = steady_clock::now(); 
    fixed_arithmetic_sequence(numbers);
    //print_stack(fixed_arithmetic_sequence(numbers));
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    return 0;
}

void print_stack(Stack stack)
{
    std::cout << "Size: " << stack.size() << std::endl;
    while (!stack.empty()) {
        std::cout << stack.top() << " ";
        stack.pop();
    }
    std::cout << std::endl;
}


void generate_arithmetic_sequence_with_mistake(Stack &sequence, int start, int stop, int step)
{
    // There find out last term, because possible situation
    // when stop is not last term, so 
    // last_term = stop - redundant part
    // redundant_part = remainder of (stop - start) divided by step
    // Remainder = (stop - start) % step
    int last_term = stop - (stop - start) % step;
    // Number of terms in arithmetic sequence is 
    // (last_term - first_term) / difference + 1 
    // I need missed term in the middle of sequence, so I need to exclude 
    // first term and last term from missing.
    int number = (last_term - start) / step - 1;
    int missed_term = start + (std::rand() % number + 1) * step;

    std::cout << "Arithmetic sequence generator: missing " << missed_term << std::endl;

    for (int x = start; x <= stop; x += step) {
        if (x == missed_term) {
            continue;
        }
        sequence.push(x);
    }
}

Stack fixed_arithmetic_sequence(Stack sequence_with_mistake)
{
    int step;
    int popped, top;
    Stack fixed_sequence; 

    popped = sequence_with_mistake.top();
    sequence_with_mistake.pop();
    fixed_sequence.push(popped);
    step = popped - sequence_with_mistake.top();

    while (sequence_with_mistake.size() > 1) {
        popped = sequence_with_mistake.top();
        sequence_with_mistake.pop();
        top = sequence_with_mistake.top();
        if (popped - top > step) {
            std::cout << "Found missed term: " << top + step << std::endl;
            fixed_sequence.push(top + step);
        }
        else if (popped - top < step) {
            step = popped - top;
            std::cout << "Found missed term: " << popped + step << std::endl;
            fixed_sequence.push(popped + step);
        }
        fixed_sequence.push(popped);
    }
    fixed_sequence.push(top);

    return fixed_sequence;
}

int find_step(Stack sequence_with_mistake)
{
    int popped, top;
    int step;

    step = popped = sequence_with_mistake.top();
    sequence_with_mistake.pop();
    while (!sequence_with_mistake.empty()) {
        top = sequence_with_mistake.top();
        if (popped - top < step) {
            step = popped - top;
        }
        popped = top;
        sequence_with_mistake.pop();
    }
    return step;
}
