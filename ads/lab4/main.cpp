#include <iostream>
#include <fstream>
#include <chrono>
#include "HashTable.h"

HashTable ReadHashTableFromFile(std::ifstream &file);
 
int main(void)
{
    using namespace std::chrono;
    steady_clock::time_point start, end;
    std::ifstream fin("input.txt");
    std::string key;
    int value;
    HashTable table = ReadHashTableFromFile(fin);
/*
    start = steady_clock::now(); 
    table.GetElement("One");
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<nanoseconds>(end - start).count() << " ns" << std::endl;

    start = steady_clock::now(); 
    table.GetElement("Nine");
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<nanoseconds>(end - start).count() << " ns" << std::endl;

    start = steady_clock::now(); 
    table.GetElement("One");
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<nanoseconds>(end - start).count() << " ns" << std::endl;

    start = steady_clock::now(); 
    table.GetElement("Ten");
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<nanoseconds>(end - start).count() << " ns" << std::endl;
*/
///* Test    
    std::cout << "Before modification\n";
    table.Print(std::cout);

    std::cout << "Adding elemtns\n";
    do {
        std::getline(std::cin, key);
        std::cin >> value;
        std::cin.ignore();
        std::cout << table.AddElement(key, value) << '\n';
        table.Print(std::cout);
    } while (key != "No");

    std::cout << "Getting elements\n";
    do {
        std::getline(std::cin, key);
        std::cout << table.GetElement(key) << '\n';
    } while (key != "No");

    std::cout << "Removing elements\n";
    do {
        std::getline(std::cin, key);
        if (!table.RemoveElement(key)) {
            std::cout << "Can't remove " << key << '\n';
        }
    } while (key != "No");

    std::cout << "After modification\n";
    table.Print(std::cout);

    std::cout << "Getting elements\n";
    do {
        std::getline(std::cin, key);
        std::cout << table.GetElement(key) << '\n';
    } while (key != "No");
//*/
}

HashTable ReadHashTableFromFile(std::ifstream &file)
{
    size_t n;

    file >> n;
    HashTable table(n);

    for (size_t i = 0; i < n; i++) {
        std::string key;
        int value;
        
        file.ignore();
        std::getline(file, key);
        //file >> key;
        file >> value;
        if (!table.AddElement(key, value)) {
            std::clog << "Can't add (" << key << ", " << value << ")\n";
        }
    }
    return table;
}
