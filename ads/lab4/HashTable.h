#ifndef HashTable_H_INCLUDED
#define HashTable_H_INCLUDED

#include <ostream>
#include <string>
#include <vector>
#include <list>

struct HashItem {
    std::string key;
    int value;

    HashItem(std::string key, int value)
    {
        this->key = key;
        this->value = value;
    }
    void Print(std::ostream &out) const
    {
        out << "\"" << key << "\"" << "=>" << value;
    }
};

class HashTable {
public:
    HashTable(size_t capacity);
    bool AddElement(const std::string &key, const int &value);
    bool RemoveElement(const std::string &key);
    int GetElement(const std::string &key) const;
    void Print(std::ostream &out) const;
    ~HashTable();
private:
    std::list<HashItem> *hash_table;
    std::vector<bool> used;
    size_t size;
    size_t capacity;

    size_t GetHash(const std::string &key) const;
    bool AddElementWithFixingCollision(const HashItem &new_node);
    bool RemoveElementWithFixingCollision(const std::string &key);
    int GetElementWithFixingCollision(const std::string &key) const;
};

#endif
