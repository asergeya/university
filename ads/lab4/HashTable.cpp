#include "HashTable.h"
#include "md5.h"

HashTable::HashTable(size_t capacity) 
{
    this->size = 0;
    this->capacity = capacity;
    this->hash_table = new std::list<HashItem>[capacity];
    this->used.reserve(capacity);

    for (size_t i = 0; i < capacity; i++) {
        this->used[i] = false;
    }
}

bool HashTable::AddElement(const std::string &key, const int &value)
{
    size_t index = GetHash(key);
    HashItem new_node(key, value);

    if (used[index]) {
        return AddElementWithFixingCollision(new_node);
    } 

    hash_table[index].push_back(new_node);
    used[index] = true;
    size++;

    return true;
}

bool HashTable::RemoveElement(const std::string &key)
{
    size_t index = GetHash(key);
    auto &chain = hash_table[index];

    if (used[index] && chain.front().key == key) {
        chain.pop_front();
        used[index] = false;
        size--;
        return true;
    }
    if (used[index]) {
        return RemoveElementWithFixingCollision(key);
    } 
    return false;
}

int HashTable::GetElement(const std::string &key) const
{
    size_t index = GetHash(key);
    auto &chain = hash_table[index];

    if (used[index] && chain.front().key != key) {
        return GetElementWithFixingCollision(key);
    } 
    if (used[index]) {
        return chain.front().value;
    }
    //throw "Not found!";
    return -1;
}

void HashTable::Print(std::ostream &out) const
{
    auto items_counter = size;
    out << "{";
    for (size_t i = 0; i < capacity; i++) {
        if (used[i]) {
            auto &chain = hash_table[i];

            for (const auto &item : chain) {
                item.Print(out);
                items_counter--;
                if (items_counter > 0) {
                    out << ", ";
                }
            }
        }
    }
    out << "}\n";
}

size_t HashTable::GetHash(const std::string &key) const
{
    std::string md5hash = md5(key);
    size_t sum = 0;

    for (auto letter : md5hash) {
        sum += letter;
    }

    return sum % capacity;
}

HashTable::~HashTable()
{
    delete[] hash_table;
}

bool HashTable::AddElementWithFixingCollision(const HashItem &new_node)
{
    auto &chain = hash_table[GetHash(new_node.key)];

    for (auto &item : chain) {
        if (item.key == new_node.key) {
            item.value = new_node.value; 
            return true;
        }
    }
    chain.push_back(new_node);
    size++;
    return true;
}

bool HashTable::RemoveElementWithFixingCollision(const std::string &key)
{
    auto &chain = hash_table[GetHash(key)];

    for (auto it = chain.begin(); it != chain.end(); it++) {
        if (it->key == key) {
            chain.erase(it);
            size--;
            return true;
        }
    }
    return false;
}

int HashTable::GetElementWithFixingCollision(const std::string &key) const
{
    auto &chain = hash_table[GetHash(key)];

    for (const auto &item : chain) {
        if (item.key == key) {
            return item.value;
        }
    }
    //throw "Not found!";
    return -1;
}
