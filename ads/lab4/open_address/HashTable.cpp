#include "HashTable.h"
#include "md5.h"

HashTable::HashTable(size_t capacity) 
{
    this->capacity = capacity;
    this->keys.reserve(capacity);
    this->values.reserve(capacity);
    this->used.reserve(capacity);

    for (size_t i = 0; i < capacity; i++) {
        this->used[i] = false;
    }
}

bool HashTable::AddElement(const std::string &key, const int &value)
{
    size_t index = GetHash(key);

    if (used[index] && keys[index] != key) {
        return AddElementWithFixingCollision(key, value);
    } 
    keys[index] = key;
    values[index] = value;
    used[index] = true;

    return true;
}

bool HashTable::RemoveElement(const std::string &key)
{
    size_t index = GetHash(key);

    if (used[index] && keys[index] != key) {
        return RemoveElementWithFixingCollision(key);
    } 
    if (used[index] && keys[index] == key) {
        used[index] = false;
        return true;
    }
    return false;
}

int HashTable::GetElement(const std::string &key) const
{
    size_t index = GetHash(key);

    if (used[index] && keys[index] != key) {
        return GetElementWithFixingCollision(key);
    } 
    if (used[index] && keys[index] == key) {
        return values[index];
    }
    return -1;
}

void HashTable::Print(std::ostream &out) const
{
    size_t element_counter = 0;
    for (size_t i = 0; i < capacity; i++) {
        if (used[i]) {
            element_counter++;
        }
    }
    out << "{";
    for (size_t i = 0; i < capacity; i++) {
        if (used[i]) {
            out << "\"" << keys[i] << "\"" << "=>" << values[i] << ((element_counter == 1) ? "" : ", ");
            element_counter--;
        }
    }
    out << "}\n";
}

size_t HashTable::GetHash(const std::string &key) const
{
    std::string md5hash = md5(key);
    size_t sum = 0;

    for (auto letter : md5hash) {
        sum += letter;
    }

    return sum % capacity;
}

bool HashTable::AddElementWithFixingCollision(const std::string &key, const int &value)
{
    size_t index = GetHash(key);

    while (index < capacity && used[index]) {
        index++; 
    }
    if (index == capacity) {
        return false;
    }
    keys[index] = key;
    values[index] = value;
    used[index] = true;

    return true;
}

bool HashTable::RemoveElementWithFixingCollision(const std::string &key)
{
    size_t index = GetHash(key);

    while (index < capacity && keys[index] != key) {
        index++; 
    }
    if (index == capacity) {
        return false;
    }
    used[index] = false;

    return true;
}

int HashTable::GetElementWithFixingCollision(const std::string &key) const
{
    size_t index = GetHash(key);

    while (index < capacity && keys[index] != key) {
        index++; 
    }
    if (index == capacity) {
        return -1;
    }
    if (used[index]) {
        return values[index];
    }
    return -1;
}
