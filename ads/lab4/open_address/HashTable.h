#ifndef HashTable_H_INCLUDED
#define HashTable_H_INCLUDED

#include <ostream>
#include <string>
#include <vector>

class HashTable {
public:
    HashTable(size_t capacity);
    bool AddElement(const std::string &key, const int &value);
    bool RemoveElement(const std::string &key);
    int GetElement(const std::string &key) const;
    void Print(std::ostream &out) const;
private:
    std::vector<std::string> keys;
    std::vector<int> values;
    std::vector<bool> used;
    size_t capacity;

    size_t GetHash(const std::string &key) const;
    bool AddElementWithFixingCollision(const std::string &key, const int &value);
    bool RemoveElementWithFixingCollision(const std::string &key);
    int GetElementWithFixingCollision(const std::string &key) const;
};

#endif
