#include <iostream>
#include <algorithm>
#include <vector>
#include <chrono>
#include <climits>
#include <cstdlib>

#define SIZE_UPPER_BOUND 10
#define NUM_UPPER_BOUND 1000001
#define NUM 2021

int find_min_greater_than_n_fast(const std::vector<int> &numbers, int n);
int find_min_greater_than_n_medium(std::vector<int> &numbers, int n);
int find_min_greater_than_n_slow(std::vector<int> &numbers, int n);
void bubble_sort(std::vector<int> &numbers);
void manual_input_vector(std::vector<int> &vec);
void random_input_vector(std::vector<int> &vec);
void print_vector(const std::vector<int> &vec);

int main(void)
{
    using namespace std::chrono;

    std::srand(std::time(nullptr));
    std::vector<int> numbers;
    steady_clock::time_point start, end;

    random_input_vector(numbers); 
    std::cout << "Vector size: " << numbers.size() << std::endl;

    start = steady_clock::now(); 
    std::cout << find_min_greater_than_n_slow(numbers, NUM) << std::endl;
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    start = steady_clock::now(); 
    std::cout << find_min_greater_than_n_medium(numbers, NUM) << std::endl;
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    start = steady_clock::now(); 
    std::cout << find_min_greater_than_n_fast(numbers, NUM) << std::endl;
    end = steady_clock::now(); 
    std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    return 0;
}

// O(n)
int find_min_greater_than_n_fast(const std::vector<int> &numbers, int n)
{
    int min = INT_MAX;

    for (auto x : numbers) {
        if (x > n and x < min) {
            min = x;
        }
    }
    if (min == n) {
        return -1;
    }
    return min;
}

// O(nlogn)
int find_min_greater_than_n_medium(std::vector<int> &numbers, int n)
{
    std::sort(numbers.begin(), numbers.end());
    for (auto x : numbers) 
    {
        if (x > n) {
            return x;
        }
    }
    return -1;
}

// O(n2)
int find_min_greater_than_n_slow(std::vector<int> &numbers, int n)
{
    bubble_sort(numbers);
    print_vector(numbers);
    for (auto x : numbers) 
    {
        if (x > n) {
            return x;
        }
    }
    return -1;
}

void bubble_sort(std::vector<int> &numbers)
{
    int num_of_swaps = -1;
    int sorted_count = 0;

    while (num_of_swaps != 0) {
        num_of_swaps = 0;
        for (int i = 0; i < numbers.size() - sorted_count - 1; i++) {
            if (numbers[i] > numbers[i+1]) {
                std::swap(numbers[i], numbers[i+1]);
                num_of_swaps++;
            }
        }
        sorted_count++;
    }
}

void manual_input_vector(std::vector<int> &vec)
{
    int temp;

    std::cout << "Enter some numbers (Ctrl+D to end input):" << std::endl;
    while (std::cin >> temp) {
        vec.push_back(temp);
    }
}

void random_input_vector(std::vector<int> &vec)
{
    int size; // = 100000;
    
    // Generate size of vector
    do {
        size = std::rand() % SIZE_UPPER_BOUND;
    } while (size <= 0);

    // Fill vector with numbers
    for (int i = 0; i < size; i++) {
        vec.push_back(rand() % NUM_UPPER_BOUND);
    }
}

void print_vector(const std::vector<int> &vec)
{
    for (auto x : vec) {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}
