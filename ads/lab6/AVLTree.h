#ifndef AVLTree_H_INCLUDED
#define AVLTree_H_INCLUDED

#include <ostream>
#include <algorithm>

class AVLTree {
private:
    struct Node {
        double data;
        int height = 1;
        Node *left = nullptr;
        Node *right = nullptr;

        Node(double data)
        {
            this->data = data;
        }
    };
    Node *root;
    size_t size;
   
    Node * RotateLeft(Node *node);
    Node * RotateRight(Node *node);
    Node * Balance(Node *node);
    Node * InsertHelper(Node *node, double data);
    int GetHeight(Node *node) const;
    void FixHeight(Node *node);
    int GetBalance(Node *node) const;
    void PrintHelper(Node *node, std::ostream &out, int count) const;
    double GetSumHelper(Node *node) const;
    void Delete(Node *node);
    Node * FindMin(Node *node) const;
    Node * RemoveMin(Node *node);
    Node * RemoveHelper(Node *node, double key);
    Node * GetHelper(Node *node, double key) const;
public:
    AVLTree();
    bool IsEmpty(void) const;
    void Insert(double data);
    void Remove(double key);
    double Get(double key);
    void Print(std::ostream &out) const;
    double GetSum(void) const;
    double GetAverage(void) const;
    void AddNodesByTask(int number);
    ~AVLTree();
};

#endif
