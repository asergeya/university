#include "AVLTree.h"

//Private methods
AVLTree::Node * AVLTree::RotateLeft(Node *node)
{
    Node *p = node->right;
    node->right = p->left;
    p->left = node;
    FixHeight(node);
    FixHeight(p);

    return p;
}

AVLTree::Node * AVLTree::RotateRight(Node *node)
{
    Node *p = node->left;
    node->left = p->right;
    p->right = node;
    FixHeight(node);
    FixHeight(p);

    return p;
}

AVLTree::Node * AVLTree::Balance(Node *node)
{
    FixHeight(node);
    if (GetBalance(node) == 2) {
        if (GetBalance(node->right) < 0) {
            node->right = RotateRight(node->right);
        }
        return RotateLeft(node);
    }
    if (GetBalance(node) == -2) {
        if (GetBalance(node->left) > 0) {
            node->left = RotateLeft(node->left);
        }
        return RotateRight(node);
    }
    return node;
}

AVLTree::Node * AVLTree::InsertHelper(Node *node, double data)
{
    if (node == nullptr) {
        return new Node(data);
    }
    if (data < node->data) {
        node->left = InsertHelper(node->left, data);
    }
    else {
        node->right = InsertHelper(node->right, data);
    }
    return Balance(node);
}

int AVLTree::GetHeight(Node *node) const
{
    return (node)? node->height : 0;
}

void AVLTree::FixHeight(Node *node)
{
    auto height_left = GetHeight(node->left);
    auto height_right = GetHeight(node->right);

    if (height_left > height_right) {
        node->height = height_left + 1;
    }
    else {
        node->height = height_right + 1;
    }
}

int AVLTree::GetBalance(Node *node) const
{
    return GetHeight(node->right) - GetHeight(node->left);
}

void AVLTree::PrintHelper(Node *node, std::ostream &out, int count) const
{
    if (node == nullptr) {
        return;
    }

    PrintHelper(node->left, out, count + 1);
    for (auto i = 0; i < count; i++) {
        out << "   ";
    }
    out << node->data << '\n';
    PrintHelper(node->right, out, count + 1);
}

double AVLTree::GetSumHelper(Node *node) const
{
    if (node == nullptr) {
        return 0;
    }
    return node->data + GetSumHelper(node->left) + GetSumHelper(node->right);
}

void AVLTree::Delete(Node *node)
{
    if (node == nullptr) {
        return;
    }
    Delete(node->left);
    Delete(node->right);

    delete node;
}

AVLTree::Node * AVLTree::FindMin(Node *node) const
{
	return (node->left)? FindMin(node->left) : node;
}
AVLTree::Node * AVLTree::RemoveMin(Node *node)
{
	if (node->left == nullptr) {
        return node->right;
    }
	node->left = RemoveMin(node->left);

	return Balance(node);
}

AVLTree::Node * AVLTree::RemoveHelper(Node *node, double key)
{
    if (node == nullptr) {
        return node;
    }
	if (key < node->data) {
		node->left = RemoveHelper(node->left, key);
    }
	else if (key > node->data) {
		node->right = RemoveHelper(node->right, key);	
    }
	else {
		Node *left = node->left;
		Node *right = node->right;

		delete node;
		if (right == nullptr) {
            return left;
        }
		Node *min = FindMin(right);
		min->right = RemoveMin(right);
		min->left = left;

		return Balance(min);
	}
	return Balance(node);
}

AVLTree::Node * AVLTree::GetHelper(Node *node, double key) const
{
    if (node == nullptr || node->data == key) {
        return node;
    }
    if (node->data < key) {
        return GetHelper(node->right, key);
    }
    return GetHelper(node->left, key);
}

//Public methods
AVLTree::AVLTree() 
{
    root = nullptr;
    size = 0;
}

bool AVLTree::IsEmpty(void) const
{
    return root == nullptr; 
}

void AVLTree::Insert(double data)
{
    root = InsertHelper(root, data);
    size++;
}

void AVLTree::Remove(double key)
{
    root = RemoveHelper(root, key);
    size--;
}

double AVLTree::Get(double key)
{
    Node *found = GetHelper(root, key);

    if (found) {
        return found->data;
    }
    return -1;
}

void AVLTree::Print(std::ostream &out) const
{
    out << "Num of elements: " << size << '\n';
    PrintHelper(root, out, 0);
    out << '\n';
}

double AVLTree::GetSum(void) const
{
    return GetSumHelper(root);
}

double AVLTree::GetAverage(void) const
{
    return GetSum() / size;
}

void AVLTree::AddNodesByTask(int number)
{
    auto average = GetAverage();

    for (auto i = 1; i <= number; i++) {
        Insert((i * average) / number);
    }
}

AVLTree::~AVLTree()
{
    Delete(root);
}
