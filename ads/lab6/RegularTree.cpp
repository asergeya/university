#include "RegularTree.h"

RegularTree::RegularTree()
{
    this->root = nullptr;
    this->size = 0;
}

bool RegularTree::IsEmpty(void) const
{
    return this->root == nullptr;
}

void RegularTree::Insert(double data)
{
    Node *new_node = new Node;

    new_node->data = data;
    if (IsEmpty()) {
        root = new_node;
    }
    else {
        Node *current = root;
        Node *parent;

        while (current != nullptr) {
            parent = current;
            if (data < current->data) {
                current = current->left;
            }
            else {
                current = current->right;
            }
        }
        if (data < parent->data) {
            parent->left = new_node;
        }
        else {
            parent->right = new_node;
        }
    }
    size++;
}

void RegularTree::Remove(double key)
{
    root = RemoveHelper(root, key);
    size--;
}

double RegularTree::Get(double key)
{
    Node *found = GetHelper(root, key);

    if (found) {
        return found->data;
    }
    return -1;
}

void RegularTree::Print(std::ostream &out) const
{
    PrintRecursive(root, out, 0);
    out << '\n';
}

double RegularTree::GetAverage(void) const
{
    return GetSum() / size;
}

double RegularTree::GetSum(void) const
{
    return GetSumRecursive(root);
}

void RegularTree::AddNodesByTask(int number)
{
    auto average = GetAverage();

    for (auto i = 1; i <= number; i++) {
        Insert((i * average) / number);
    }
}

RegularTree::~RegularTree()
{
    Delete(root);
}

void RegularTree::PrintRecursive(Node *node, std::ostream &out, int count) const
{
    if (node == nullptr) {
        return;
    }

    PrintRecursive(node->left, out, count + 1);
    for (auto i = 0; i < count; i++) {
        out << "   ";
    }
    out << node->data << '\n';
    PrintRecursive(node->right, out, count + 1);
}

double RegularTree::GetSumRecursive(Node *node) const
{
    if (node == nullptr) {
        return 0;
    }
    return node->data + GetSumRecursive(node->left) + GetSumRecursive(node->right);
}

RegularTree::Node * RegularTree::FindMin(Node *node) const
{
	return (node->left)? FindMin(node->left) : node;
}

RegularTree::Node * RegularTree::RemoveHelper(Node *node, double key)
{
    if (node == nullptr) {
        return node;
    }
    if (key < node->data) {
        node->left = RemoveHelper(node->left, key);
    }
    else if (key > node->data) {
        node->right = RemoveHelper(node->right, key);
    }
    else {
        if (node->left == nullptr && node->right == nullptr) {
            return nullptr;
        }
        else if (node->left == nullptr) {
            Node *temp = node->right;
            delete node;

            return temp;
        }
        else if (node->right == nullptr){
            Node *temp = node->left;
            delete node;

            return temp;
        }
        Node *temp = FindMin(node->right);

        node->data = temp->data;
        node->right = RemoveHelper(node->right, temp->data);
    }
    return node;
}

RegularTree::Node * RegularTree::GetHelper(Node *node, double key) const
{
    if (node == nullptr || node->data == key) {
        return node;
    }
    if (node->data < key) {
        return GetHelper(node->right, key);
    }
    return GetHelper(node->left, key);
}

void RegularTree::Delete(Node *node)
{
    if (node == nullptr) {
        return;
    }
    Delete(node->left);
    Delete(node->right);

    delete node;
}
