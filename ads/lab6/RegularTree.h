#ifndef RegularTree_H_INCLUDED
#define RegularTree_H_INCLUDED

#include <ostream>

class RegularTree {
private:
    struct Node {
        double data;
        Node *left = nullptr;
        Node *right = nullptr;
    };
    Node *root;
    size_t size;

    void PrintRecursive(Node *node, std::ostream &out, int count) const; 
    double GetSumRecursive(Node *node) const;
    Node * FindMin(Node *node) const;
    Node * RemoveHelper(Node *node, double key);
    Node * GetHelper(Node *node, double key) const;
    void Delete(Node *node);
public:
    RegularTree();
    bool IsEmpty(void) const;
    void Insert(double data); 
    void Remove(double key);
    double Get(double key);
    void Print(std::ostream &out) const;
    double GetAverage(void) const;
    double GetSum(void) const;
    void AddNodesByTask(int number);
    ~RegularTree();
};

#endif
