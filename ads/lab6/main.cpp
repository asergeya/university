#include <iostream>
#include <chrono>
#include <cstdlib>
#include <ctime>

#include "AVLTree.h"
#include "RegularTree.h"


#define SIZE_UPPER_BOUND 1000000
#define SIZE_HARDCODED 9999999
#define NUM_UPPER_BOUND 10000

void random_input(AVLTree &tree);
void random_input(RegularTree &tree);

int main(void)
{
    using namespace std::chrono;
    std::srand(std::time(nullptr));
    steady_clock::time_point start, end;

    AVLTree test;
    //RegularTree test;

    random_input(test);

    start = steady_clock::now(); 
    test.Insert(2021);
    end = steady_clock::now(); 
    std::cout << "[Addition] Time passed: "
              << duration_cast<nanoseconds>(end - start).count()
              << " ns" << std::endl;

    start = steady_clock::now(); 
    test.Get(2021);
    end = steady_clock::now(); 
    std::cout << "[Search] Time passed: " 
              << duration_cast<nanoseconds>(end - start).count() 
              << " ns" << std::endl;

    start = steady_clock::now(); 
    test.Remove(2021);
    end = steady_clock::now(); 
    std::cout << "[Removing] Time passed: " 
              << duration_cast<nanoseconds>(end - start).count() 
              << " ns" << std::endl;

    /* Task by variant
    test.Print(std::cout);

    int A;
    std::cout << "Enter A:\n";
    do {
        std::cin >> A;
    } while (A <= 0);

    test.AddNodesByTask(A);
    test.Print(std::cout);
    */
}

void random_input(AVLTree &tree)
{
    int size;
    
    do {
        size = std::rand() % SIZE_UPPER_BOUND;
    } while (size <= 0);

#ifdef SIZE_HARDCODED
    size = SIZE_HARDCODED;
#endif

    for (int i = 0; i < size; i++) {
        tree.Insert(rand() % NUM_UPPER_BOUND);
    }
}

void random_input(RegularTree &tree)
{
    int size;
    
    do {
        size = std::rand() % SIZE_UPPER_BOUND;
    } while (size <= 0);

#ifdef SIZE_HARDCODED
    size = SIZE_HARDCODED;
#endif

    for (int i = 0; i < size; i++) {
        tree.Insert(rand() % NUM_UPPER_BOUND);
    }
}
