#include <iostream>
#include <algorithm>
#include <chrono>
#include <vector>
#include <climits>
#include <cstdlib>
#include <ctime>

#define SIZE_UPPER_BOUND 1000000
#define SIZE_HARDCODED 100000000
#define NUM_UPPER_BOUND 1000000
#define NUM 202109

void print_vector(const std::vector<int> &vec);
void random_input_vector(std::vector<int> &vec);
int find_min_greater_than_n_linear_search(std::vector<int> &array, int n);
int right_bound(std::vector<int> &array, int n); 
int find_min_greater_than_n_binary_search(std::vector<int> &array, int n);

int main(void)
{
    using namespace std::chrono;
    std::srand(std::time(nullptr));
    std::vector<int> numbers;
    steady_clock::time_point start, end;
    
    random_input_vector(numbers); 
    std::sort(numbers.begin(), numbers.end());
    std::cout << "Vector size: " << numbers.size() << std::endl;
    //print_vector(numbers);

    start = steady_clock::now(); 
    std::cout << *std::upper_bound(numbers.begin(), numbers.end(), NUM) << std::endl;
    end = steady_clock::now(); 
    std::cout << "[STL algorithm] Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;

    start = steady_clock::now(); 
    std::cout << find_min_greater_than_n_binary_search(numbers, NUM) << std::endl;
    end = steady_clock::now(); 
    std::cout << "[Binary search] Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;
    
    start = steady_clock::now(); 
    std::cout << find_min_greater_than_n_linear_search(numbers, NUM) << std::endl;
    end = steady_clock::now(); 
    std::cout << "[Linear search] Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;
}

void print_vector(const std::vector<int> &vec)
{
    for (auto x : vec) {
        std::cout << x << " ";
    }
    std::cout << std::endl;
}

void random_input_vector(std::vector<int> &vec)
{
    int size;
    
    // Generate size of vector
    do {
        size = std::rand() % SIZE_UPPER_BOUND;
    } while (size <= 0);

#ifdef SIZE_HARDCODED
    size = SIZE_HARDCODED;
#endif

    // Fill vector with numbers
    for (int i = 0; i < size; i++) {
        vec.push_back(rand() % NUM_UPPER_BOUND);
    }
}

int find_min_greater_than_n_linear_search(std::vector<int> &array, int n)
{
    int min = INT_MAX;
    
    for (auto &x : array) {
        if (x > n and x < min) {
            min = x;
        } 
    }
    if (min == INT_MAX) {
        return -1;
    }
    return min;
}

int right_bound(std::vector<int> &array, int key) 
{
    int left = 0, right = array.size() - 1;

    while (left < right) {
        int middle = left + (right - left) / 2;

        if (array[middle] <= key) {
            left = middle + 1;    
        } 
        else if (array[middle] > key) {
            right = middle - 1;
        }
    }
    return right;
}

int find_min_greater_than_n_binary_search(std::vector<int> &array, int n)
{
    int potential_index = right_bound(array, n);
    
    if (array[potential_index] <= n) {
        potential_index++;
    }

    if (potential_index < array.size()) {
        return array[potential_index];
    }
    return -1;
}
