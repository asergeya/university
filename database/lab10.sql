USE Deanery;

# Добавление новой группы
INSERT INTO Groupe (Name, MajorID, HeadmanID, CuratorID) VALUES("КИ20-20Б", 3, NULL, NULL);

# Перевод студентов в новую группу
UPDATE Student SET GroupID = 5 WHERE GroupID = 3;

# Назначение старосты и куратора из "старой" группы
UPDATE Groupe 
SET 
    HeadmanID = (SELECT 
            HeadmanID
        FROM
            Groupe
        WHERE
            ID = 3),
    CuratorID = (SELECT 
            CuratorID
        FROM
            Groupe
        WHERE
            ID = 3)
WHERE
    ID = 5;
    
# Удаление "старой" группы
DELETE FROM Groupe WHERE ID = 3;
SET SQL_SAFE_UPDATES = 0;
#DELETE FROM Groupe WHERE ID NOT IN (SELECT DISTINCT GroupID FROM Student);
SET SQL_SAFE_UPDATES = 1;

SELECT * FROM Groupe;