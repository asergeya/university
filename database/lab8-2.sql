USE Shop;

# 1
SELECT
	Customer.Name AS Сотрудник,
    SUM(Deal.Quantity * Warehouse.UnitPrice) AS `Сумма сделок`,
    ROW_NUMBER() OVER (ORDER BY `Сумма Сделок` DESC) AS `Занятое место`
FROM
	Deal
		INNER JOIN
	Warehouse ON Deal.InventoryID = Warehouse.InventoryID
		INNER JOIN
	Customer ON Deal.CustomerID = Customer.CustomerID
GROUP BY Customer.CustomerID
ORDER BY `Занятое место`
LIMIT 3;

# 2
SELECT
	C.Name AS Сотрудник,
    YEAR(D.Date) AS Год,
    MONTH(D.Date) AS Месяц,
    SUM(D.Quantity * W.UnitPrice) AS `Сумма сделок`,
    SUM(D.Quantity * W.UnitPrice) - LEAD(SUM(D.Quantity * W.UnitPrice)) OVER w AS `Разница с предыдущим`
FROM
	Deal D
		INNER JOIN
	Warehouse W ON D.InventoryID = W.InventoryID
		INNER JOIN
	Customer C ON D.CustomerID = C.CustomerID
GROUP BY Сотрудник, Год, Месяц
WINDOW w AS (PARTITION BY Сотрудник ORDER BY Год, Месяц)
ORDER BY Сотрудник;