SELECT * FROM Shop.Supplier ORDER BY Name DESC;
SELECT * FROM Shop.Supplier WHERE LENGTH(Name)=(SELECT MIN(LENGTH(Name)) FROM Shop.Supplier);

SELECT * FROM Shop.Customer;
# between version
SELECT * FROM Shop.Customer WHERE LEFT(Name, 1) BETWEEN "I" and "M";
# like version
SELECT * FROM Shop.Customer WHERE Name LIKE "I%";
SELECT * FROM Shop.Customer WHERE Name RLIKE '^[I-M]v'; 

SELECT * FROM Shop.Deal;
# for a month
SELECT COUNT(DealID) FROM Shop.Deal WHERE DATE_SUB(CURDATE(), INTERVAL 1 MONTH) <= Date;
# for a current month
SELECT COUNT(DealID) FROM Shop.Deal WHERE MONTH(Date) = MONTH(CURDATE()) and YEAR(Date) = YEAR(CURDATE());
SELECT * FROM Shop.Deal WHERE WEEKDAY(Date) IN (2,3);