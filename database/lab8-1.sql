USE Shop;

# 1
SELECT 
    Product.Name AS Наименование,
    SUM(Warehouse.Quantity) AS Количество,
    CASE
        WHEN SUM(Warehouse.Quantity) < 5 THEN 'Мало'
        WHEN SUM(Warehouse.Quantity) <= 10 THEN 'Достаточно'
        ELSE 'Много'
    END AS Оценка
FROM
    Warehouse
        INNER JOIN
    Product ON Warehouse.ProductID = Product.ProductID
GROUP BY Warehouse.ProductID
ORDER BY Количество DESC;

# 2
SELECT 
    Product.Name AS Наименование,
    SUM(Warehouse.Quantity) AS Количество
FROM
    Warehouse
        INNER JOIN
    Product ON Warehouse.ProductID = Product.ProductID
GROUP BY Warehouse.ProductID
HAVING Количество BETWEEN 0 AND 10;

# View Warehouse
SELECT
	Warehouse.InventoryID AS ID,
	Product.Name AS Наименование
FROM
	Warehouse
		INNER JOIN
	Product ON Warehouse.ProductID = Product.ProductID;
    
# Add more products in deal
INSERT INTO Deal (InventoryID, CustomerID, EmployeeID, Quantity, Date) 
	VALUES(6,5,3,3,"2021-03-26"), (6,5,2,1,"2021-02-27"), (5,5,3,1,"2021-04-27"), (4,5,3,11,"2021-05-27");

# 3
SELECT
	Product.Name AS Наименование, 
    SUM(Deal.Quantity * Warehouse.UnitPrice) AS Сумма 
FROM 
	Deal
		INNER JOIN
	Warehouse ON Deal.InventoryID = Warehouse.InventoryID
		INNER JOIN
	Product ON Warehouse.ProductID = Product.ProductID
GROUP BY Product.ProductID
ORDER BY Сумма DESC
LIMIT 3;

# 4
SET lc_time_names = 'ru_RU';
SELECT
	Product.Name AS Наименование,
    YEAR(Deal.Date) AS Год,
    MONTHNAME(Deal.Date) AS Месяц,
    SUM(Deal.Quantity * Warehouse.UnitPrice) AS Сумма
FROM
	Deal
		INNER JOIN
	Warehouse ON Deal.InventoryID = Warehouse.InventoryID
		INNER JOIN
	Product ON Warehouse.ProductID = Product.ProductID
GROUP BY Наименование, Год, Месяц;

# 5
SELECT
	Product.Name AS Наименование,
    YEAR(Deal.Date) AS Год,
    MONTHNAME(Deal.Date) AS Месяц,
    SUM(Deal.Quantity * Warehouse.UnitPrice) AS Сумма
FROM
	Deal
		INNER JOIN
	Warehouse ON Deal.InventoryID = Warehouse.InventoryID
		INNER JOIN
	Product ON Warehouse.ProductID = Product.ProductID
GROUP BY Наименование, Год, Месяц
HAVING Наименование = "Dell Vostro 5590" AND Сумма < 80000;