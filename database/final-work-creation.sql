-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema BusService
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `BusService` ;

-- -----------------------------------------------------
-- Schema BusService
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `BusService` ;
USE `BusService` ;

-- -----------------------------------------------------
-- Table `BusService`.`Bus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Bus` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Bus` (
  `BusID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Model` NVARCHAR(45) NOT NULL,
  `Capacity` INT NOT NULL,
  PRIMARY KEY (`BusID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BusService`.`Town`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Town` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Town` (
  `TownID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` NVARCHAR(45) NOT NULL,
  PRIMARY KEY (`TownID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BusService`.`Cash`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Cash` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Cash` (
  `CashID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `TownID` INT UNSIGNED NOT NULL,
  `Address` NVARCHAR(255) NOT NULL,
  PRIMARY KEY (`CashID`),
  INDEX `fk_Cash_Town1_idx` (`TownID` ASC) VISIBLE,
  CONSTRAINT `fk_Cash_Town1`
    FOREIGN KEY (`TownID`)
    REFERENCES `BusService`.`Town` (`TownID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BusService`.`Passenger`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Passenger` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Passenger` (
  `PassengerID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` NVARCHAR(45) NOT NULL,
  PRIMARY KEY (`PassengerID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BusService`.`Route`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Route` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Route` (
  `RouteID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `From` INT UNSIGNED NOT NULL,
  `To` INT UNSIGNED NOT NULL,
  `Departure` TIME NOT NULL,
  `Duration` TIME NOT NULL,
  `Days` NVARCHAR(30) NOT NULL,
  `Price` DECIMAL(12,4) NOT NULL,
  `MinCapacity` INT NOT NULL,
  PRIMARY KEY (`RouteID`),
  INDEX `fk_Route_Town1_idx` (`From` ASC) VISIBLE,
  INDEX `fk_Route_Town2_idx` (`To` ASC) VISIBLE,
  CONSTRAINT `fk_Route_Town1`
    FOREIGN KEY (`From`)
    REFERENCES `BusService`.`Town` (`TownID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Route_Town2`
    FOREIGN KEY (`To`)
    REFERENCES `BusService`.`Town` (`TownID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BusService`.`Driver`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Driver` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Driver` (
  `DriverID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` NVARCHAR(45) NOT NULL,
  `TownID` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`DriverID`),
  INDEX `fk_Driver_Town1_idx` (`TownID` ASC) VISIBLE,
  CONSTRAINT `fk_Driver_Town1`
    FOREIGN KEY (`TownID`)
    REFERENCES `BusService`.`Town` (`TownID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BusService`.`Trip`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Trip` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Trip` (
  `TripID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `RouteID` INT UNSIGNED NOT NULL,
  `BusID` INT UNSIGNED,
  `DriverID` INT UNSIGNED,
  `Date` DATE NOT NULL,
  PRIMARY KEY (`TripID`),
  INDEX `fk_Trip_Route1_idx` (`RouteID` ASC) VISIBLE,
  INDEX `fk_Trip_Bus1_idx` (`BusID` ASC) VISIBLE,
  INDEX `fk_Trip_Driver1_idx` (`DriverID` ASC) VISIBLE,
  CONSTRAINT `fk_Trip_Route1`
    FOREIGN KEY (`RouteID`)
    REFERENCES `BusService`.`Route` (`RouteID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Trip_Bus1`
    FOREIGN KEY (`BusID`)
    REFERENCES `BusService`.`Bus` (`BusID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Trip_Driver1`
    FOREIGN KEY (`DriverID`)
    REFERENCES `BusService`.`Driver` (`DriverID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `BusService`.`Ticket`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `BusService`.`Ticket` ;

CREATE TABLE IF NOT EXISTS `BusService`.`Ticket` (
  `TicketID` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `CashID` INT UNSIGNED NOT NULL,
  `PassengerID` INT UNSIGNED NOT NULL,
  `TripID` INT UNSIGNED NOT NULL,
  `Place` INT UNSIGNED NOT NULL,
  `Timestamp` DATETIME NOT NULL,
  PRIMARY KEY (`TicketID`),
  INDEX `fk_Ticket_Cash_idx` (`CashID` ASC) VISIBLE,
  INDEX `fk_Ticket_Passenger1_idx` (`PassengerID` ASC) VISIBLE,
  INDEX `fk_Ticket_Trip1_idx` (`TripID` ASC) VISIBLE,
  CONSTRAINT `fk_Ticket_Cash`
    FOREIGN KEY (`CashID`)
    REFERENCES `BusService`.`Cash` (`CashID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ticket_Passenger1`
    FOREIGN KEY (`PassengerID`)
    REFERENCES `BusService`.`Passenger` (`PassengerID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Ticket_Trip1`
    FOREIGN KEY (`TripID`)
    REFERENCES `BusService`.`Trip` (`TripID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;