DROP DATABASE IF EXISTS Deanery;
CREATE DATABASE IF NOT EXISTS Deanery;
USE Deanery;

CREATE TABLE IF NOT EXISTS Major (
    ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Code NVARCHAR(10) NOT NULL,
    Name NVARCHAR(50) NOT NULL,
    TypeOfEducation NVARCHAR(50) NOT NULL,
    Degree NCHAR(1) NOT NULL,
    Duration INT UNSIGNED NOT NULL,
    Description NVARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS Teacher (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Surname NVARCHAR(255) NOT NULL,
    Name NVARCHAR(255) NOT NULL,
    MiddleName NVARCHAR(255),
    ScienceDegree NVARCHAR(255) NOT NULL,
    ScienceTitle NVARCHAR(255) NOT NULL,
    Department NVARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS Student (
	ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Surname NVARCHAR(255) NOT NULL,
    Name NVARCHAR(255) NOT NULL,
    MiddleName NVARCHAR(255),
    GroupID INT NOT NULL,
    AdmissionYear YEAR NOT NULL
);

CREATE TABLE IF NOT EXISTS Groupe (
	ID INT NOT NULL AUTO_INCREMENT,
    Name NVARCHAR(10) NOT NULL,
    MajorID INT NOT NULL,
    HeadmanID INT,
    CuratorID INT,
    
    PRIMARY KEY (ID),
	FOREIGN KEY (MajorID) REFERENCES Major(ID),
	FOREIGN KEY (HeadmanID) REFERENCES Student(ID),
	FOREIGN KEY (CuratorID) REFERENCES Teacher(ID)
);

ALTER TABLE Student ADD FOREIGN KEY (GroupID) REFERENCES Groupe(ID);

CREATE TABLE IF NOT EXISTS Subject (
	ID INT NOT NULL AUTO_INCREMENT,
    Name NVARCHAR(255) NOT NULL,
    MajorID INT NOT NULL,
    Semester INT NOT NULL,
    Hours INT UNSIGNED NOT NULL,
    Attestation NVARCHAR(255) NOT NULL,
    
	PRIMARY KEY (ID),
    FOREIGN KEY (MajorID) REFERENCES Major(ID)
);

CREATE TABLE IF NOT EXISTS Gradebook (
	ID INT NOT NULL AUTO_INCREMENT,
    StudentID INT NOT NULL,
    SubjectID INT NOT NULL,
    Date Date NOT NULL,
    Grade NVARCHAR(50) NOT NULL,
    
    PRIMARY KEY (ID),
    FOREIGN KEY (StudentID) REFERENCES Student(ID),
	FOREIGN KEY (SubjectID) REFERENCES Subject(ID)
);

CREATE TABLE IF NOT EXISTS Teaching (
	ID INT NOT NULL AUTO_INCREMENT,
    SubjectID INT NOT NULL,
    TeacherID INT NOT NULL,
    
    PRIMARY KEY (ID),
    FOREIGN KEY (SubjectID) REFERENCES Subject(ID),
    FOREIGN KEY (TeacherID) REFERENCES Teacher(ID)
);

INSERT INTO Major (Code, Name, TypeOfEducation, Degree, Duration, Description) VALUES
	("09.03.01","Информатика и вычислительная техника","Очно","Б",8,"Здесь должно быть описание"),
    ("09.03.02","Информационные системы и технологии","Очно","Б",8,"Здесь должно быть описание"),
    ("09.03.03","Прикладная информатика","Очно","Б",8,"Здесь должно быть описание"),
    ("09.04.01","Информатика и вычислительная техника","Очно","М",8,"Здесь должно быть описание");

INSERT INTO Teacher (Surname, Name, MiddleName, ScienceDegree, ScienceTitle, Department) VALUES
	("Владимиров","Сергей","Иванович", "Доктор наук", "Профессор", "Математика");
    
INSERT INTO Groupe (Name, MajorID, HeadmanID, CuratorID) VALUES
	("КИ20-08Б", 1, NULL, 1),
    ("КИ20-11Б", 2, NULL, 1),
    ("КИ20-12Б", 2, NULL, 1),
    ("КИ20-09М", 4, NULL, 1);

INSERT INTO Student (Surname, Name, MiddleName, GroupID, AdmissionYear) VALUES
	("Владимиров","Сергей","Иванович",1,"2020"),
    ("Петров","Петр","Петрович",1,"2020"),
    ("Иванов","Иван","Иванович",1,"2020"),
    ("Сидоров","Олег","Анатольевич",1,"2020"),
    ("Кустов","Сергей","Александрович",1,"2020"),
    ("Бобров","Алексей","Олегович",1,"2020"),
    ("Анакин","Максим","Федорович",1,"2020"),
    ("Гарин","Евгений","Николаевич",2,"2020"),
    ("Копылов","Владимир","Александрович",2,"2020"),
    ("Железняк","Сергей","Алексеевич",2,"2020"),
    ("Бацылев","Владимир","Михайлович",2,"2020"),
    ("Пухов","Сергей","Анатольевич",2,"2020"),
    ("Семенов","Дмитрий","Анатольевич",2,"2020"),
    ("Арханов","Виталий","Эдуардович",2,"2020"),
    ("Титов","Эдуард","Михайлович",3,"2020"),
    ("Гордаш","Сергей","Петрович",3,"2020"),
    ("Суховецкий","Александр","Дмитриевич",3,"2020"),
    ("Гладышев","Андрей","Борисович",3,"2020"),
    ("Николаев","Афанасий","Витальевич",3,"2020"),
    ("Климов","Степан","Анатольевич",3,"2020"),
    ("Филлипов","Даниил","Александрович",3,"2020"),
    ("Пестеров","Сергей","Афанасьевич",4,"2020"),
    ("Пестеров","Александр","Сергеевич",4,"2020"),
    ("Жданов","Сергей","Анатольевич",4,"2020"),
    ("Пухов","Лев","Егорович",4,"2020"),
    ("Кобзев","Анатолий","Анатольевич",4,"2020"),
    ("Дуров","Егор","Анатольевич",4,"2020"),
    ("Бриков","Андрей","Анатольевич",4,"2020");

# Добавляем старост групп
UPDATE Groupe SET HeadmanID = 1 WHERE ID = 1;
UPDATE Groupe SET HeadmanID = 8 WHERE ID = 2;
UPDATE Groupe SET HeadmanID = 15 WHERE ID = 3;
UPDATE Groupe SET HeadmanID = 22 WHERE ID = 4;

INSERT INTO Subject (Name, MajorID, Semester, Hours, Attestation) VALUES
	# Первый семестр ИВТ (первое направление)
	("Алгебра и геометрия", 1, 1, 80, "З"),
    ("Основы программирования", 1, 1, 80, "З"),
    ("ВПД", 1, 1, 80, "З"),
    ("Алгебра и геометрия", 1, 1, 80, "Э"),
    ("Математический анализ", 1, 1, 80, "Э"),
    # Второй семестр ИВТ (первое направление)
    ("Основы программирования", 1, 2, 80, "З"),
    ("ВПД", 1, 2, 80, "З"),
    ("Учебная практика", 1, 2, 80, "З"),
    ("Учебная практика", 1, 2, 80, "Э"),
    ("Философия", 1, 2, 80, "Э"),
    # ИСИТ
    ("Основы программирования", 2, 2, 80, "З");
    
INSERT INTO Gradebook (StudentID, SubjectID, Date, Grade) VALUES
	# Первый семестр ИВТ
    ## Отличник только в первом семестре
	(1, 1, "2020-12-29", "Зачтено"),
    (1, 2, "2020-12-29", "Зачтено"),
    (1, 3, "2020-12-29", "Зачтено"),
    (1, 4, "2020-12-29", "Отлично"),
    (1, 5, "2020-12-29", "Отлично"),

    (2, 1, "2020-12-29", "Не зачтено"),
    (2, 2, "2020-12-29", "Зачтено"),
    (2, 3, "2020-12-29", "Зачтено"),
    (2, 4, "2020-12-29", "Неудовлетворительно"),
    (2, 5, "2020-12-29", "Отлично"),
    
    (3, 1, "2020-12-29", "Зачтено"),
    (3, 2, "2020-12-29", "Зачтено"),
    (3, 3, "2020-12-29", "Зачтено"),
    (3, 4, "2020-12-29", "Хорошо"),
    (3, 5, "2020-12-29", "Отлично"),
    
    (4, 1, "2020-12-29", "Зачтено"),
    (4, 2, "2020-12-29", "Зачтено"),
    (4, 3, "2020-12-29", "Зачтено"),
    (4, 4, "2020-12-29", "Удовлетворительно"),
    (4, 5, "2020-12-29", "Хорошо"),
    
    ## Отличник в первом семестре и во втором
    (5, 1, "2020-12-29", "Зачтено"),
    (5, 2, "2020-12-29", "Зачтено"),
    (5, 3, "2020-12-29", "Зачтено"),
    (5, 4, "2020-12-29", "Отлично"),
    (5, 5, "2020-12-29", "Отлично"),
    
    ## Отличник только во втором семестре
    (6, 1, "2020-12-29", "Зачтено"),
    (6, 2, "2020-12-29", "Зачтено"),
    (6, 3, "2020-12-29", "Зачтено"),
    (6, 4, "2020-12-29", "Хорошо"),
    (6, 5, "2020-12-29", "Отлично"),
    
    (7, 1, "2020-12-29", "Зачтено"),
    (7, 2, "2020-12-29", "Зачтено"),
    (7, 3, "2020-12-29", "Зачтено"),
    (7, 4, "2020-12-29", "Хорошо"),
    (7, 5, "2020-12-29", "Хорошо"),
    # Второй семестр ИВТ
    ## Отличник только в первом семестре
	(1, 6, "2021-07-01", "Зачтено"),
    (1, 7, "2021-07-01", "Зачтено"),
    (1, 8, "2021-07-01", "Зачтено"),
    (1, 9, "2021-07-01", "Хорошо"),
    (1, 10, "2021-07-01", "Хорошо"),
    
    (2, 6, "2021-07-01", "Не зачтено"),
    (2, 7, "2021-07-01", "Зачтено"),
    (2, 8, "2021-07-01", "Зачтено"),
    (2, 9, "2021-07-01", "Неудовлетворительно"),
    (2, 10, "2021-07-01", "Отлично"),
    
    (3, 6, "2021-07-01", "Зачтено"),
    (3, 7, "2021-07-01", "Зачтено"),
    (3, 8, "2021-07-01", "Зачтено"),
    (3, 9, "2021-07-01", "Хорошо"),
    (3, 10, "2021-07-01", "Отлично"),
    
    (4, 6, "2021-07-01", "Зачтено"),
    (4, 7, "2021-07-01", "Зачтено"),
    (4, 8, "2021-07-01", "Зачтено"),
    (4, 9, "2021-07-01", "Удовлетворительно"),
    (4, 10, "2021-07-01", "Хорошо"),
    
    ## Отличник в первом семестре и во втором
    (5, 6, "2021-07-01", "Зачтено"),
    (5, 7, "2021-07-01", "Зачтено"),
    (5, 8, "2021-07-01", "Зачтено"),
    (5, 9, "2021-07-01", "Отлично"),
    (5, 10, "2021-07-01", "Отлично"),
	
    ## Отличник только во втором семестре
    (6, 6, "2021-07-01", "Зачтено"),
    (6, 7, "2021-07-01", "Зачтено"),
    (6, 8, "2021-07-01", "Зачтено"),
    (6, 9, "2021-07-01", "Отлично"),
    (6, 10, "2021-07-01", "Отлично"),
    
    (7, 6, "2021-07-01", "Зачтено"),
    (7, 7, "2021-07-01", "Зачтено"),
    (7, 8, "2021-07-01", "Зачтено"),
    (7, 9, "2021-07-01", "Хорошо"),
    (7, 10, "2021-07-01", "Хорошо");