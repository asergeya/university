LOAD DATA LOCAL INFILE '/home/sergey/Documents/university/database/products.txt' INTO TABLE Shop.Product
FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE '/home/sergey/Documents/university/database/suppliers.txt' INTO TABLE Shop.Supplier
FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE '/home/sergey/Documents/university/database/employees.txt' INTO TABLE Shop.Employee
FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE '/home/sergey/Documents/university/database/customers.txt' INTO TABLE Shop.Customer
FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE '/home/sergey/Documents/university/database/warehouse.txt' INTO TABLE Shop.Warehouse
FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE '/home/sergey/Documents/university/database/deals.txt' INTO TABLE Shop.Deal
FIELDS TERMINATED BY ',';

SELECT * FROM Shop.Product;
SELECT * FROM Shop.Supplier;
SELECT * FROM Shop.Employee;
SELECT * FROM Shop.Customer;
SELECT * FROM Shop.Warehouse;
SELECT * FROM Shop.Deal;