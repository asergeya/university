USE Deanery;

CREATE OR REPLACE VIEW plan AS
SELECT
	Student.ID AS `ID`,
    CONCAT(Student.Surname, ' ', Student.Name, ' ', Student.MiddleName) AS `Full Name`,
    Groupe.Name AS `Group`,
    Major.Code As `Major`,
    Subject.Semester AS `Semester`,
	COUNT(Subject.ID) AS `ExamsNum`
FROM
	Subject
		INNER JOIN
	Major ON Subject.MajorID = Major.ID
		INNER JOIN
	Groupe ON Major.ID = Groupe.MajorID
		INNER JOIN
	Student ON Groupe.ID = Student.GroupID
GROUP BY Student.ID, Major.ID, Subject.Semester;

CREATE OR REPLACE VIEW perfect AS
SELECT
	StudentID AS `ID`,
    Subject.Semester AS `Semester`,
    COUNT(Grade) AS `ExamsNum`
FROM
	Gradebook
		INNER JOIN
	Subject ON Gradebook.SubjectID = Subject.ID
WHERE Grade IN ("Отлично", "Зачтено")
GROUP BY StudentID, Semester;

CREATE OR REPLACE VIEW excellent_students AS
SELECT
	`Full Name` AS ФИО,
    `Group` AS Группа,
    `Semester` AS Семестр
FROM
	plan
		INNER JOIN
	perfect USING(ID, Semester)
WHERE plan.ExamsNum = perfect.ExamsNum
GROUP BY `ID`, `Group`, `Semester`;

SELECT * FROM excellent_students;
