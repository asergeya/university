USE Shop;

# 1 subquery
SELECT 
    Name
FROM
    Product
WHERE
    ProductID IN (SELECT 
            ProductID
        FROM
            Warehouse
        WHERE
            Quantity = (SELECT 
                    MAX(Quantity)
                FROM
                    Warehouse));

# 1 join
SELECT 
    Product.Name, Warehouse.Quantity
FROM
    Warehouse
        INNER JOIN
    Product ON Warehouse.ProductID = Product.ProductID
WHERE
    Quantity = (SELECT 
            MAX(Quantity)
        FROM
            Shop.Warehouse);
# 2 subquery
SELECT 
    Name
FROM
    Product
WHERE
    ProductID IN (SELECT 
            ProductID
        FROM
            Warehouse
        WHERE
            Quantity BETWEEN 2 AND 10)
ORDER BY Name DESC;

# 2 join
SELECT
    Warehouse.InventoryID, Product.Name, Warehouse.Quantity
FROM
    Warehouse
        INNER JOIN
    Product ON Warehouse.ProductID = Product.ProductID
WHERE
    Quantity BETWEEN 2 AND 10
ORDER BY Product.Name DESC;

# 3 subquery
SELECT 
    Name
FROM
    Supplier
WHERE
    SupplierID IN (SELECT 
            SupplierID
        FROM
            Warehouse)
ORDER BY Name;

# 3 join
SELECT DISTINCT
    Supplier.Name
FROM
    Warehouse
		INNER JOIN
    Supplier ON Warehouse.SupplierID = Supplier.SupplierID
ORDER BY Supplier.Name;

# 4 subquery
SELECT 
    Name
FROM
    Customer
WHERE
    CustomerID IN (SELECT 
            CustomerID
        FROM
            Deal
        WHERE
            DATE_SUB(CURDATE(), INTERVAL 30 DAY) <= Date);

# 4 join
SELECT 
	Customer.Name, Deal.Date
FROM
	Deal
		INNER JOIN
	Customer ON Deal.CustomerID = Customer.CustomerID
WHERE DATE_SUB(CURDATE(), INTERVAL 1 MONTH) <= Date;

# 5 subquery
SELECT MIN(UnitPrice), 0.4 * MAX(UnitPrice) FROM Warehouse;
SELECT 
    Name
FROM
    Product
WHERE
    Name RLIKE '^[O,H]'
        AND ProductID IN (SELECT 
            ProductID
        FROM
            Warehouse
        WHERE
            UnitPrice <= 0.4 * (SELECT 
                    MAX(UnitPrice)
                FROM
                    Warehouse)); 

# 6 subquery
SELECT 
    Name
FROM
    Supplier
WHERE
    SupplierID NOT IN (SELECT 
            SupplierID
        FROM
            Warehouse
        WHERE
            ProductID IN (SELECT 
                    ProductID
                FROM
                    Product
                WHERE
                    Product.Name RLIKE '^[O,H]'));

# 7 subquery
SELECT 
    Name, 
    CASE
		WHEN Gender = "ж" THEN "Женщина" 
        WHEN Gender = "м" THEN "Мужчина" 
        ELSE "undefine" END As Gender
FROM
    Customer
WHERE
    CustomerID IN (SELECT 
            CustomerID
        FROM
            Deal);
            