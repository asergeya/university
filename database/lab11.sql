USE Deanery;

DROP FUNCTION IF EXISTS GetAverageGrade;
DROP FUNCTION IF EXISTS GetGroupID;
DROP FUNCTION IF EXISTS GetMajorID;
DROP PROCEDURE IF EXISTS GetExamsListByGroupID;
DROP PROCEDURE IF EXISTS GetExamsListByGroupName;

delimiter //

CREATE FUNCTION GetAverageGrade(subject_id INT)
RETURNS DOUBLE DETERMINISTIC
BEGIN
	DECLARE average_grade DOUBLE;
	SELECT 
		SUM(
			CASE Grade
				WHEN 'Отлично' THEN 5
				WHEN 'Хорошо' THEN 4
				WHEN 'Удовлетворительно' THEN 3
				WHEN 'Неудовлетворительно' THEN 2
			END
		) / COUNT(
			CASE Grade
				WHEN 'Отлично' THEN 5
				WHEN 'Хорошо' THEN 4
				WHEN 'Удовлетворительно' THEN 3
				WHEN 'Неудовлетворительно' THEN 2
			END) INTO average_grade
	FROM
		Gradebook
	WHERE 
		SubjectID = subject_id;
        
    RETURN average_grade;
END//

CREATE FUNCTION GetGroupID(group_name NVARCHAR(10))
RETURNS INT DETERMINISTIC
BEGIN
	RETURN (SELECT ID FROM Groupe WHERE Name = group_name);
END//

CREATE FUNCTION GetMajorID(group_id INT)
RETURNS INT DETERMINISTIC
BEGIN
	RETURN (SELECT MajorID FROM Groupe WHERE ID = group_id);
END//

CREATE PROCEDURE GetExamsListByGroupID(IN group_id INT, IN semester_no INT)
BEGIN
	DECLARE major_id INT;
    
    SET major_id = GetMajorID(group_id);
    
    SELECT DISTINCT Name AS Предмет From Subject WHERE MajorID = major_id AND Semester = semester_no;
END//

CREATE PROCEDURE GetExamsListByGroupName(IN group_name NVARCHAR(10), IN semester_no INT)
BEGIN
	DECLARE group_id INT;
	DECLARE major_id INT;
    
    SET group_id = GetGroupID(group_name);
    SET major_id = GetMajorID(group_id);	
    
    SELECT DISTINCT Name AS Предмет, Attestation AS `Тип аттестации` From Subject WHERE MajorID = major_id AND Semester = semester_no;
END//

delimiter ;

SELECT GetAverageGrade(10) AS `Средний балл`;
CALL GetExamsListByGroupID(1, 2);
CALL GetExamsListByGroupName("КИ20-08Б", 2);