USE `BusService`;

# 1. Получить данные (время, цену на билет) о рейсах в город Томск из города Новосибирск.
SELECT 
    CONCAT(`Date`, ' ', `Departure`) AS `Дата и время отправления`, 
    `Duration` AS `Время в пути`, 
    `Price` AS `Цена`
FROM
    `Trip`
		INNER JOIN
	`Route` USING(`RouteID`)
WHERE
    `From` IN (SELECT 
            `TownID`
        FROM
            `Town`
        WHERE
            `Name` = 'Новосибирск')
	AND
	`To` IN (SELECT 
            `TownID`
        FROM
            `Town`
        WHERE
            Name = 'Томск');

# 2. Получить суммарный доход какой-либо кассы.
# одной определенной
SELECT
	SUM(`Price`) AS `Доход`
FROM
	`Ticket`
		INNER JOIN
	`Trip` USING(`TripID`)
		INNER JOIN
	`Route` USING(`RouteID`)
WHERE `CashID` = 1;
# по кассам
SELECT
	`CashID` AS `Касса`,
    `Town`.`Name` AS `Город`,
	SUM(`Price`) AS `Доход`
FROM
	`Ticket`
		INNER JOIN
	`Cash` USING(`CashID`)
		INNER JOIN
	`Town` USING(`TownID`)
		INNER JOIN
	`Trip` USING(`TripID`)
		INNER JOIN
	`Route` USING(`RouteID`)
GROUP BY `CashID`, `Town`.`Name`;

# 3. Получить суммарные доходы касс в городе Томске.
SELECT 
    SUM(`Price`) AS `Доход касс`
FROM
    `Ticket`
        INNER JOIN
    `Trip` USING (`TripID`)
        INNER JOIN
    `Route` USING (`RouteID`)
WHERE
    `CashID` IN (SELECT 
            `CashID`
        FROM
            `Cash`
        WHERE
            `TownID` = (SELECT 
                    `TownID`
                FROM
                    `Town`
                WHERE
                    `Name` = 'Томск'));

# 4. Получить число рейсов, совершённых данным водителем за год.
SELECT
	COUNT(*) AS `Число рейсов`
FROM
	`Trip`
WHERE `DriverID` = 4 AND DATE_SUB(CURDATE(), INTERVAL 1 YEAR) <= `Date`;

# 5. Назначить водителя на рейс.
DROP TRIGGER IF EXISTS Route_DriverInsertChecker;
DROP TRIGGER IF EXISTS Route_DriverUpdateChecker;

DELIMITER //

CREATE TRIGGER Route_DriverInsertChecker
BEFORE INSERT ON `Trip`
FOR EACH ROW 
BEGIN
	IF NEW.`DriverID` NOT IN (SELECT `DriverID` FROM `Driver` WHERE `TownID` IN 
							 (SELECT `From` FROM `Route` WHERE `RouteID` = NEW.`RouteID`)) THEN
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'Driver does not live in this town!';
	END IF;
END//

CREATE TRIGGER Route_DriverUpdateChecker
BEFORE UPDATE ON `Trip`
FOR EACH ROW 
BEGIN
	IF NEW.`DriverID` NOT IN (SELECT `DriverID` FROM `Driver` WHERE `TownID` IN 
							 (SELECT `From` FROM `Route` WHERE `RouteID` = NEW.`RouteID`)) THEN
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'Driver does not live in this town!';
	END IF;
END//

DELIMITER ;

SELECT * FROM `Trip` WHERE `TripID` = 9;
UPDATE `Trip` SET `DriverID` = 1 WHERE `TripID` = 9;
SELECT * FROM `Trip` WHERE `TripID` = 9;

# 6. Оформить билет для клиента в город Новосибирск из Томска, с учѐтом занятых мест в автобусе.
DROP PROCEDURE IF EXISTS GetFreePlaces;
DROP PROCEDURE IF EXISTS BuyTicket;

DELIMITER //

CREATE PROCEDURE GetFreePlaces(IN trip INT UNSIGNED)
BEGIN
	DECLARE bus INT UNSIGNED;
    DECLARE max_place INT UNSIGNED;
    
    SELECT `BusID` INTO bus FROM `Trip` WHERE `TripID` = trip;
    SELECT `Capacity` INTO max_place FROM `Bus` WHERE `BusID` = bus;
	
    SELECT 
		seq AS Place 
	FROM 
		seq_1_to_1000 
	WHERE
		seq NOT IN (SELECT `Place` From `Ticket` WHERE `TripID` = trip) 
			AND 
		seq <= max_place;
END//

CREATE PROCEDURE BuyTicket(IN cash INT UNSIGNED, passenger INT UNSIGNED, IN trip INT UNSIGNED)
BEGIN
	DECLARE place INT UNSIGNED;
    
	CALL GetFreePlaces(trip);
    SELECT * INTO place FROM free_place LIMIT 1;
    
    IF place IS NULL THEN 
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'There is not free place!';
	END IF;
    
    INSERT INTO Ticket (`CashID`, `PassengerID`, `TripID`, `Place`, `Timestamp`) 
		VALUES(cash, passenger, trip, place, NOW());
END//

DELIMITER ;

CALL GetFreePlaces(1);
CALL BuyTicket(1, 1, 1);

# 7. Получить число рейсов, совершѐнных автобусами «Икарус» и «ПАЗ».
SELECT 
    COUNT(*) AS `Число рейсов`
FROM
    `Trip`
WHERE
    `BusID` IN (SELECT 
            `BusID`
        FROM
            `Bus`
        WHERE
            `Model` IN ('Икарус' , 'ПАЗ'));
            
# 8. Проверить все свободные места на рейс. См. №6.
Call GetFreePlaces(1);

# 9. Показать число билетов, проданных кассами.
SELECT
	`CashID` AS `Касса`,
    `Address` AS `Адрес`,
	COUNT(*) AS `Продано`
FROM
	`Ticket`
		INNER JOIN
	`Cash` USING(`CashID`)
GROUP BY `CashID`;

# 10. Показать среднюю цену билетов.
DROP FUNCTION IF EXISTS GetAveragePrice;

DELIMITER //

CREATE FUNCTION GetAveragePrice()
RETURNS DOUBLE DETERMINISTIC
BEGIN
	RETURN (SELECT AVG(`Price`) FROM `Ticket` INNER JOIN `Trip` USING(`TripID`) INNER JOIN `Route` USING(`RouteID`));
END//

DELIMITER ;

SELECT GetAveragePrice() AS `Средняя цена`;

# 11. Показать рейсы, цена на которые выше средней цены на билеты.
SELECT
	`RouteID` AS `ID`,
    FT.`Name` AS `Откуда`,
    TT.`Name` AS `Куда`,
    `Price` AS `Цена`
FROM
	`Route`
		INNER JOIN
	`Town` FT ON `Route`.`From` = FT.`TownID`
		INNER JOIN
	`Town` TT ON `Route`.`To` = TT.`TownID`
WHERE `Price` >= (SELECT AVG(`Price`) FROM `Route`);

# 12. Отменить клиенту билет, после того как он его вернул.
DELETE FROM `Ticket` WHERE `PassengerID` = 5 AND `TicketID` = 21;

# 13. Показать информацию о рейсах из города Томск (включая цену на билет, время рейса).
SELECT
	FT.`Name` AS `Откуда`,
    TT.`Name` AS `Куда`,
    CONCAT(`Date`, ' ', `Departure`) AS `Дата и время`,
    `Days` AS `Дни`,
	`Price` AS `Цена`
FROM
	`Trip`
		INNER JOIN
	`Route` USING(`RouteID`)
		INNER JOIN
	`Town` FT ON `Route`.`From` = FT.`TownID`
		INNER JOIN
	`Town` TT ON `Route`.`To` = TT.`TownID`
WHERE FT.`Name` = "Томск";

# 14. Посчитать количество билетов, проданных по рейсам Томск – Новосибирск за 2014-2015.
SELECT
	COUNT(*) AS `Билеты 2014-2015`
FROM
	`Ticket`
		INNER JOIN
	`Trip` USING(`TripID`)
		INNER JOIN
	`Route` USING(`RouteID`)
WHERE
	`From` = (SELECT `TownID` FROM `Town` WHERE `Name` = "Томск")
		AND
	`To` = (SELECT `TownID` FROM `Town` WHERE `Name` = "Новосибирск")
		AND
	`Date` BETWEEN "2014-01-01" AND "2015-12-31";
    
# 15. Назначить автобус на рейс.
DROP TRIGGER IF EXISTS Route_BusInsertChecker;
DROP TRIGGER IF EXISTS Route_BusUpdateChecker;

DELIMITER //

CREATE TRIGGER Route_BusInsertChecker
BEFORE INSERT ON `Trip`
FOR EACH ROW 
BEGIN
	IF NEW.`BusID` NOT IN (SELECT `BusID` FROM `Bus` WHERE `Capacity` >= 
							 (SELECT `MinCapacity` FROM `Route` WHERE `RouteID` = NEW.`RouteID`)) THEN
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'This bus is very small!';
	END IF;
END//

CREATE TRIGGER Route_BusUpdateChecker
BEFORE UPDATE ON `Trip`
FOR EACH ROW 
BEGIN
	IF NEW.`BusID` NOT IN (SELECT `BusID` FROM `Bus` WHERE `Capacity` >= 
							 (SELECT `MinCapacity` FROM `Route` WHERE `RouteID` = NEW.`RouteID`)) THEN
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'This bus is very small!';
	END IF;
END//

DELIMITER ;

SELECT * FROM `Trip` WHERE `TripID` = 10;
UPDATE `Trip` SET `BusID` = 4 WHERE `TripID` = 10;
SELECT * FROM `Trip` WHERE `TripID` = 10;

# 16. Получить список рейсов, на которые может быть назначен данный водитель и данный автобус с числом мест более 30.
SELECT
	*
FROM
	`Trip`
		INNER JOIN
	`Route` USING(`RouteID`)
WHERE 
	`From` = (SELECT `TownID` FROM `Driver` WHERE `DriverID` = 1)
		AND
	`MinCapacity` <= 30;

# 17. После ремонта в автобусе добавили 3 места, добавить эти места и в БД.
SET SQL_SAFE_UPDATES = 0;
UPDATE `Bus` SET `Capacity` = `Capacity` + 3 WHERE `BusID` = 4;
SET SQL_SAFE_UPDATES = 1;