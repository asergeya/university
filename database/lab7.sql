USE Shop;

# 1
SELECT 
    Product.Name AS Product, Supplier.Name AS Supplier
FROM
    Warehouse
        INNER JOIN
    Product ON Warehouse.ProductID = Product.ProductID
        INNER JOIN
    Supplier ON Warehouse.SupplierID = Supplier.SupplierID;
    
# 2
SELECT 
    Product.Name AS Product, Supplier.Name AS Supplier, Warehouse.Quantity AS Quantity
FROM
    Product
        LEFT JOIN
    Warehouse ON Product.ProductID = Warehouse.ProductID
		LEFT JOIN
	Supplier ON Warehouse.SupplierID = Supplier.SupplierID;

# 3 left
SELECT
	Product.Name AS Product, Customer.Name AS Customer, Deal.Quantity AS Quantity
FROM 
	Warehouse
		LEFT JOIN
	Deal ON Warehouse.InventoryID = Deal.InventoryID
		LEFT JOIN
	Customer ON Deal.CustomerID = Customer.CustomerID
		LEFT JOIN
	Product ON Warehouse.ProductID = Product.ProductID;

# 3 right
SELECT 
    Product.Name AS Product,
    IFNULL(Customer.Name, 'undefined') AS Customer,
    IFNULL(Deal.Quantity, 'no sales') AS Quantity
FROM
    Deal
		INNER JOIN
    Customer ON Deal.CustomerID = Customer.CustomerID
        RIGHT JOIN
    Warehouse ON Deal.InventoryID = Warehouse.InventoryID
        INNER JOIN
    Product ON Warehouse.ProductID = Product.ProductID;

# 4
SELECT DISTINCT
    Supplier.Name AS Supplier
FROM
    Warehouse
        JOIN
    Supplier ON Warehouse.SupplierID = Supplier.SupplierID;

# 5
SELECT * FROM Employee;
SELECT 
    Employee.Name AS Employee,
    IFNULL(Employer.Name, 'undefined') AS Employer
FROM
    Employee
        LEFT JOIN
    Employee Employer ON Employee.ManagerID = Employer.EmployeeID;
