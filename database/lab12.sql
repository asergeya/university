USE Deanery;

CREATE OR REPLACE VIEW exams_num AS
SELECT
	MajorID,
    Semester,
	COUNT(ID) AS cnt
FROM
	Subject
GROUP BY MajorID, Semester;

SELECT * FROM exams_num;

CREATE OR REPLACE VIEW exams AS
SELECT
	StudentID,
	CONCAT(S.Surname, ' ', S.Name, ' ', S.MiddleName) AS ФИО,
    G.MajorID,
    G.Name AS Группа,
    D.Semester AS Семестр,
    Grade
FROM
	Gradebook
		JOIN
	Student S ON Gradebook.StudentID = S.ID
		JOIN
	Subject D ON Gradebook.SubjectID = D.ID	
		JOIN
	Groupe G ON S.GroupID = G.ID
WHERE Grade IN ("Отлично", "Зачтено");

CREATE OR REPLACE VIEW v AS 
SELECT 
	ФИО, Группа, Семестр
FROM 
	exams
GROUP BY StudentID, Группа, Семестр
HAVING COUNT(Grade) IN (SELECT cnt FROM exams_num WHERE Semester = Семестр AND exams_num.MajorID = MajorID);

SELECT * FROM v;