USE Deanery;

DROP FUNCTION IF EXISTS GetMajorID;
DROP TRIGGER IF EXISTS Subject_PreventDeletion;
DROP TRIGGER IF EXISTS Groupe_UpdateHeadman;
DROP TRIGGER IF EXISTS Gradebook_InsertCheck;

DELIMITER //

CREATE TRIGGER Subject_PreventDeletion
BEFORE DELETE ON Subject 
FOR EACH ROW 
BEGIN
	IF OLD.ID IN (SELECT SubjectID FROM Gradebook) THEN
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'This subject has records in gradebook!';
	END IF;
END//

CREATE TRIGGER Groupe_UpdateHeadman
BEFORE UPDATE ON Groupe 
FOR EACH ROW 
BEGIN
	IF NEW.HeadmanID NOT IN (SELECT ID FROM Student WHERE GroupID = OLD.ID) THEN
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'Headman does not consist in this group!';
	END IF;
END//

CREATE FUNCTION GetMajorID(student_id INT)
RETURNS INT DETERMINISTIC
BEGIN
	DECLARE group_id INT;
    
	SELECT GroupID INTO group_id FROM Student WHERE ID = student_id;
    
	RETURN (SELECT MajorID FROM Groupe WHERE ID = group_id);
END//

CREATE TRIGGER Gradebook_InsertCheck
BEFORE INSERT ON Gradebook
FOR EACH ROW 
BEGIN
	IF NEW.SubjectID NOT IN (SELECT ID FROM Subject WHERE MajorID = GetMajorID(NEW.StudentID)) THEN
		SIGNAL SQLSTATE '45000' -- "unhandled user-defined exception"
		SET MESSAGE_TEXT = 'Student does not study this subject!';
	END IF;
END//

DELIMITER ;
;
# DELETE FROM Subject WHERE ID = 1;
UPDATE Groupe SET HeadmanID = 7 WHERE ID = 1;
#UPDATE Groupe SET HeadmanID = 9 WHERE ID = 1;
INSERT INTO Gradebook (StudentID, SubjectID, Date, Grade) 
	VALUES (1, 11, "2020-12-29", "Зачтено");