% mm
violet = [10.5 21.5 15   32.5];
red    = [17.5 32   24.5 45.5];
measurement = [12.5 25.5 19.5 38];
%measurement = [8.5 16.5 13 27]
d      = 0.01;
k1     = 1;
k2     = 2;
S1     = 200;
S2     = 300;

S      = S1;
for i = 1:4
  if (i > 2)
    S = S2;
  endif
  if (mod(i, 2) == 1)
    k = k1;
  else
    k = k2;
  endif
  % IMPORTANT: nm
  % Change violet
  length(i) = (d * measurement(i)) / (k * S) * 10^6;
endfor

length
average = sum(length) / 4
for i = 1:4
  averages(i) = length(i) - average;
endfor
averages
sum(averages)
d_average = sum(averages) / 4
e = d_average / 760 * 100

