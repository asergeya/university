#include <boost/program_options.hpp>
#include <Magick++.h>
#include <mpi.h>

#include <filesystem>
#include <iostream>
#include <chrono>
#include <vector>
#include <string>
#include <array>

#include <cstdint>
#include <climits>

#if SIZE_MAX == UCHAR_MAX
   #define M_MPI_SIZE_T MPI_UNSIGNED_CHAR
#elif SIZE_MAX == USHRT_MAX
   #define M_MPI_SIZE_T MPI_UNSIGNED_SHORT
#elif SIZE_MAX == UINT_MAX
   #define M_MPI_SIZE_T MPI_UNSIGNED
#elif SIZE_MAX == ULONG_MAX
   #define M_MPI_SIZE_T MPI_UNSIGNED_LONG
#elif SIZE_MAX == ULLONG_MAX
   #define M_MPI_SIZE_T MPI_UNSIGNED_LONG_LONG
#endif


constexpr std::size_t kernel_size = 5;
constexpr std::size_t kernels_num = 4;
constexpr std::array<std::array<std::array<double, kernel_size>, kernel_size>, kernels_num> array = {
        0,    0.3,    0,    0,     0,
       -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,

        0,    0.3,    0,    0,      0,
       -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,

        0,    0.3,    0,    0,      0,
        -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,

        0,    0.3,    0,    0,      0,
       -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
};

void processImage(int argc, char *argv[], const std::string &path, const std::array<std::array<std::array<double, kernel_size>, kernel_size>, kernels_num> &filter);

int main(int argc, char *argv[])
{
    using namespace boost::program_options;
    namespace fs = std::filesystem;

    std::string filename = "test.jpg";

    options_description desc{"Options"};
    desc.add_options()
        ("help,h", "Help message")
        ("thread,t", value<int>(), "thread num")
        ("file,f", value<std::string>(), "filename");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
        std::cout << desc << '\n';
    else {
        if (vm.count("file")) {
            filename = vm["file"].as<std::string>();

            if (not fs::exists(filename)) {
                std::cout << filename << " does not exist! Using test.jpg instead\n";
                return 1;
            }
        }
        try {
            processImage(argc, argv, filename, array);
        } catch (std::exception &e) {
            std::cerr << "Caught exception: " << e.what() << '\n';
            return 1;
        }
    }
}

void processImage(int argc, char *argv[], const std::string &path, const std::array<std::array<std::array<double, kernel_size>, kernel_size>, kernels_num> &filter)
{
    using namespace std::chrono;

    steady_clock::time_point start, end;
    Magick::Image image;
    MagickCore::Quantum *pixels;
    std::vector<int> pixels_value;
    std::vector<int> sums;
    std::size_t image_rows;
    std::size_t image_cols;
    std::size_t image_channels;
    std::size_t row_block;
    std::size_t row_start;
    std::size_t row_end;
    int process_num;
    int process_id;

    start = steady_clock::now();
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &process_num);
    MPI_Comm_rank(MPI_COMM_WORLD, &process_id);

    if (process_id == 0) {
        image.read(path);
        image_rows     = image.rows();
        image_cols     = image.columns();
        image_channels = image.channels();
        pixels         = image.getPixels(0, 0, image_cols, image_rows);
        pixels_value.insert(pixels_value.end(), &pixels[0], &pixels[image_rows*image_cols*image_channels - 1]);
    }
    MPI_Bcast(&image_rows, 1, M_MPI_SIZE_T, 0, MPI_COMM_WORLD);
    MPI_Bcast(&image_cols, 1, M_MPI_SIZE_T, 0, MPI_COMM_WORLD);
    MPI_Bcast(&image_channels, 1, M_MPI_SIZE_T, 0, MPI_COMM_WORLD);

    pixels_value.resize(image_rows*image_cols*image_channels);

    MPI_Bcast(pixels_value.data(), image_rows*image_cols*image_channels, MPI_INT, 0, MPI_COMM_WORLD);

    row_block = (image_rows+process_num-1) / process_num;
    row_start = row_block*process_id;
    row_end   = row_block * (process_id+1);
    if (row_end > image_rows) {
        row_end = image_rows;
    }
    sums.resize(row_block*process_num*image_cols*image_channels);

    auto local_size = row_block*image_cols*image_channels;
    std::vector<int> local_sums(local_size);
    for (std::size_t row = row_start; row < row_end; ++row) {
        for (std::size_t col = 0; col < image_cols; ++col) {
            for (std::size_t channel = 0; channel < image_channels; ++channel) {

                auto sum = 0;
                for (std::size_t i = 0; i < kernel_size; ++i) {
                    for (std::size_t j = 0; j < kernel_size; ++j) {
                        auto r = row + (i-1);
                        auto c = col + (j-1);

                        if (r < image_rows and c < image_cols) {
                            auto index = r*image_cols*image_channels + c*image_channels + channel;

                            sum += pixels_value[index] * filter[channel][i][j];
                        }
                    }
                }
                local_sums[(row-row_start)*image_cols*image_channels + col*image_channels + channel] = sum;
            }
        }
    }
    MPI_Gather(local_sums.data(), local_size, MPI_INT, sums.data(), local_size, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Finalize();
    end = steady_clock::now();

    if (process_id == 0) {
        for (std::size_t i = 0; i < image_rows*image_cols*image_channels; ++i) {
            pixels[i] = std::abs(sums[i]);
        }

        image.syncPixels();
        image.write("processed_" + path);

        std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;
    }
}