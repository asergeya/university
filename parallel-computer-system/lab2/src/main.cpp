#include <boost/program_options.hpp>
#include <Magick++.h>
#include <omp.h>

#include <filesystem>
#include <iostream>
#include <chrono>
#include <vector>
#include <string>
#include <array>

constexpr std::size_t kernel_size = 5;
constexpr std::size_t kernels_num = 4;
constexpr std::array<std::array<std::array<double, kernel_size>, kernel_size>, kernels_num> array = {
        0,    0.3,    0,    0,     0,
       -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,

        0,    0.3,    0,    0,      0,
       -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,

        0,    0.3,    0,    0,      0,
        -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,

        0,    0.3,    0,    0,      0,
       -0.2,  0,      0.2, -1,     -1.0,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
        0,    0.3,    0,    0.1,    0.2,
};

void processImage(const std::string &path, const std::array<std::array<std::array<double, kernel_size>, kernel_size>, kernels_num> &filter);

int main(int argc, char *argv[])
{
    using namespace boost::program_options;
    namespace fs = std::filesystem;

    std::string filename = "test.jpg";

    options_description desc{"Options"};
    desc.add_options()
        ("help,h", "Help message")
        ("thread,t", value<int>(), "thread num")
        ("file,f", value<std::string>(), "filename");

    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);

    if (vm.count("help"))
        std::cout << desc << '\n';
    else {
        if (vm.count("thread")) {
            auto thread_num = vm["thread"].as<int>();

            if (thread_num > 0 and thread_num <= omp_get_max_threads()) {
                omp_set_num_threads(thread_num);
            } else {
                std::cout << omp_get_max_threads() << " - num of max supported threads. Using it\n";
            }
        }
        if (vm.count("file")) {
            filename = vm["file"].as<std::string>();

            if (not fs::exists(filename)) {
                std::cout << filename << " does not exist! Using test.jpg instead\n";
                return 1;
            }
        }
        try {
            processImage(filename, array);
        } catch (std::exception &e) {
            std::cerr << "Caught exception: " << e.what() << '\n';
            return 1;
        }
    }
}

void processImage(const std::string &path, const std::array<std::array<std::array<double, kernel_size>, kernel_size>, kernels_num> &filter)
{
    using namespace std::chrono;

    steady_clock::time_point start, end;
    Magick::Image image(path);
    auto image_rows = image.rows();
    auto image_cols = image.columns();
    auto image_channels = image.channels();
    auto pixel = image.getPixels(0, 0, image_cols, image_rows);
    std::vector<double> sums(image_rows*image_cols*image_channels);

    start = steady_clock::now();
    #pragma omp parallel for collapse(3)
    for (std::size_t row = 0; row < image_rows; ++row) {
        for (std::size_t col = 0; col < image_cols; ++col) {
            for (std::size_t channel = 0; channel < image_channels; ++channel) {

                auto sum = 0;
                for (std::size_t i = 0; i < kernel_size; ++i) {
                    for (std::size_t j = 0; j < kernel_size; ++j) {
                        auto r = row + (i-1);
                        auto c = col + (j-1);

                        if (r < image_rows and c < image_cols) {
                            auto index = r*image_cols*image_channels + c*image_channels + channel;

                            sum += pixel[index] * filter[channel][i][j];
                        }
                    }
                }
                sums[row*image_cols*image_channels + col*image_channels + channel] = sum;
            }
        }
    }
    end = steady_clock::now();

    #pragma omp parallel for
    for (std::size_t i = 0; i < image_rows*image_cols*image_channels; ++i) {
        pixel[i] = std::abs(sums[i]);
    }

    image.syncPixels();
    image.write("processed_" + path);

    std::cout << "Time passed: " << duration_cast<milliseconds>(end - start).count() << " ms" << std::endl;
}