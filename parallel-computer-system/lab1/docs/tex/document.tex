\documentclass[utf8x, 14pt, bold, times]{G7-32} % Стиль (по умолчанию будет 14pt)

\include{preamble}
\include{listings}

\begin{document}

\frontmatter % выключает нумерацию ВСЕГО; здесь начинаются ненумерованные главы: реферат, введение, глоссарий, сокращения и прочее.

\include{info}
\include{mytitle}
\maketitle

\tableofcontents
\addtocontents{toc}{\vspace{1cm}}

\Introduction

\textbf{Цель работы:}
познакомиться с основными принципами, директивами и функциями OpenMP.

\textbf{Порядок выполнения работы:}

\begin{enumerate}
\item Написать последовательную программу на языке С++.
\item В программе предусмотреть замер времени выполнения вычислений (без учета
      времени ввода-вывода).
\item Выполнить компиляцию программы с отключенной оптимизацией для многоядерного
      процессора.
\item Провести несколько экспериментов с разными размерами исходных данных. 
      Зафиксировать время выполнения программы и загрузку процессора (скриншот
      диспетчера задач или его аналога).
\item Проанализировать программу, выделить фрагменты, пригодные для распараллеливания.
\item Распараллелить программу с использованием ТОЛЬКО директивы
      \mintinline{C++}{#pragma omp parallel}.
\item Выполнить компиляцию программы с отключенной оптимизацией для многоядерного
      процессора.
\item Кроме указанной директивы допускается использование функций библиотеки OpenMP
      для работы с переменными окружения.
\item Убедиться, что параллельная и последовательная версии программы выдают одинаковые
      результаты вычислений.
\item Выяснить, сколько потоков запускается в программе по умолчанию.
\item Задать различное количество потоков исполнения с помощью различных возможностей
      OpenMP.
\item Зафиксировать время выполнения программы при различных размерах обрабатываемых
      массивов и количестве нитей.
\end{enumerate}

\textbf{Задание:}

\begin{itemize}
\item инициализировать массивы $A$ и $B$ в соответствии с выражением (см. задание по варианту);
\item вычислить массив $С$ и переопределить массив $В$;
\item по требованию выполнить вывод всех массивов на экран (в файл);
\item предусмотреть возможность исполнения некоторых параллельных секций числом потоков,
      задаваемых переменной $k$;
\item при входе в параллельную секцию на экран выводить краткое описание секции
      число исполняющих нитей.
\end{itemize}

\textbf{Задание по варианту:}

\begin{enumerate}
\item Инициализировать массивы $A$ и $B$ в соответствии с выражением:
      \begin{equation}
      \begin{aligned}
      &a[0]=x;~a[i]=\frac{\sin(x\cdot i)}{i},~i=1\dots N, \\
      &b[0]=x^2;~b[i]=i\cdot b[i-1],~i=1\dots N.
      \end{aligned}
      \end{equation}
\item Выполнить вычисления:
      \begin{equation}
      \begin{aligned}
      &c[i]=a[i]\cdot b[N-i],~i=0\dots N, \\
      &b[i]=\frac{a[i]+c[i]}{i},~i=1\dots N.
      \end{aligned}
      \end{equation}
\end{enumerate}

\textbf{Входные данные:}

\begin{itemize}
\item[] $N$ "--- размер массивов;
\item[] $x$ "--- вводимый параметр;
\item[] $k$ "--- количество потоков.
\end{itemize}

\mainmatter % это включает нумерацию глав и секций в документе ниже

\chapter{Ход выполнения работы}
\nobreakingbeforechapters

\section{Сравнение производительности}

Все измерения будут проводиться на системе с процессором Intel I5-10210U.
У данного процессора 4 ядра, но имеется поддержка гиперпоточности, которую
мы обязательно проверим. Система работает под управлением GNU/Linux. При
измерениях на системе будут запущены сторонние процессы. Для измерения нагрузки
системы будет использоваться \textsl{htop}.

Проведем несколько запусков приложения без использования OpenMP. Будем постепенно
увеличивать размер массивов ($N$) и замерять время работы приложения. При каждом $N$
сделаем 5 прогонов. Использовать будем среднее значение. Полученные данные
представлены в таблице~\ref{tab:without-openmp}.

\begin{table}[H]
  \caption{Результаты запуска приложения без использования OpenMP}
  \begin{tblr}{|X[c,m,-1]|X[r,m,2]|X[r,m,2]|}
  \hline
  \SetCell[]{c} {№} &
  \SetCell[]{c} {Размер массива, $N$} &
  \SetCell[]{c} {Время выполнения приложения, мс} \\
  \hline
  1 & 1000      & 0    \\ \hline
  2 & 10000     & 0    \\ \hline
  3 & 100000    & 4    \\ \hline
  4 & 1000000   & 44   \\ \hline
  5 & 10000000  & 444  \\ \hline
  6 & 100000000 & 4593 \\ \hline
  \end{tblr}
  \label{tab:without-openmp}
\end{table}

Нагрузка системы представлена на рисунке~\ref{ris:load-without-openmp}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.9\linewidth]{figures/load-without-openmp}}
  \caption{Нагрузка системы при запуске приложения в последовательном режиме}
\label{ris:load-without-openmp}
\end{figure}

Результат работы приложения при некоторых прогонах представлен на
рисунке~\ref{ris:result-without-openmp}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.9\linewidth]{figures/result-without-openmp}}
  \caption{Результат работы приложения в последовательном режиме}
\label{ris:result-without-openmp}
\end{figure}

Теперь проведем те же самые замеры с включенным OpenMP, при этом меняя количество
используемых потоков. Полученные данные представлены в
таблице~\ref{tab:with-openmp}.

\begin{table}[H]
  \caption{Результаты запуска приложения с использованием OpenMP}
  \begin{tblr}{|X[c,m,-1]|X[r,m,2]|X[r,m,2]|}
  \hline
  \SetCell[]{c} {№} &
  \SetCell[]{c} {Размер массива, $N$} &
  \SetCell[]{c} {Время выполнения приложения, мс} \\
  \hline
  \SetCell[c=3]{c} \textbf{Два потока} & & \\ \hline
  1 & 1000      & 0     \\ \hline
  2 & 10000     & 0     \\ \hline
  3 & 100000    & 3     \\ \hline
  4 & 1000000   & 27    \\ \hline
  5 & 10000000  & 266   \\ \hline
  6 & 100000000 & 2655  \\ \hline
  \SetCell[c=3]{c} \textbf{Четыре потока} & & \\ \hline
  1 & 1000      & 0     \\ \hline
  2 & 10000     & 0     \\ \hline
  3 & 100000    & 3     \\ \hline
  4 & 1000000   & 20    \\ \hline
  5 & 10000000  & 172   \\ \hline
  6 & 100000000 & 1753  \\ \hline
  \SetCell[c=3]{c} \textbf{Восемь потоков (гиперпоточность)} & & \\ \hline
  1 & 1000      & 25    \\ \hline
  2 & 10000     & 7     \\ \hline
  3 & 100000    & 6     \\ \hline
  4 & 1000000   & 26    \\ \hline
  5 & 10000000  & 181   \\ \hline
  6 & 100000000 & 1647  \\ \hline
  \end{tblr}
  \label{tab:with-openmp}
\end{table}

Нагрузка системы представлена на рисунке~\ref{ris:load-with-openmp}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.9\linewidth]{figures/load-with-openmp}}
  \caption{Нагрузка системы при запуске приложения в параллельном режиме}
\label{ris:load-with-openmp}
\end{figure}

Результат работы приложения при некоторых прогонах представлен на
рисунке~\ref{ris:result-with-openmp}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.9\linewidth]{figures/result-with-openmp}}
  \caption{Результат работы приложения в параллельном режиме}
\label{ris:result-with-openmp}
\end{figure}

Сравнивая полученные результаты при больших $N$, видим, что производительность
увеличилась примерно в 2 раза при использовании 2 потоков и в 3 раза
при "--- 4 (4593 VS 2655 VS 1753). При малых $N$ время, затрачиваемое
на создание потоков, превышает получаемый выигрыш в производительности.
Особенно заметно при использовании максимального количества потоков системы.
Следовательно, если использовать распараллеливание в приложениях, то обязательно
нужно подбирать количество используемых потоков динамически (в runtime) от сложности
задачи (вычислений).


\section{Исходный код}

Код данной работы представлен ниже.
Он также продублирован на Gitlab. Ссылка на репозиторий:
\url{https://gitlab.com/asergeya/university/-/tree/master/parallel-system}

Рекомендуется собирать с GitLab'а.

\subsection{main.cpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/main.cpp}

\subsection{task.hpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/task.hpp}

\subsection{task.cpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/task.cpp}

\subsection{io.hpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/io.hpp}

\backmatter %% Здесь заканчивается нумерованная часть документа и начинаются ссылки и

\breakingbeforechapters 
\Conclusion

Во время выполнения работы было изучено, как использовать директивы OpenMP для
простого распараллеливания.

\end{document}
