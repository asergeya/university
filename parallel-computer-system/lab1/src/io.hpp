#ifndef IO_HPP
#define IO_HPP

#include <iostream>
#include <vector>
#include <string>
#include <limits>

template<typename Number>
Number getNumber(
    const std::string &message="",
    Number min=std::numeric_limits<Number>::min(),
    Number max=std::numeric_limits<Number>::max()
){
    Number input;

    do {
        if (not message.empty()) {
            std::cout << message;
        } 
        std::cin >> input;     
    } while (input < min or input > max);

    return input;
}

template<typename PrintableType>
void printArray(const std::vector<PrintableType> &vec, std::ostream &out, const std::string &sep=" ")
{
    for (const auto &item : vec) {
        out << item << sep; 
    }
    out << std::endl;
}

#endif // IO_HPP
