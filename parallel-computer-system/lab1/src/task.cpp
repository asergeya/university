#include "task.hpp"

#ifdef DEBUG
#include <iostream>
#include <iomanip>
#endif // DEBUG

#include <cmath>

extern ExecutionMode gExecutionMode;

// FIXME: devil copy-paste

void initFirstArray(std::vector<double> &a, double x)
{
    auto length = a.size();

    a[0] = x;

    if (gExecutionMode == ExecutionMode::Parallel) {

        auto block_size = length / omp_get_max_threads();

        if (length % omp_get_max_threads()) {
            ++block_size;
        }

        #pragma omp parallel
        {
            auto thread_num  = omp_get_thread_num();
            auto block_start = block_size*thread_num + 1;
            auto block_end   = (block_size * (thread_num+1)) + 1;

            if (block_end > length) {
                block_end = length;
            }

#ifdef DEBUG
            #pragma omp critical
            {
                std::cout << "initFirstArray(). Thread: " << thread_num << " "
                          << "block_start: " << std::setw(10) << block_start  << " "
                          << "block_end:   " << std::setw(10) << block_end    << '\n';
            }
#endif // DEBUG

            for (auto i = block_start; i < block_end; ++i) {
                a[i] = sin(x*i) / i;
            }
        }
    } else if (gExecutionMode == ExecutionMode::Sequential) {
        for (std::size_t i = 1; i < length; ++i) {
            a[i] = sin(x*i) / i;
        }
    }
}
void initSecondArray(std::vector<double> &b, double x)
{
    b[0] = x*x;

    for (std::size_t i = 1; i < b.size(); ++i) {
        b[i] = i * b[i-1];
    }
}

void calcFirst(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c)
{
    auto length = a.size();

    if (gExecutionMode == ExecutionMode::Parallel) {
        auto block_size = length / omp_get_max_threads();

        if (length % omp_get_max_threads()) {
            ++block_size;
        }

        #pragma omp parallel
        {
            auto thread_num  = omp_get_thread_num();
            auto block_start = block_size * thread_num;
            auto block_end   = block_size * (thread_num+1);

            if (block_end > length) {
                block_end = length;
            }

#ifdef DEBUG
            #pragma omp critical
            {
                std::cout << "calcFirst(). Thread: " << thread_num << " "
                          << "block_start: " << std::setw(10) << block_start  << " "
                          << "block_end:   " << std::setw(10) << block_end    << '\n';
            }
#endif // DEBUG

            for (auto i = block_start; i < block_end; ++i) {
                c[i] = a[i] * b[length-i-1];
            }
        }
    } else if (gExecutionMode == ExecutionMode::Sequential) {

        for (std::size_t i = 0; i < length; ++i) {
            c[i] = a[i] * b[length-i-1];
        }
    }
}

void calcSecond(const std::vector<double> &a, const std::vector<double> &c, std::vector<double> &b)
{
    auto length = a.size();

    if (gExecutionMode == ExecutionMode::Parallel) {

        auto block_size = length / omp_get_max_threads();

        if (length % omp_get_max_threads()) {
            ++block_size;
        }

        #pragma omp parallel
        {
            auto thread_num  = omp_get_thread_num();
            auto block_start = block_size*thread_num + 1;
            auto block_end   = (block_size * (thread_num+1)) + 1;

            if (block_end > length) {
                block_end = length;
            }

#ifdef DEBUG
            #pragma omp critical
            {
                std::cout << "calcSecond(). Thread: " << thread_num << " "
                          << "block_start: " << std::setw(10) << block_start  << " "
                          << "block_end:   " << std::setw(10) << block_end    << '\n';
            }
#endif // DEBUG

            for (auto i = block_start; i < block_end; ++i) {
                b[i] = (a[i]+c[i]) / i;
            }
        }
    } else if (gExecutionMode == ExecutionMode::Sequential) {
        for (std::size_t i = 1; i < length; ++i) {
            b[i] = (a[i]+c[i]) / i;
        }
    }
}
