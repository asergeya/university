#include "io.hpp" 

#include <omp.h>

#include <algorithm>
#include <fstream>
#include <chrono>
#include <vector>
#include <string>
#include <random>
#include <cmath>
#include <list>

const std::string kExecutionPrompt   = "Choose execution mode (0 - sequential, 1 - parallel):\n";
const std::string kSizePrompt        = "Enter size of array:\n";
const std::string kThreadsNumPrompt  = "Enter num of threads:\n";
const std::string kIntervalNumPrompt = "Enter interval num:\n";
const std::string kPrintResultPrompt = "Enable result output (0 - no, 1 - yes):\n";

int main()
{
    std::chrono::steady_clock::time_point start, end;
    auto size           = getNumber<long long>(kSizePrompt, 1);
    auto threads_num    = getNumber<int>(kThreadsNumPrompt, 1, omp_get_max_threads());
    auto interval_num   = getNumber<int>(kIntervalNumPrompt, 1);
    auto print_enable   = getNumber<int>(kPrintResultPrompt, 0, 1);
    std::vector<double> array(size);
    std::list<double> list;

    omp_set_num_threads(threads_num);
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(-1, 1); 

    for (std::size_t i = 0; i < array.size(); ++i) {
        array[i] = distribution(generator);
    }
    if (print_enable) {
        std::cout << "Array: ";
        printArray(array, std::cout);
    }

    auto task1_file = std::ofstream("task1.txt");
    if (print_enable) {
        std::cout << "\nTask 1:\n";
    }
    start = std::chrono::steady_clock::now();

    #pragma omp parallel for
    for (std::size_t i = 0; i < array.size(); ++i) {
        if (std::abs(array[i]) > 0.25) {
            if (print_enable) {
                #pragma omp critical
                {
                    task1_file << "Thread №" << omp_get_thread_num() << ": array[" << i << "]" << " = " << array[i] << '\n';
                    std::cout << "Thread №" << omp_get_thread_num() << ": array[" << i << "]" << " = " << array[i] << '\n';
                }
            }
        }
    }

    auto max = *std::max_element(array.cbegin(), array.cend());
    auto task2_file = std::ofstream("task2.txt");
    if (print_enable) {
        std::cout << "\nTask 2:\n";
    }
    
    #pragma omp parallel for
    for (std::size_t i = 0; i < array.size(); ++i) {
        if (array[i] >= 0.7*max) {
            #pragma omp critical
            list.push_back(array[i]);
            
            if (print_enable) {
                #pragma omp critical
                {
                    task2_file << "Thread №" << omp_get_thread_num() << ": array[" << i << "]" << " = " << array[i] << '\n';
                    std::cout << "Thread №" << omp_get_thread_num() << ": array[" << i << "]" << " = " << array[i] << '\n';
                }
            }
        }
    }

    std::vector<std::size_t> num_in_interval(interval_num, 0);
    auto interval_length  = 1.0 / interval_num;
    auto interval_start   = 0.0;
    auto interval_end     = 0.0;

    #pragma omp parallel for private(interval_start, interval_end)
    for (int i = 0; i < interval_num; ++i) {
        interval_start = interval_length * i;
        interval_end   = (i+1 != interval_num) ? interval_length * (i+1) : 2.0;

        for (const auto &value : array) {
            auto num = std::abs(value);

            if (num >= interval_start and num < interval_end) {
                ++num_in_interval[i];
            }
        }
    }
    if (print_enable) {
        auto task3_file = std::ofstream("task3.txt");
        auto close_bracket = ')';

        std::cout << "\nTask 3:\n";

        for (int i = 0; i < interval_num; ++i) {
            interval_start = interval_length * i;
            interval_end   = interval_length * (i+1);

            if (i+1 == interval_num) {
                interval_end = 1.0;
                close_bracket = ']';
            }
            task3_file << "[" << interval_start << ", " << interval_end << close_bracket << ": " << num_in_interval[i] << '\n';
            std::cout << "[" << interval_start << ", " << interval_end << close_bracket << ": " << num_in_interval[i] << '\n';
        }
    }
    end = std::chrono::steady_clock::now(); 

    std::cout << "Time passed: " 
              << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
              << " ms" << std::endl;
}
