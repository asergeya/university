\documentclass[utf8x, 14pt, bold, times]{G7-32} % Стиль (по умолчанию будет 14pt)

\include{preamble}
\include{listings}

\begin{document}

\frontmatter % выключает нумерацию ВСЕГО; здесь начинаются ненумерованные главы: реферат, введение, глоссарий, сокращения и прочее.

\include{info}
\include{mytitle}
\maketitle

\tableofcontents
\addtocontents{toc}{\vspace{1cm}}

\Introduction

\textbf{Цель работы:}
познакомиться с основными принципами, директивами и функциями OpenMP.

\textbf{Порядок выполнения работы:}

\begin{enumerate}
\item Проанализировать программу ЛР1 и определить, какие фрагменты программы
      целесообразно реализовать в виде секции секций.
\item Распараллелить программу с использованием директивы
      \mintinline{C++}{#pragma omp parallel sections}.
      Кроме указанной директивы допускается использование функций библиотеки 
      OpenMP для работы с переменными окружения.
\item Зафиксировать время выполнения программы при различных размерах обрабатываемых
      массивов.
\end{enumerate}

\textbf{Задание:}

\begin{itemize}
\item инициализировать массивы $A$ и $B$ в соответствии с выражением (см. задание по варианту);
\item вычислить массив $С$ и переопределить массив $В$;
\item по требованию выполнить вывод всех массивов на экран (в файл);
\item предусмотреть возможность исполнения некоторых параллельных секций числом потоков,
      задаваемых переменной $k$;
\item при входе в параллельную секцию на экран выводить краткое описание секции
      число исполняющих нитей.
\end{itemize}

\textbf{Задание по варианту:}

\begin{enumerate}
\item Инициализировать массивы $A$ и $B$ в соответствии с выражением:
      \begin{equation}
      \begin{aligned}
      &a[0]=x;~a[i]=\frac{\sin(x\cdot i)}{i},~i=1\dots N, \\
      &b[0]=x^2;~b[i]=i\cdot b[i-1],~i=1\dots N.
      \end{aligned}
      \end{equation}
\item Выполнить вычисления:
      \begin{equation}
      \begin{aligned}
      &c[i]=a[i]\cdot b[N-i],~i=0\dots N, \\
      &b[i]=\frac{a[i]+c[i]}{i},~i=1\dots N.
      \end{aligned}
      \end{equation}
\end{enumerate}

\textbf{Входные данные:}

\begin{itemize}
\item[] $N$ "--- размер массивов;
\item[] $x$ "--- вводимый параметр;
\item[] $k$ "--- количество потоков.
\end{itemize}

\mainmatter % это включает нумерацию глав и секций в документе ниже

\chapter{Ход выполнения работы}
\nobreakingbeforechapters

\section{Обоснование принятых решений}

В данной задачи использование секции секций нецелесообразно, потому что
у нас каждый поток решает \textsl{типовую} задачу (расчет независимого члена
последовательности), а секции секций необходимы для выполнения \textsl{различных} задач.
Хотя использование секции секций в этой задаче возможно. Исходя из вышесказанного,
было решено сымитировать функциональное распараллеливание в ущерб производительности.

\section{Сравнение производительности}

Все измерения будут проводиться на системе с процессором Intel I5-10210U.
У данного процессора 4 ядра, но имеется поддержка гиперпоточности, которую
мы обязательно проверим. Система работает под управлением GNU/Linux. При
измерениях на системе будут запущены сторонние процессы. Для измерения нагрузки
системы будет использоваться \textsl{htop}.

Проведем несколько запусков приложения. Будем постепенно увеличивать размер
массивов ($N$) и замерять время работы приложения. При каждом $N$
сделаем 5 прогонов. Использовать будем среднее значение. Полученные данные
представлены в таблице~\ref{tab:with-openmp}.

\begin{table}[H]
  \caption{Результаты запуска приложения с использованием OpenMP}
  \begin{tblr}{|X[c,m,-1]|X[r,m,2]|X[r,m,2]|}
  \hline
  \SetCell[]{c} {№} &
  \SetCell[]{c} {Размер массива, $N$} &
  \SetCell[]{c} {Время выполнения приложения, мс} \\
  \hline
  \SetCell[c=3]{c} \textbf{Два потока} & & \\ \hline
  1 & 1000      & 0     \\ \hline
  2 & 10000     & 0     \\ \hline
  3 & 100000    & 3     \\ \hline
  4 & 1000000   & 36    \\ \hline
  5 & 10000000  & 344   \\ \hline
  6 & 100000000 & 3585  \\ \hline
  \SetCell[c=3]{c} \textbf{Четыре потока} & & \\ \hline
  1 & 1000      & 0     \\ \hline
  2 & 10000     & 0     \\ \hline
  3 & 100000    & 4     \\ \hline
  4 & 1000000   & 36    \\ \hline
  5 & 10000000  & 393   \\ \hline
  6 & 100000000 & 3250  \\ \hline
  \SetCell[c=3]{c} \textbf{Восемь потоков (гиперпоточность)} & & \\ \hline
  1 & 1000      & 14    \\ \hline
  2 & 10000     & 4     \\ \hline
  3 & 100000    & 9     \\ \hline
  4 & 1000000   & 57    \\ \hline
  5 & 10000000  & 416   \\ \hline
  6 & 100000000 & 3329  \\ \hline
  \end{tblr}
  \label{tab:with-openmp}
\end{table}

Нагрузка системы представлена на рисунке~\ref{ris:load-with-openmp}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.9\linewidth]{figures/load-with-openmp}}
  \caption{Нагрузка системы при запуске приложения в параллельном режиме}
\label{ris:load-with-openmp}
\end{figure}

Результат работы приложения при некоторых прогонах представлен на
рисунке~\ref{ris:result-with-openmp}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.9\linewidth]{figures/result-with-openmp}}
  \caption{Результат работы приложения в параллельном режиме}
\label{ris:result-with-openmp}
\end{figure}

Сравнивая полученные результаты, видим, что производительность
практически не меняется. Все из-за неэффективного распараллеливания
программы. 

\section{Исходный код}

Код данной работы представлен ниже.
Он также продублирован на Gitlab. Ссылка на репозиторий:
\url{https://gitlab.com/asergeya/university/-/tree/master/parallel-system}

Рекомендуется собирать с GitLab’а.

\subsection{main.cpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/main.cpp}

\subsection{task.hpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/task.hpp}

\subsection{task.cpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/task.cpp}

\subsection{io.hpp}

\inputminted[fontsize=\footnotesize, breaklines]{cpp}{../../src/io.hpp}

\backmatter %% Здесь заканчивается нумерованная часть документа и начинаются ссылки и

\breakingbeforechapters 
\Conclusion

Во время выполнения работы было изучено, как использовать директивы OpenMP для
простого распараллеливания.

\end{document}
