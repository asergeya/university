#ifndef TASK_HPP
#define TASK_HPP

#include <vector>
#include <omp.h>

void initFirstArray(std::vector<double> &a, double x);
void initSecondArray(std::vector<double> &b, double x);
void calcFirst(const std::vector<double> &a, const std::vector<double> &b, std::vector<double> &c);
void calcSecond(const std::vector<double> &a, const std::vector<double> &c, std::vector<double> &b);

#endif // TASK_HPP
