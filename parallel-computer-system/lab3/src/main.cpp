#include "io.hpp" 
#include "task.hpp"

#include <chrono>
#include <vector>
#include <string>

const std::string kSizePrompt        = "Enter size of array:\n";
const std::string kXVarPrompt        = "Enter x:\n";
const std::string kPrintResultPrompt = "Enable result output (0 - no, 1 - yes):\n";
const std::string kThreadsNumPrompt  = "Enter num of threads:\n";

int main()
{
    std::chrono::steady_clock::time_point start, end;
    auto size           = getNumber<long long>(kSizePrompt, 1);
    auto x              = getNumber<double>(kXVarPrompt);
    auto print_result   = getNumber<int>(kPrintResultPrompt, 0, 1);
    auto threads_num    = getNumber<int>(kThreadsNumPrompt, 1, omp_get_max_threads());
    
    omp_set_num_threads(threads_num);

    std::vector<double> a(size);
    std::vector<double> b(size);
    std::vector<double> c(size);

    start = std::chrono::steady_clock::now(); 
    #pragma omp parallel
    {
        #pragma omp sections 
        {
            #pragma omp section
            initFirstArray(a, x);

            #pragma omp section
            initSecondArray(b, x);
        } // pragma omp sections
        
    } // pragmq omp parallel
    calcFirst(a, b, c);
    calcSecond(a, c, b);
    end = std::chrono::steady_clock::now(); 

    if (print_result) {
        printArray(a, std::cout);
        printArray(b, std::cout);
        printArray(c, std::cout);
    }
    std::cout << "Time passed: " 
              << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count()
              << " ms" << std::endl;
}
