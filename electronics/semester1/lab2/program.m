%Input data
N=1;
E=100*N;
R1=10*N;
%Attention: R2 needs to be changed! (100, 50, 20, 10, 5, 2, 1)
R2=[100*N 50*N 20*N 10*N 5*N 2*N 1*N];
R2_size=numel(R2);
%Calculation
for k=1:1:R2_size
  I(k)=E/(R1+R2(k));
  UR2(k)=I(k)*R2(k);
  PR2(k)=I(k)*UR2(k);
  PE(k)=E*I(k);
  kpd(k)=PR2(k)/PE(k);
end
%Output result
format bank;
tmp = [R2 I UR2 PR2 PE kpd];
reshape(tmp, R2_size, numel(tmp)/R2_size)