Uxx = 8.3550;
Ri = 36.695;
Rkopt = Ri;
R1 = (0.1:0.1:10)*Rkopt;
I1 = Uxx ./ (Ri + R1);
U1 = I1 .* R1;
P1 = I1 .* U1;

%{
plot(R1, I1, 'b', "linewidth", 5);
set(gca, 'FontSize', 20)
xlabel("R1, Ом");
ylabel("I1, А");
grid on;
%}
%{
plot(R1, U1, 'r', "linewidth", 5);
set(gca, 'FontSize', 20)
xlabel("R1, Ом");
ylabel("U1, В");
grid on;
%}
plot(R1, P1, 'g', "linewidth", 5);
set(gca, 'FontSize', 20)
xlabel("R1, Ом");
ylabel("P1, Вт");
grid on;
