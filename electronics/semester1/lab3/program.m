% Variant
N = 1;
% Input data
R1 = 100*N;
R2 = 150*N;
R3 = 200*N;
E1 = 300*N;
E2 = 50*N;

% Kirchhoff's circuit laws
A = [1 -1 -1; R1 R2 0; R1 0 R3];
B = [0; E1; E1-E2]

% Calculation
I = A\B
U1 = R1*I(1,1)
U2 = R2*I(2,1)
U3 = R3*I(3,1)

% Checking balance of powers
PG = I(1,1)*E1 - I(3,1)*E2
PN = (I(1,1)^2 * R1) + (I(2,1)^2 * R2) + (I(3,1)^2 * R3)
