w = 2 * pi * 50;
L = 0;
R = 2;
C = 6 * 10^(-3);
atand((w*L-1/(w*C))/R);

R = 0:0.1:10;
%C = 0.006:-0.0001:0;
for k=1:1:numel(R)
  phi(k) = atand((-1/(w*C))/R(k));
end

plot(R, phi, 'b', "linewidth", 5);
set(gca, 'FontSize', 20)
xlabel("R, Ом");
ylabel("φ, град");
grid on;