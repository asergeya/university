w = 2 * pi * 50;
C = 0;
%atand((w*L-1/(w*C))/R);
%{
L = 0.002;
R = 2:-0.01:0;
for k=1:1:numel(R)
  phi(k) = atand((w*L)/R(k));
end
%}

R = 2;
L = 0:0.001:0.02;
for k=1:1:numel(L)
  phi(k) = atand((w*L(k))/R);
end

plot(L, phi, 'r', "linewidth", 5);
set(gca, 'FontSize', 20);
xlabel("L, Гн");
ylabel("φ, град");
grid on;

L=0.002;
R=0.1;
phi = atand((w*L)/R)