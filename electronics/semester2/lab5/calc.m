E0 = 0.6;
Ek = 15;
B = 200;
R1 = 18000;
R2 = 10000;
Re = 4300;
Rk = 5100;
Rn = 50000;

Vt = 0.025;

# Анализ для постоянной составляющей

Eb = R2*Ek / (R1 + R2)
Rb = R1*R2 / (R1 + R2)

# Ток базы
Ib = (Eb - E0) / (Rb + Re*(B+1))
# Ток коллектора
Ik_lazy = B*Ib;
Ik = (B*(Eb - E0)) / (Rb + Re*(B+1))
# Ток эммитера
Ie = Ik + Ib

Uk = Rk*Ik; # Ek-RkIk
Uk = Ek - Rk*Ik;
Ue = Re*Ie;

Uke = Uk - Ue

# Анализ для переменной составляющей
gm = Ik / Vt
rpi = B / gm
re = Vt / Ie

R12 = R1*R2 / (R1+R2)

Ku = -gm * (Rk*Rn) / (Rk+Rn)
Ri = (rpi*R12) / (rpi+R12)
Ro = (Rk*Rn) / (Rk+Rn)
