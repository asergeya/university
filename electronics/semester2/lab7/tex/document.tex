\documentclass[utf8x, 14pt, bold, times]{G7-32} % Стиль (по умолчанию будет 14pt)

\include{preamble}

\begin{document}

\frontmatter % выключает нумерацию ВСЕГО; здесь начинаются ненумерованные главы: реферат, введение, глоссарий, сокращения и прочее.

\include{info}
\include{mytitle}
\maketitle

\tableofcontents
\addtocontents{toc}{\vspace{1cm}}


\Introduction

\textbf{Цель работы:}
исследование статических и динамических характеристик логического
инвертора на биполярном транзисторе.

\textbf{Предварительный расчет:}

Схема инвертора на биполярном транзисторе, нагруженного на L
инверторы того же типа, представлена на рисунке \ref{ris:task-scheme}.
Источником сигнала является инвертор, аналогичный проектируемому.

Исходные данные приведены в таблице \ref{tab:task-data}. Необходимо
рассчитать:

\begin{enumerate}
\item Сопротивление нагрузки в коллекторной цепи $R_\textup{к}$.
\item Сопротивление резистора в цепи базы $R_\textup{б}$.
\item Статическую мощность, потребляемую инвертором.
\end{enumerate}

А также построить передаточную характеристику инвертора.

\iffalse
А также:
\begin{enumerate}
\item Построить передаточную характеристику инвертора для двух случаев:
    \begin{enumerate}
    \item инвертор работает в режиме холостого хода;
    \item инвертор нагружен на L аналогичных ключей.
    \end{enumerate}
\end{enumerate}
\fi

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.5\linewidth]{figures/task-scheme}}
    \caption{Схема инвертора на биполярном транзисторе}
\label{ris:task-scheme}
\end{figure}

\begin{table}[H]
    \caption{Исходные данные}
    \begin{tabular}{|*{3}{c|}}
    \hline
    Вариант & $E_\textup{к}$, В & L \\
    \hhline{|*{3}{=}|}
    1 & 10 & 3 \\
    \hline
    \end{tabular}
    \label{tab:task-data}
\end{table}

\mainmatter % это включает нумерацию глав и секций в документе ниже

\chapter{Предварительный расчет}
\nobreakingbeforechapters

Сопротивление резистора $R_\textup{к}$ найдем по формуле:

\begin{equation}
    R_\textup{к} = \frac{E_\textup{к}-U_\textup{кэ нас}}{I_\textup{нас}} \\[1em]
\end{equation}

Напряжение коллектор-эмиттер в режиме насыщения $U_\textup{кэ нас}$ примем
равным 0.2~В, ток коллектора в режиме насыщения $I_\textup{нас}$ для транзистора
Q2N3904 можно считать равным 10~мА. Получим:

\begin{equation}
    R_\textup{к} = \frac{10-0.2}{0.01} = 980~\textup{Ом}\\[1em]
\end{equation}

Для того чтобы определить сопротивление резистора в цепи базы $R_\textup{б}$, сначала
необходимо найти ток базы $I_\textup{б}$. Величину коэффициента насыщения $Q$ примем
равной 1.5, коэффициент $\beta=100$. Получим:

\begin{equation}
    I_\textup{б} = Q\frac{I_\textup{нас}}{\beta} = 1.5\frac{0.01}{100} = 150~\textup{мкА} \\[1em]
\end{equation}

Теперь уже рассчитаем сопротивление самого резистора $R_\textup{б}$, приняв
напряжение логической единицы $U_\textup{вх}^1$ равным $\frac{E_\textup{к}}{2}$:

\begin{equation}
    R_\textup{б} \approx \frac{U_\textup{вх}^1-0.7}{I_\textup{б}}
                 \approx \frac{5-0.7}{0.00015} \approx 28.7~\textup{кОм} \\[1em]
\end{equation}

Найдем статическую мощность биполярного инвертора без нагрузки:

\begin{equation}
    P = 0.5EI_\textup{к нас} \approx 0.5\frac{E_\textup{к}^2}{R_\textup{к}}
      \approx 0.5\frac{10^2}{980} \approx 0.05~\textup{Вт} \\[1em]
\end{equation}

Построим передаточную характеристику инвертора в режиме холостого хода:
\begin{itemize}
\item Транзистор находится в режиме отсечки, если 
      $U_\textup{вх}~=~U_\textup{вх}^0~<~0.7~\textup{В}$.
      При этом напряжение на выходе 
      $U_\textup{вых}=U_\textup{вых}^1 \approx 10~\textup{В}$.
\item Транзистор находится в состоянии насыщения, если эмиттерный и коллекторный
      переходы смещены в прямом направлении. При этом 
      $U_\textup{кэ нас}~=~U_\textup{вых}^0~=~0.2~\textup{В}$,
      $I_\textup{б}~=~\frac{I_\textup{нас}}{\beta}~=\frac{0.01}{100} = 10^{-4}$~А.
      Тогда:
      
      \begin{equation}
          U_\textup{вх}^1=R_\textup{б}I_\textup{б} + U_\textup{бэ}
                         = 28700 \cdot 10^{-4} + 0.7 = 3.57~\textup{В} 
      \end{equation}
\end{itemize}

Итак:

\begin{equation}
\begin{aligned}
    &U_\textup{вх}^0 = 0.7~\textup{В}, U_\textup{вх}^1 = 3.57~\textup{В} \\
    &U_\textup{вых}^1 = 10~\textup{В}, U_\textup{вых}^0 = 0.2~\textup{В} \\[1em]
\end{aligned}
\end{equation}

Полученная передаточная характеристика представлена 
на рисунке~\ref{ris:transfer-pre-graph}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.5\linewidth]{figures/transfer-pre-graph}}
    \caption{Передаточная характеристика инвертора в режиме холостого хода}
\label{ris:transfer-pre-graph}
\end{figure}

\iffalse
Построим передаточную характеристику инвертора, нагруженного на L аналогичных ключей:
\fi

\chapter{Ход работы}

\section{Исследование характеристик инвертора в режиме холостого хода}

Соберем схему инвертора на биполярном транзисторе без нагрузки в NI Multisim.
Полученная схема представлена на рисунке~\ref{ris:multisim-scheme-1}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.6\linewidth]{figures/multisim-scheme-1}}
    \caption{Схема инвертора на биполярном транзисторе без нагрузки в NI Multisim}
\label{ris:multisim-scheme-1}
\end{figure}

С помощью утилиты \textsl{DC Sweep} построим передаточную характеристику
инвертора. Она представлена на рисунке \ref{ris:transfer-graph}.
Диапазон изменения входного напряжения от 0 до $E_\textup{к}$.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/transfer-graph}}
    \caption{Передаточная характеристика инвертора в режиме холостого хода}
\label{ris:transfer-graph}
\end{figure}

Заметим, что передаточная характеристика не сильно отличается от полученной в
предварительном расчете.

По графику передаточной характеристики определим уровни логических нуля и
единицы, а также оценим помехоустойчивость инвертора.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/transfer-graph-levels}}
    \caption{Передаточная характеристика инвертора в режиме холостого хода}
\label{ris:transfer-graph-levels}
\end{figure}

Уровни логических нуля и единицы:

\begin{equation}
\begin{aligned}
    &U_\textup{вх}^0 \approx 0.6~\textup{В}, U_\textup{вх}^1 \approx 2.5~\textup{В} \\
    &U_\textup{вых}^1 \approx 10~\textup{В}, U_\textup{вых}^0 \approx 0.3~\textup{В} \\[1em]
\end{aligned}
\end{equation}


Помехоустойчивость инвертора:

\begin{equation}
\begin{aligned}
    &U_\textup{вх}^0 - U_\textup{вых}^0 = 0.6 - 0.3 = 0.3~\textup{В} \\
    &U_\textup{вых}^1 - U_\textup{вх}^1 = 10 - 2.5 = 7.5~\textup{В} \\[1em]
\end{aligned}
\end{equation}

График входного и выходного напряжений представлен на
рисунке \ref{ris:u-graph}. Оранжевая линия "--- выходное напряжение,
красная "--- входное.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/u-graph}}
    \caption{График входного и выходного напряжений в режиме холостого хода}
\label{ris:u-graph}
\end{figure}

График тока коллектора и мгновенной мощности, отдаваемой источником,
представлен на рисунке \ref{ris:i-p-graph}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/i-p-graph}}
    \caption{График тока коллектора и мгновенной мощности в режиме холостого хода}
\label{ris:i-p-graph}
\end{figure}

\section{Исследование характеристик инвертора с нагрузкой}

Соберем схему инвертора на биполярном транзисторе с нагрузкой в NI Multisim.
Полученная схема представлена на рисунке~\ref{ris:multisim-scheme-2}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.6\linewidth]{figures/multisim-scheme-2}}
    \caption{Схема инвертора на биполярном транзисторе с нагрузкой в NI Multisim}
\label{ris:multisim-scheme-2}
\end{figure}

С помощью утилиты \textsl{DC Sweep} построим передаточную характеристику
инвертора. Она представлена на рисунке \ref{ris:transfer-graph-2}.
Диапазон изменения входного напряжения от 0 до $E_\textup{к}$.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/transfer-graph-2}}
    \caption{Передаточная характеристика инвертора в динамическом режиме}
\label{ris:transfer-graph-2}
\end{figure}

По графику передаточной характеристики определим уровни логических нуля и
единицы, а также оценим помехоустойчивость инвертора.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/transfer-graph-2-levels}}
    \caption{Передаточная характеристика инвертора в динамическом режиме}
\label{ris:transfer-graph-2-levels}
\end{figure}

Уровни логических нуля и единицы:

\begin{equation}
\begin{aligned}
    &U_\textup{вх}^0 \approx 0.6~\textup{В}, U_\textup{вх}^1 \approx 2.5~\textup{В} \\
    &U_\textup{вых}^1 \approx 9~\textup{В}, U_\textup{вых}^0 \approx 0.3~\textup{В} \\[1em]
\end{aligned}
\end{equation}

График входного и выходного напряжений представлен на
рисунке \ref{ris:u-graph-2}. Оранжевая линия "--- выходное напряжение,
красная "--- входное.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/u-graph-2}}
    \caption{График входного и выходного напряжений в динамическом режиме}
\label{ris:u-graph-2}
\end{figure}

Время включения $t_\textup{вкл}$ складывается из времени задержки и
длительности фронта:

\begin{equation}
    t_\textup{вкл}=t_\textup{з}+t_\textup{ф} \approx 20~\textup{нс} + 150~\textup{нс} = 170~\textup{нс}
\end{equation}

Время выключения $t_\textup{выкл}$ складывается из времени рассасывания и
времени спада коллекторного тока:

\begin{equation}
    t_\textup{выкл}=t_\textup{з}+t_\textup{ф} \approx 7~\textup{нс} + 20~\textup{нс} = 27~\textup{нс}
\end{equation}

График тока коллектора и мгновенной мощности, отдаваемой источником,
представлен на рисунке \ref{ris:i-p-graph-2}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.8\linewidth]{figures/i-p-graph-2}}
    \caption{График тока коллектора и мгновенной мощности в динамическом режиме}
\label{ris:i-p-graph-2}
\end{figure}


\backmatter %% Здесь заканчивается нумерованная часть документа и начинаются ссылки и

\breakingbeforechapters 

\Conclusion

Проверил и убедился, что:
\begin{itemize}
\item У нагруженного инвертора уменьшается быстродействие.
\end{itemize}

\end{document}
