#include <iostream>
#include <algorithm>

void sort(int arr[], int len, bool asc);
int * read_array(int len);
void print_array(const int arr[], int len);
bool is_element_in_array(const int arr[], int len, int key);

int main(void)
{
    int * a = nullptr;
    int * b = nullptr;
    int * c = nullptr;
    int a_length = 0;
    int b_length = 0;
    int c_length = 0;
    int c_max_length = 0;
    bool isAscending = true;
    
    std::cout << "Enter size of first array:\n";
    std::cout << "=> ";
    std::cin >> a_length;
    std::cout << "Enter size of second array:\n";
    std::cout << "=> ";
    std::cin >> b_length;
    std::cout << "How to sort (1 - for ascending, else - 0):\n";
    std::cout << "=> ";
    std::cin >> isAscending;
    a = read_array(a_length);
    b = read_array(b_length);

    std::cout << "First array:\n";
    print_array(a, a_length);
    sort(a, a_length, isAscending);
    std::cout << "Sorted:\n";
    print_array(a, a_length);
    std::cout << "\nSecond array:\n";
    print_array(b, b_length);
    sort(b, b_length, isAscending);
    std::cout << "Sorted:\n";
    print_array(b, b_length);

    c_max_length = std::min(a_length, b_length);
    c = new int[c_max_length];
    for (int i = 0; i < a_length; i++) {
        if (is_element_in_array(b, b_length, a[i]) && !is_element_in_array(c, c_length, a[i])) {
                c[c_length] = a[i];
                c_length++;
        }
    }
    std::cout << "Third array:\n";
    print_array(c, c_length);

    delete[] a;
    delete[] b;
    delete[] c;

    return 0;
}

void sort(int arr[], int len, bool asc)
{
    for (int i = 0; i < len-1; i++) {
        for (int j = i + 1; j < len; j++) {
            if (asc) {
                if (arr[j] < arr[i]) {
                    std::swap(arr[j], arr[i]);
                }
            }
            else {
                if (arr[j] > arr[i]) {
                    std::swap(arr[j], arr[i]);
                }
            }
        }
    }
}

int * read_array(int len)
{
    int * arr = new int[len];

    std::cout << "Enter " << len << " elements:\n";
    std::cout << "=> ";
    for (int i = 0; i < len; i++) {
        std::cin >> arr[i];
    }
    return arr;
}

void print_array(const int arr[], int len) 
{
    for (int i = 0; i < len; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}

bool is_element_in_array(const int arr[], int len, int key)
{
    for (int i = 0; i < len; i++) {
        if (key == arr[i]) {
            return true;
        }
    }
    return false;
}
