#include <cstdio>

double f(double x);

int main(void)
{
    double a, b, s;

    printf("Enter a and b:\n");
    printf("=> ");
    scanf("%lf %lf", &a, &b);
    printf("Enter step:\n");
    printf("=> ");
    scanf("%lf", &s);

    for (double x = a; x <= b; x += s) {
        printf("%15.3lf", x);
    }
    putchar('\n');
    for (double x = a; x <= b; x += s) {
        printf("%15.5e", f(x));
    }
    putchar('\n');

    return 0;
}

double f(double x)
{
    return 5*x + 2;
}
