#include <cstdio>

double f(double x);

int main(void)
{
    double a, b, s;

    printf("Enter a and b:\n");
    printf("=> ");
    scanf("%lf %lf", &a, &b);
    printf("Enter step:\n");
    printf("=> ");
    scanf("%lf", &s);

    for (double x = a; x <= b; x += s) {
        printf("%10lf %15e\n", x, f(x));
    }

    return 0;
}

double f(double x)
{
    return 2*x*x - 3*x + 1;
}
