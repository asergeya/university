#include <cstdio>
#include <cmath>

double f(double x);

static const double pi = acos(-1);

int main(void)
{
    double a, b, s;
    double scale;
    
    printf("Enter a and b:\n");
    printf("=> ");
    scanf("%lf %lf", &a, &b);
    printf("Enter s:\n");
    printf("=> ");
    scanf("%lf", &s);
    printf("Enter scale:\n");
    printf("=> ");
    scanf("%lf", &scale);
 
    double y;
    int nspace;
    for (double x = a; x <= b; x += s) {
        y = f(x);
        nspace = scale * y;
        for (int i = 0; i < nspace; i++) {
            putchar(' ');
        }
        printf("$(%lf, %lf)\n", x, y);
    }
    
    return 0;
}

double f(double x)
{
    return sin(pi*x)*sin(pi*x);
}
