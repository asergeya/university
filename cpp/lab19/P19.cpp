/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
 */
#include <iostream>

int LeftBound(int * arr, int left_bound, int right_bound, int key);
int BinarySearch(int * arr, int n, int key);

int main()
{
    int * a;
    int n;
    int key;
    
    std::cout << "Enter size of array:\n";
    std::cout << "=> ";
    std::cin >> n;

    a = new int[n];
    
    std::cout << "Enter " << n << " elements:\n";
    for (int i = 0; i < n; i++) {
        std::cin >> a[i];
    }
    std::cout << "Enter key:\n";
    std::cout << "=> ";
    while (std::cin >> key) { 
        std::cout << BinarySearch(a, n, key) << '\n';
        std::cout << "Enter key:\n";
        std::cout << "=> ";
    }

    return 0;
}

int LeftBound(int * arr, int left_bound, int right_bound, int key)
{
    int middle = left_bound + (right_bound - left_bound) / 2;

    if (right_bound < left_bound) {
        return left_bound; 
    }
    if (arr[middle] >= key) {
        return LeftBound(arr, left_bound, middle - 1, key);
    }
    else {
        return LeftBound(arr, middle + 1, right_bound, key);
    }
}

int BinarySearch(int * arr, int n, int key)
{
    int potential_first_index = LeftBound(arr, 0, n-1, key);

    if (potential_first_index < n and arr[potential_first_index] == key) {
        return potential_first_index;
    }
    return -1;
}
