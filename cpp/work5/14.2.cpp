#include <cstdio>

double power(double x, int p);

int main(void)
{
    int n;
    double x, product = 1;
    
    printf("Enter n:\n");
    printf("=> ");
    scanf("%d", &n);
    printf("Enter x:\n");
    printf("=> ");
    scanf("%lf", &x);

    for (int i = 1; i <= n; i++) {
        product *= 2 * power(x, i);
    }
    printf("%.1lf\n", product);

    return 0;
}

double power(double x, int p)
{
    double return_value = 1;

    if (p > 0) {
        for (int i = 0; i < p; i++) {
            return_value *= x;
        }
    }
    else if (p < 0) {
        for (int i = 0; i > p; i--) {
            return_value /= x;
        }
    }

    return return_value;
}


