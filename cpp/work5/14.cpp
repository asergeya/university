#include <cstdio>

double power(double x, int p);

int main(void)
{
    int n;
    double x, t, product = 1;
    
    printf("Enter n:\n");
    printf("=> ");
    scanf("%d", &n);
    printf("Enter x:\n");
    printf("=> ");
    scanf("%lf", &x);
    
    t = x;
    for (int i = 1; i <= n; i++) {
        product *= 2 * t;
        t *= x;
    }
    printf("%.1lf\n", product);

    return 0;
}

