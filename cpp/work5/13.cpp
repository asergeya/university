#include <cstdio>

double ai(int i);

int main(void)
{
    int n;
    double sum = 0;

    printf("Enter n:\n");
    printf("=> ");
    scanf("%d", &n);

    for (int i = 1; i <= n; i++) {
        sum += ai(i);
    }
    printf("%.2lf\n", sum);

    return 0;
}

double ai(int i)
{
    return double(i) / (i + 1);
}
