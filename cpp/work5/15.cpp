#include <cstdio>

int sum_minus_plus(int n);

int main(void)
{
    int n;

    printf("Enter n:\n");
    printf("=> ");
    scanf("%d", &n);
    
    printf("%d\n", sum_minus_plus(n));

    return 0;
}

int sum_minus_plus(int n)
{
    int sum = 0, sign = -1;

    for (int i = 1; i <= n; i++) {
        sum += i * sign;
        sign *= -1;
    }

    return sum;
}

