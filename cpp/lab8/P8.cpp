#include <iostream>
#include <cstdlib>

int * read_array(int len);
void print_array(const int arr[], int len);
int my_task(const int A[], int lenA, int B[], int lenB);
int get_index_first_positive_element(const int a[], int n);

int main(void)
{
    int * a = nullptr;
    int * b = nullptr;
    int lengthA;
    int lengthB;

    std::cout << "Enter size of array:" << std::endl;
    std::cout << "=> ";
    std::cin >> lengthA;
    std::cout << "Enter array (" << lengthA << " elements):" << std::endl;
    std::cout << "=> ";
    a = read_array(lengthA);
   
    b = (int *) malloc(sizeof(int) * lengthA);
    lengthB = my_task(a, lengthA, b, lengthA);
    print_array(b, lengthB); 
    
    free(a);
    free(b);

    return 0;
}

int * read_array(int len)
{
    int * a = (int *) malloc(sizeof(int) * len);

    for (int i = 0; i < len; i++) {
        std::cin >> a[i];
    }
    return a;
}

void print_array(const int arr[], int len)
{
    for (int i = 0; i < len; i++) {
        std::cout << "[" << i << "] = " << arr[i] << std::endl;
    }
}

int my_task(const int A[], int lenA, int B[], int lenB)
{
    int badi = get_index_first_positive_element(A, lenA);
    int j = 0;

    for (int i = 0; i < lenA; i++) {
        if (i != badi) {
            B[j] = A[i];
            j++;
        }    
    }
    return j;
}

int get_index_first_positive_element(const int a[], int n)
{
    for (int i = 0; i < n; i++) {
        if (a[i] > 0) {
            return i;
        }
    }
    return -1;
}
