#include <iostream>
#include <fstream>

int ** make2d(size_t nrows, size_t ncols, int val);
void print2d(const int * const * arr, size_t nrows, size_t ncols);
void print_fliplr(const int * const * arr, size_t nrows, size_t ncols, std::ostream &ost);

int main(void)
{
    int ** array2d = nullptr;
    size_t nrow;
    size_t ncol;
    std::ofstream fout;

    std::cout << "Enter number of rows:\n";
    std::cout << "=> ";
    std::cin >> nrow;
    std::cout << "Enter number of cols:\n";
    std::cout << "=> ";
    std::cin >> ncol;

    array2d = make2d(nrow, ncol, 0);
    print2d(array2d, nrow, ncol);
    fout.open("fout.txt", std::ios::out);
    print_fliplr(array2d, nrow, ncol, fout);
    fout.close();

    for (size_t i = 0; i < nrow; i++) {
        delete[] array2d[i];
    }
    delete[] array2d;

    return 0;
}

int ** make2d(size_t nrows, size_t ncols, int val)
{
    int ** a = new int * [nrows];
    for (size_t i = 0; i < nrows; i++) {
        a[i] = new int[ncols];
    }

    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            a[i][j] = ++val;
        }
    }

    return a;
}

void print2d(const int * const * arr, size_t nrows, size_t ncols) 
{
    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            std::cout.width(4);
            std::cout << arr[i][j] << " ";
        } 
        std::cout << '\n';
    }
}

void print_fliplr(const int * const * arr, size_t nrows, size_t ncols, std::ostream &ost)
{
    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            ost.width(4);
            ost << arr[i][ncols-j-1] << " ";
        } 
        ost << '\n';
    }
}

