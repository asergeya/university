#include <ios>
#include <iostream>
#include <fstream>

void print2d(const int * const * arr, size_t nrows, size_t ncols, std::ostream &ost);
int ** make2d(size_t nrows, size_t ncols);

int main(void)
{
    int ** a;
    int nrow, ncol;
    std::ofstream fout;

    std::cout << "Enter number of row:\n";
    std::cout << "=> ";
    std::cin >> nrow;
    std::cout << "Enter number of col:\n";
    std::cout << "=> ";
    std::cin >> ncol;
    
    a = make2d(nrow, ncol);
    fout.open("fout.txt", std::ios::out);
    print2d(a, nrow, ncol, fout);
    fout.close();

    for (int i = 0; i < nrow; i++) {
        delete[] a[i];
    }
    delete[] a;

    return 0;
}

void print2d(const int * const * arr, size_t nrows, size_t ncols, std::ostream &ost)
{
    for (int i = 0; i < nrows; i++) {
        for (int j = 0; j < ncols; j++) {
            ost.width(4);
            ost << arr[i][j] << " ";
        }
        ost << '\n';
    }
}

int ** make2d(size_t nrows, size_t ncols)
{
    int ** a = new int * [nrows];
    for (int i = 0; i < nrows; i++) {
        a[i] = new int[ncols];
    }
    int val = 0;

    for (int i = 0; i < nrows; i++) {
        for (int j = 0; j < ncols; j++) {
            a[i][j] = ++val;
        }
    }

    return a;
}
