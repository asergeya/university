#include <iostream>
#include <fstream>

int ** make2d(size_t nrows, size_t ncols);
void print2d(const int * const * arr, size_t nrows, size_t ncols);
void print_transposed(const int * const * arr, size_t size, std::ostream &ost);

int main(void)
{
    int ** array2d = nullptr;
    std::size_t n;
    std::ofstream fout;

    std::cout << "Enter number of n:\n";
    std::cout << "=> ";
    std::cin >> n;

    array2d = make2d(n, n);
    std::cout << "Before transpose\n";
    print2d(array2d, n, n);
    fout.open("fout.txt", std::ios::out);
    print_transposed(array2d, n, fout);
    fout.close();

    for (std::size_t i = 0; i < n; i++) {
        delete[] array2d[i];
    }
    delete[] array2d;

    return 0;
}

int ** make2d(size_t nrows, size_t ncols)
{
    int val = 0;
    int ** a = new int * [nrows];
    for (size_t i = 0; i < nrows; i++) {
        a[i] = new int[ncols];
    }

    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            a[i][j] = ++val;
        }
    }

    return a;
}

void print2d(const int * const * arr, size_t nrows, size_t ncols)
{
    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            std::cout.width(4);
            std::cout << arr[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void print_transposed(const int * const * arr, size_t size, std::ostream &ost)
{
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            ost.width(4);
            ost << arr[j][i] << " ";
        }
        ost << '\n';
    }    
}
