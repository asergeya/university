#include <iostream>
#include <fstream>
#include <string>
#include <vector>

void rev_lines(std::istream & ist, std::ostream & ost);

int main(void)
{
    std::ofstream fout;
    std::ifstream fin;

    fin.open("fin.txt", std::ios::in);
    fout.open("fout.txt", std::ios::out);
    rev_lines(fin, fout);
    fin.close();
    fout.close();

    return 0;
}

void rev_lines(std::istream & ist, std::ostream & ost)
{
    std::string buf;
    std::vector<std::string> str;

    while (std::getline(ist, buf)) {
        str.insert(str.begin(), buf);
    }
    for (auto i : str) {
        ost << i << '\n';
    }
}
