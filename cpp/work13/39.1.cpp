#include <iostream>
#include <fstream>
#include <string>

void wrap_text(std::istream & ist, std::ostream & ost, int max_line_len);

int main(void)
{
    int maxlen;
    std::ofstream fout;
    std::ifstream fin;
    
    std::cout << "Enter max len:\n";
    std::cout << "=> ";
    std::cin >> maxlen;

    fin.open("fin.txt", std::ios::in);
    fout.open("fout.txt", std::ios::out);
    wrap_text(fin, fout, maxlen);
    fin.close();
    fout.close();

    return 0;
}

void wrap_text(std::istream & ist, std::ostream & ost, int max_line_len)
{
    std::string text("");
    std::string buf;
    int len = 0;
    
    while (std::getline(ist, buf)) {
        text.append(buf);
        text.append("\n");
    }
    text.erase(text.length()-1);
    
    for (int i = 0; text[i]; i++) {
        ost << text[i];
        if (text[i] != '\n') {
            len++;
        }
        else {
            len = 0;
        }
        if (len == max_line_len) {
            len = 0;
            ost << '\n';
        }
    }
    
    std::cout << text << std::endl;
}
