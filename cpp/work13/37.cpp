#include <iostream>
#include <fstream>
#define SIZE 255

void join(const char * sep, std::istream & ist, std::ostream & ost);

int main(void)
{
    char sep[SIZE];
    std::ofstream fout;
    std::ifstream fin;

    std::cout << "Enter separators:\n";
    std::cout << "=> ";
    std::cin.getline(sep, SIZE);

    fin.open("fin.txt", std::ios::in);
    fout.open("fout.txt", std::ios::out);
    //join(sep, std::cin, std::cout);
    join(sep, fin, fout);
    fin.close();
    fout.close();

    return 0;
}

void join(const char * sep, std::istream & ist, std::ostream & ost)
{
    char str[SIZE];
    
    if (ist >> str) {
        ost << str;
        while (ist >> str) {
            ost << sep << str;
        }
        ost << '\n';
    }
}

