#include <cstdio>
#include <cmath>

double function1(double x);
double function2(double x);
double sum(double a, double b);

int main(void)
{
    double x;
    double f1, f2;

    puts("Enter x:");
    printf("=> ");
    scanf("%lf", &x);
    
    f1 = function1(x);
    f2 = function2(x);
    printf("%lf\n", sum(f1, f2));

    return 0;
}

double function1(double x)
{
    return 10.0 / (x*x + 1);
}

double function2(double x)
{
    double five_root = 100 * sin(x*x*M_PI/180);

    if (five_root < 0) {
        five_root = -1 * pow(-1*five_root, 1.0/5.0);
    }
    else {
        five_root = pow(five_root, 1.0/5.0);
    }
    
    return five_root + 300000;
}

double sum(double a, double b)
{
    return a + b;
}
