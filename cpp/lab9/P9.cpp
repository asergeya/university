#include <iostream>
#include <cstdlib>

int * read_array(int n);
void print_array(const int * a, int n);
int my_task(const int * A, int lenA, int *& B);
int get_index_first_positive_element(const int * a, int n);

int main(void)
{
    int * a = nullptr;
    int * b = nullptr;
    int lengthA;
    int lengthB;

    std::cout << "Enter size of array:" << std::endl;
    std::cout << "=> ";
    std::cin >> lengthA; 
    std::cout << "Enter array (" << lengthA << " elements):" << std::endl;
    std::cout << "=> ";
    a = read_array(lengthA);
    
    lengthB = my_task(a, lengthA, b);
    if (lengthB != 0) {
        print_array(b, lengthB);
    }
    else {
        std::cerr << "We can't create new array.\n" 
                  << "There aren't positive elements in A array!" << std::endl;
    }
    
    free(a);
    free(b);

    return 0;
}

int * read_array(int n)
{
    int * a = (int *) malloc(sizeof(int) * n);

    for (int i = 0; i < n; i++) {
        std::cin >> *(a + i);
    }
    return a;
}

void print_array(const int * a, int n)
{
    for (int i = 0; i < n; i++) {
        std::cout << "[" << i << "] = " << *(a + i) << std::endl;
    }
}

int my_task(const int * A, int lenA, int *& B)
{
    int badi = get_index_first_positive_element(A, lenA);
    int lenB = 0; 
    
    if (badi != -1) {
        B = (int *) malloc(sizeof(int) * (lenA-1));
        for (int i = 0; i < lenA; i++) {
            if (i != badi) {
                *(B + lenB) = *(A + i);
                lenB++;
            }
        }
    }
    return lenB;
}

int get_index_first_positive_element(const int * a, int n)
{
    for (int i = 0; i < n; i++) {
        if (*(a + i) > 0) {
            return i;
        }
    }
    return -1;
}
