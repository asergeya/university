#include <iostream>
#include <cstring>
#define SIZE 255

char * last_dir(char * s);

int main(void)
{
    char str[SIZE];
    char * dir;

    std::cout << "Enter path:\n";
    std::cout << "=> ";
    std::cin >> str;
    
    dir = last_dir(str);
    std::cout << dir << std::endl;

    delete[] dir;

    return 0;
}

char * last_dir(char * s)
{
    char * dirname = new char[SIZE];
    char * temp = new char[SIZE];
    int k = 0, num = 0;
    
    temp[0] = '\0';
    strcpy(dirname, "\\");
    for (int i = 0; s[i] != '\0'; i++) {
        if (num == 2) {
            temp[k] = '\0';
            strcpy(dirname, temp);
            temp[0] = '\0';
            k = 0; num = 1;
        }
        if (s[i] == '\\' || s[i] == '/') {
            num += 1;
            continue;
        }
        if (num) {
            temp[k++] = s[i];
        }
    }
    delete[] temp;

    return dirname;
}
