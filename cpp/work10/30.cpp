#include <iostream>
#include <cstring>
#define SIZE 255

size_t join(char * s_out, size_t len, const char * const * s_in,
                 size_t n_in, const char * sep);

int main(void)
{
    char ** words;
    int nword;
    char separator[SIZE];
    char * out = nullptr;
    size_t length;
    
    std::cout << "Enter max lenght:\n";
    std::cout << "=> ";
    std::cin >> length;
    std::cout << "Enter numbers of words:\n";
    std::cout << "=> ";
    std::cin >> nword;
    std::cin.ignore();

    words = new char * [nword];
    for (int i = 0; i < nword; i++) {
        words[i] = new char[SIZE];
    }

    std::cout << "Enter " << nword << " strings\n";
    for (int i = 0; i < nword; i++) {
        std::cout << "=> ";
        std::cin.getline(words[i], SIZE);
    }
    std::cout << "Enter separator's string:\n";
    std::cout << "=> ";
    std::cin.getline(separator, SIZE);

    out = new char[length+1];
    
    std::cout << join(out, length, words, nword, separator) << std::endl;

    for (int i = 0; i < nword; i++) {
        delete[] words[i];
    }
    delete[] words;

    std::cout << out << std::endl;

    delete[] out;

    return 0;
}


size_t join(char * s_out, size_t len, const char * const * s_in,
                 size_t n_in, const char * sep)
{
    size_t top = 0;
    
    for (size_t i = 0; i < n_in; i++) {
        for (size_t j = 0; s_in[i][j] && top < len; j++) {
            s_out[top] = s_in[i][j];
            top++;
        }
        for (size_t k = 0; sep[k] && top < len && i != n_in-1; k++) {
            s_out[top] = sep[k];
            top++;
        }
    }
    s_out[top] = '\0';

    return top;
}
