#include <iostream>
#include <cstring>
#define SIZE 255

void rstrip(char * s, const char * chars);
bool is_element(const char * s, char key);

int main(void)
{
    char str[SIZE];
    char badSymbols[SIZE];

    std::cout << "Enter string:\n";
    std::cout << "=> ";
    std::cin.getline(str, SIZE);
    std::cout << "Enter string which consist of bad symbols:\n";
    std::cout << "=> ";
    std::cin.getline(badSymbols, SIZE);
    
    rstrip(str, badSymbols);
    std::cout << "The string after rstrip function:\n";
    std::cout << str << std::endl;

    return 0;
}

void rstrip(char * s, const char * chars)
{
    bool can_remove;
    
    for (int i = strlen(s)-1; i >= 0; i--) {
        can_remove = false;
        for (int j = 0; chars[j] != '\0'; j++) {
            if (chars[j] == s[i]) {
                s[i] = '\0';
                can_remove = true;
            }
        }
        if (!can_remove) {
            break;
        }
    }
}
