#include <iostream>
#include <cstring>
#define SIZE 255

void rstrip(char * s, const char * chars);
bool is_element(const char * s, char key);

int main(void)
{
    char str[SIZE];
    char badSymbols[SIZE];

    std::cout << "Enter string:\n";
    std::cout << "=> ";
    std::cin.getline(str, SIZE);
    std::cout << "Enter string which consist of bad symbols:\n";
    std::cout << "=> ";
    std::cin.getline(badSymbols, SIZE);
    
    rstrip(str, badSymbols);
    std::cout << "The string after rstrip function:\n";
    std::cout << str << std::endl;

    return 0;
}

void rstrip(char * s, const char * chars)
{
    bool can_remove;

    for (int j = 0; chars[j] != '\0'; j++) {
        while (is_element(chars, s[0])) {
            for (int k = 0; s[k] != '\0'; k++) {
                s[k] = s[k+1];
            }
        }
    }
    for (int i = strlen(s)-1; i >= 0; i--) {
        can_remove = false;
        for (int j = 0; chars[j] != '\0'; j++) {
            if (chars[j] == s[i]) {
                s[i] = '\0';
                can_remove = true;
            }
        }
        if (!can_remove) {
            break;
        }
    }
}

bool is_element(const char * s, char key)
{
    for (int i = 0; s[i] != '\0'; i++) {
        if (key == s[i]) {
            return true;
        }
    }
    return false;
}
