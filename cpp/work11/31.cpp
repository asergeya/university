#include <iostream>

int ** make2d(std::size_t nrows, std::size_t ncols, int val);
void print2d(int ** a, std::size_t nrows, std::size_t ncols);

int main(void)
{
    int ** array2d = nullptr;
    std::size_t nrow;
    std::size_t ncol;
    int val;

    std::cout << "Enter number of rows:\n";
    std::cout << "=> ";
    std::cin >> nrow;
    std::cout << "Enter number of cols:\n";
    std::cout << "=> ";
    std::cin >> ncol;
    std::cout << "Enter value for each element of array:\n";
    std::cin >> val;

    array2d = make2d(nrow, ncol, val);
    print2d(array2d, nrow, ncol);

    for (std::size_t i = 0; i < nrow; i++) {
        delete[] array2d[i];
    }
    delete[] array2d;

    return 0;
}

int ** make2d(std::size_t nrows, std::size_t ncols, int val)
{
    int ** a = new int * [nrows];
    for (std::size_t i = 0; i < nrows; i++) {
        a[i] = new int[ncols];
    }
    for (std::size_t i = 0; i < nrows; i++) {
        for (std::size_t j = 0; j < ncols; j++) {
            a[i][j] = val;
        }
    }

    return a;

}
void print2d(int ** a, std::size_t nrows, std::size_t ncols)
{
    for (std::size_t i = 0; i < nrows; i++) {
        for (std::size_t j = 0; j < ncols; j++) {
            std::cout << a[i][j] << " ";
        }
        std::cout << std::endl;
    }
}
