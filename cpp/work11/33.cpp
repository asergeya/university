#include <iostream>
#include <algorithm>

int ** make2d(size_t nrows, size_t ncols);
void print2d(int ** a, size_t nrows, size_t ncols);
void transpose(int ** arr, size_t size);

int main(void)
{
    int ** array2d = nullptr;
    std::size_t n;

    std::cout << "Enter number of n:\n";
    std::cout << "=> ";
    std::cin >> n;

    array2d = make2d(n, n);
    std::cout << "Before transpose\n";
    print2d(array2d, n, n);
    transpose(array2d, n);
    std::cout << "After transpose\n";
    print2d(array2d, n, n);

    for (std::size_t i = 0; i < n; i++) {
        delete[] array2d[i];
    }
    delete[] array2d;

    return 0;
}

int ** make2d(size_t nrows, size_t ncols)
{
    int val = 0;
    int ** a = new int * [nrows];
    for (size_t i = 0; i < nrows; i++) {
        a[i] = new int[ncols];
    }

    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            a[i][j] = ++val;
        }
    }

    return a;

}

void print2d(int ** a, size_t nrows, size_t ncols)
{
    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            std::cout.width(4);
            std::cout << a[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void transpose(int ** arr, size_t size) 
{

    for (size_t i = 0; i < size; i++) {
        for (size_t j = i+1; j < size; j++) {
            std::swap(arr[i][j], arr[j][i]);
        }
    }
    /*
    int ** temp = new int * [size];
    for (size_t i = 0; i < size; i++) {
        temp[i] = new int[size];
    }

    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            temp[i][j] = arr[j][i];
        }
    }
    
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j++) {
            arr[i][j] = temp[i][j];
        }
    }

    for (size_t i = 0; i < size; i++) {
        delete[] temp[i];
    }
    delete[] temp;
    */
}
