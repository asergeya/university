#include <iostream>
#include <algorithm>

int ** make2d(size_t nrows, size_t ncols, int val);
void print2d(int ** a, size_t nrows, size_t ncols);
void fliplr(int ** arr, size_t nrows, size_t ncols);

int main(void)
{
    int ** array2d = nullptr;
    size_t nrow;
    size_t ncol;

    std::cout << "Enter number of rows:\n";
    std::cout << "=> ";
    std::cin >> nrow;
    std::cout << "Enter number of cols:\n";
    std::cout << "=> ";
    std::cin >> ncol;

    array2d = make2d(nrow, ncol, 0);
    print2d(array2d, nrow, ncol);
    fliplr(array2d, nrow, ncol);
    std::cout << std::endl;
    print2d(array2d, nrow, ncol);

    for (size_t i = 0; i < nrow; i++) {
        delete[] array2d[i];
    }
    delete[] array2d;

    return 0;
}

int ** make2d(size_t nrows, size_t ncols, int val)
{
    int ** a = new int * [nrows];
    for (size_t i = 0; i < nrows; i++) {
        a[i] = new int[ncols];
    }
    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            a[i][j] = ++val;
        }
    }

    return a;

}

void print2d(int ** a, size_t nrows, size_t ncols)
{
    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols; j++) {
            std::cout.width(4);
            std::cout << a[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

void fliplr(int ** arr, size_t nrows, size_t ncols)
{
    for (size_t i = 0; i < nrows; i++) {
        for (size_t j = 0; j < ncols/2; j++) {
            std::swap(arr[i][j], arr[i][ncols-j-1]);
        } 
    }
}

