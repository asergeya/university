/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#define NUM_OF_INSERTIONS 3

struct Node {
    int data;
    Node * next;
};

Node * insert_node(Node * head, Node * prev, Node * n);
void push_node(Node * head, int new_data);
void print_list(const Node * first, std::ostream & ost); 
bool is_prime(int num);
Node * find_prime_num(Node * head);
int generate_num(int high_bound);

int main(void)
{
    std::srand(std::time(nullptr));
    Node * head = new Node;
    int num;
    
    head->next = nullptr;
    if (std::cin >> num) {
        head->data = num;
    }
    while (std::cin >> num) {
        push_node(head, num); 
    } 

    print_list(head, std::cout);
    
    Node * prime = find_prime_num(head);
    if (prime != nullptr) {
        for (int i = 0; i < NUM_OF_INSERTIONS; i++) {
            Node * temp = new Node;
            
            temp->data = generate_num(prime->data);
            head = insert_node(head, prime, temp);
        }
    }
    
    std::cout << '\n';
    print_list(head, std::cout);

    while (head != nullptr) {
        Node * tmp = head;

        head = head->next;
        delete tmp;
    }

    return 0;
}

Node * insert_node(Node * head, Node * prev, Node * n)
{
    if (prev == nullptr) {
        n->next = head;
        
        return n;
    }
    n->next = prev->next;
    prev->next = n;
    
    return head;
}

void push_node(Node * head, int new_data)
{
    Node * current = head;
    
    while (current->next != nullptr) {
        current = current->next;
    }
    
    current->next = new Node;
    current->next->data = new_data;
    current->next->next = nullptr;
}

void print_list(const Node * first, std::ostream & ost) 
{
    const Node * current = first;

    while (current != nullptr) {
        ost << current->data << '\n';
        current = current->next;
    }
}

bool is_prime(int num)
{
    if (num <= 1) {
        return false;
    }
    for (int i = 2; i * i <= num; i++) {
        if (num % i == 0) {
            return false;
        }
    }
    return true;
}

Node * find_prime_num(Node * head)
{
    Node * current = head;

    while (current != nullptr and not is_prime(current->data)) {
        current = current->next;
    }
    return current;
}

int generate_num(int high_bound)
{
    return std::rand() % high_bound + 1;
}
