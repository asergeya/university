#include <iostream>
#include <fstream>
#include <string>

struct Date {
    int day;
    int month;
    // 20XY, 0 <= XY <= 99
    int year; 

    bool check(void) 
    {
        int days_in_months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        bool is_leap_year;
        // Is leap year?-------------------------------------------------------
        if (year % 4 == 0) {
            days_in_months[1] = 29;
        }
        //---------------------------------------------------------------------
        if (0 <= year && year <= 99) {
            if (1 <= month && month <= 12) {
                if (1 <= day && day <= days_in_months[month-1]) {
                    return true;
                }
            }
        }
        return false;
    }

    int numofdays(void)
    { 
        int days_in_months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int num = 365*year + day + year/4;
        
        if (year % 4 == 0) {
            if (month >= 3) {
                num++;
            }
        }
        else {
            num++;
        }
        for (int i = 0; i <= month-2; i++) {
            num += days_in_months[i]; 
        }
        return num;
    }

    bool initdays(int num)
    {
        int days_in_months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int i = 0;

        if (num <= 0 || num >= 36526) {
            return false;
        }
        year = 0;
        month = 0;
        do {
            if (year % 4 == 0) {
                days_in_months[1] = 29;
            }
            else {
                days_in_months[1] = 28;
            }
            num -= days_in_months[month++];
            if (month == 12) {
                year++;
                month = 0;
            }
        } while (num > 0);
        day = num + days_in_months[(month-1+12) % 12];
        if (month == 0) {
            month = 12;
            year--;
        }
        return true;
    }

    bool init(int d, int m, int y)
    {
        day = d;
        month = m;
        year = y;

        if (check()) {
            return true;
        }
        return false;
    }

    void print(std::ostream & out) 
    {
        if (day <= 9) {
            out << '0';
        }
        out << day << ".";
        if (month <= 9) {
            out << '0';
        }
        out << month << ".";
        if (year <= 9) {
            out << '0';
        }
        out << year << '\n';
    }
};

void my_task(std::istream & ist, std::ostream & ost);
void test_function(void);

int main(void)
{
    std::ofstream fout;
    std::ifstream fin;

    test_function();

    fin.open("fin.txt", std::ios::in);
    fout.open("fout.txt", std::ios::out);
    my_task(fin, fout);
    fin.close();
    fout.close();

    return 0;
}

void my_task(std::istream & ist, std::ostream & ost)
{
    Date cur, prev;
    std::string buf;
    int d, m, y;
    int n;

    ist >> buf;
    d = std::stoi(buf);
    m = std::stoi(buf.substr(3,2));
    y = std::stoi(buf.substr(6,2));
    cur.init(d, m, y);
    cur.print(std::cout);

    std::cout << "Enter number of day:\n";
    std::cout << "=> ";
    std::cin >> n;
    prev.initdays(cur.numofdays() - n);
    prev.print(ost);
    prev.print(std::cout);
}

void test_function(void) 
{
    Date x;

    for (int i = 1; i <= 36525; i++) {
        x.initdays(i);
        if (x.numofdays() != i || !x.check()) {
            std::cerr << "Mistake!\n";
            std::cerr << "i = " << i << " numofdays() = " << x.numofdays() << '\n';
        }
    }
}
