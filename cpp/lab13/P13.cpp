#include <iostream>
#include <fstream>

int ** make2d(size_t n);
void print2d(const int * const * a, size_t n, std::ostream &ost);

int main(void)
{
    int ** a;
    size_t n;
    std::ofstream fout;

    std::cout << "Enter n:\n";
    std::cout << "=> ";
    std::cin >> n;
    
    a = make2d(n);
    fout.open("fout.txt", std::ios::out);
    print2d(a, n, fout);
    fout.close();

    for (size_t i = 0; i < n; i++) {
        delete[] a[i];
    }
    delete[] a;

    return 0;
}


int ** make2d(size_t n) 
{
    int ** a = new int * [n];
    for (size_t i = 0; i < n; i++) {
        a[i] = new int[n];
    }

    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < n; j++) {
            if (j < n-i-1) {
                a[i][j] = 0;
            }
            else if (j == n-i-1) {
                a[i][j] = 1;
            }
            else {
                a[i][j] = 2;
            }
        }
    }

    return a;
}
void print2d(const int * const * a, size_t n, std::ostream &ost) {
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < n; j++) {
            std::cout.width(4);
            std::cout << a[i][j] << " ";
            
            ost.width(4);
            ost << a[i][j] << " ";
        }
        std::cout << std::endl;
        ost << '\n';
    }
}
