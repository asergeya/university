#include <iostream>
#include <cmath>

int main(void)
{
    int num_seats, num_free_seats;
    int num_buses, num_people;

    std::cout << "Enter numbers of people:\n";
    std::cout << "=> ";
    std::cin >> num_people;
    std::cout << "Enter numbers of seats in a bus:\n";
    std::cout << "=> ";
    std::cin >> num_seats;
    
    //num_buses = ceil(double(num_people) / num_seats);
    num_buses = (num_people + num_seats - 1) / num_seats;
    num_free_seats = (num_seats * num_buses) - num_people;

    std::cout << "You need " << num_buses << " bus/es\n";
    std::cout << "You have " << num_free_seats << " free seats\n";

    return 0;
}
