#include <iostream>
#include <cmath>

int main(void)
{
    double x;

    std::cout << "Enter float number:\n";
    std::cout << "=> ";
    std::cin >> x;
    std::cout << "First digit after point is " << abs(int(x*10) % 10) << std::endl;

    return 0;
}
