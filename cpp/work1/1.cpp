#include <iostream>

void swap1(int *a, int *b);
void swap2(int *a, int *b);

int main(void)
{
    int a, b;
    
    std::cout << "Enter first number:\n";
    std::cout << "=> ";
    std::cin >> a;
    std::cout << "Enter second number:\n";
    std::cin >> b;
    
    std::cout << "a = " << a << "\nb = " << b << '\n';
    std::cout << "Swaping a and b\n";
    swap1(&a, &b);
    std::cout << "a = " << a << "\nb = " << b << '\n';
    std::cout << "Swaping a and b again\n";
    swap2(&a, &b);
    std::cout << "a = " << a << "\nb = " << b << '\n';

    return 0;
}

void swap1(int *a, int *b)
{
    *a ^= *b;
    *b ^= *a;
    *a ^= *b;
}

void swap2(int *a, int *b)
{
    *a = *a + *b;
    *b = *a - *b;
    *a = *a - *b;
}
