#include <cstdio>

int main(void)
{
    int h1, min1;
    int h2, min2;
    int lenH, lenM, duration;

    printf("Enter hours and minutes:\n");
    printf("=> ");
    scanf("%d %d", &h1, &min1);
    printf("Enter hours and minutes:\n");
    printf("=> ");
    scanf("%d %d", &h2, &min2);

    if (h2 < h1) {
        duration = (24-h1+h2)*60-min1+min2;
    }
    else {
        duration = (h2*60+min2) - (h1*60+min1);
    }
    lenH = duration / 60;
    lenM = duration % 60;
    
    if (lenM < 10) {
        printf("Duration is %d:0%d\n", lenH, lenM);
    }
    else {
        printf("Duration is %d:%d\n", lenH, lenM);
    }

    return 0;
}

        
