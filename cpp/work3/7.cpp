#include <cstdio>

int find_most_frequent(int, int, int, int);

int main(void)
{
    int a, b, c, d;
    
    printf("Enter a, b, c, d:\n");
    printf("=> ");
    scanf("%d %d %d %d", &a, &b, &c, &d);
    printf("%d\n", find_most_frequent(a, b, c, d));

    return 0;
}

int find_most_frequent(int a, int b, int c, int d)
{
    int sum;

    sum = a+b+c+d;
    
    if (sum == 2) {
        return -1;
    }
    else if (sum > 2) {
        return 1;
    }
    else {
        return 0;
    }
}
