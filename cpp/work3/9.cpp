#include <cstdio>

float min_or_max(float a, float b, float c) 
{
    if (a > 0.5) {
        if (b > c) {
            return b;
        }
        else {
            return c;
        }
    }
    else {
        if (b < c) {
            return b;
        }
        else {
            return c;
        }
    }
}

int main(void)
{
    float a, b, c;

    printf("Enter a, b, c:\n");
    printf("=> ");
    scanf("%f %f %f", &a, &b, &c);

    printf("%lf\n", min_or_max(a, b, c));

    return 0;
}

