#include <cstdio>
#include <cmath>

float dist(float x1, float y1, float x2, float y2);

int main(void)
{   
    float x1, y1;
    float x2, y2;

    printf("Enter (x1, y1):\n");
    printf("=> ");
    scanf("%f %f", &x1, &y1);
    printf("Enter (x2, y2):\n");
    printf("=> ");
    scanf("%f %f", &x2, &y2);

    printf("%.2f\n", dist(x1, y1, x2, y2));

    return 0;
}

float dist(float x1, float y1, float x2, float y2)
{
    return pow((pow(x2-x1, 2.0) + pow(y2-y1, 2.0)), 1.0/2.0);
}

