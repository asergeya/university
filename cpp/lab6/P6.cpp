#include <cstdio>
#include <cmath>

double cos_row(double x, double b);
long long factorial(int n);

int main(void)
{
    double accuracy, x;
    double value_cos, value_cos_row, diff;

    printf("Enter x:\n");
    printf("=> ");
    scanf("%lf", &x);
    printf("Enter accuracy:\n");
    printf("=> ");
    scanf("%lf", &accuracy);

    value_cos_row = cos_row(x*M_PI/180, accuracy);
    value_cos = cos(x*M_PI/180);
    diff = fabs(value_cos - value_cos_row);
    
    printf("Value of cos row: %.10lf\n", value_cos_row);
    printf("Value of cos from cmath: %.10lf\n", value_cos);
    printf("Diff is %.10lf\n", diff);

    return 0;
}

double cos_row(double x, double b)
{
    double cosv = 1;
    double t = x*x, a;
    int i = 2, sign = -1;
    
    do {
        a = t/factorial(i);
        cosv += sign * a;
        sign *= -1;
        i += 2;
        t *= x*x;
    } while (fabs(a) >= b);

    return cosv;
}

long long factorial(int n)
{
    long long f = 1;

    for (int i = 2; i <= n; i++) {
        f *= i;
    }

    return f;
}
