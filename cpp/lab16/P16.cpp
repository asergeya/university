/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
 */
#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>
#define MAX_LEN 256

enum roles {ADMIN, MODERATOR, REGULAR_USER};

struct user_permissions {
    char surname[MAX_LEN];
    char login[MAX_LEN];
    char password[MAX_LEN];
    roles role;
};

void print_data(const user_permissions * data, size_t len, std::ostream & ost);
bool compare_two_records(user_permissions a, user_permissions b);
void save_bin(const user_permissions * data, size_t len, std::ostream & ost);
void save_bin_task(const user_permissions * data, size_t len, std::ostream & ost);
user_permissions * load_txt(std::istream &ist, size_t & len);
user_permissions * load_bin(std::istream &ist, size_t & len);

int main()
{
    user_permissions * data;
    size_t len, num_records;
    int mode;
    std::ifstream txt_file;
    std::fstream task_file;
        
    
    std::cout << "Choose mode (1, 2):\n";
    std::cout << "=> ";
    std::cin >> mode;

    switch (mode) {
        case 1:
            txt_file.open("user_permissions.txt");
            task_file.open("data.dat", std::ios::binary|std::ios::out); 

            data = load_txt(txt_file, len);
            print_data(data, len, std::cout);
            num_records = len / sizeof(user_permissions);
            std::sort(data, data + num_records, compare_two_records);
            save_bin_task(data, len, task_file);
            
            txt_file.close();
            task_file.close();
            break;
        case 2:
            task_file.open("data.dat", std::ios::binary|std::ios::in); 
            data = load_bin(task_file, len);
            print_data(data, len, std::cout);

            task_file.close();
            break;
    }

    delete[] data;
    
    return 0;
}

void print_data(const user_permissions * data, size_t len, std::ostream & ost)
{
    size_t num_records = len / sizeof(user_permissions);
    
    for (int i = 0; i < num_records; i++) {
        ost << data[i].surname << '\n';
        ost << data[i].login << '\n';
        ost << data[i].password << '\n';
        //ost << data[i].role << '\n';
        switch (data[i].role) {
            case 0:
                ost << "Admin\n";
                break;
            case 1:
                ost << "Moderator\n";
                break;
            case 2:
                ost << "Regular user\n";
                break;
        }
        ost << '\n';
    }
}

bool compare_two_records(user_permissions a, user_permissions b)
{
    if (strcmp(a.surname, b.surname) <= 0) {
        return true;
    }
    return false;
}

void save_bin(const user_permissions * data, size_t len, std::ostream & ost)
{
    size_t num_records = len / sizeof(user_permissions);
    
    ost.seekp(0, std::ios::beg);
    ost.write((char *)& num_records, sizeof(num_records));
    ost.write((char *) data, len);
}

void save_bin_task(const user_permissions * data, size_t len, std::ostream & ost)
{
    size_t num_records = len / sizeof(user_permissions);
    
    ost.seekp(0, std::ios::beg);
    // save twice
    num_records *= 2;
    ost.write((char *)& num_records, sizeof(num_records));
    // save sorted data by surname
    ost.write((char *) data, len);
    // save sorted data by role
    // last item of enum is REGULAR_USER (number is 2)
    for (int i = 2; i >= 0; i--) {
        for (int j = 0; j < num_records; j++) {
            if (data[j].role == i) {
                ost.write((char *)& data[j], sizeof(user_permissions));
            }
        }
    }
}

user_permissions * load_txt(std::istream &ist, size_t & len)
{
    size_t num_records;
    int input_role;
    user_permissions * data;

    ist >> num_records;
    data = new user_permissions[num_records];

    for (int i = 0; i < num_records; i++) {
        ist >> data[i].surname;
        ist >> data[i].login;
        ist >> data[i].password;
        ist >> input_role;
        data[i].role = static_cast<roles>(input_role);
    }
    len = num_records * sizeof(user_permissions);

    return data;
}
user_permissions * load_bin(std::istream &ist, size_t & len)
{
    size_t num_records;
    user_permissions * data;

    ist.seekg(0, std::ios::beg);
    ist.read((char *)& num_records, sizeof(num_records));
    data = new user_permissions[num_records];
    len = num_records * sizeof(user_permissions);
    ist.read((char *) data, len);

    return data;
}
