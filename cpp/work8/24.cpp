#include <iostream>

int splice_array(const int * arr1, const int * arr2, int len, int * arr_out);
void input_array(int * a, int n);
void print_array(const int * a, int n);

int main(void)
{
    int * a = nullptr;
    int * b = nullptr;
    int * aout = nullptr;
    int n;
    
    std::cout << "Enter size of arrays:" << std::endl;
    std::cout << "=> ";
    std::cin >> n;
    
    a = (int *) malloc(sizeof(int) * n);
    b = (int *) malloc(sizeof(int) * n);
    aout = (int *) malloc(sizeof(int) * 2 * n);

    std::cout << "Enter first array (" << n << " elements)" << std::endl;
    std::cout << "=> ";
    input_array(a, n);
    std::cout << "Enter second array (" << n << " elements)" << std::endl;
    std::cout << "=> ";
    input_array(b, n);

    std::cout << splice_array(a, b, n, aout) << " is size aout" << std::endl;
    print_array(aout, 2*n);

    free(a);
    free(b);
    free(aout);

    return 0;
}

int splice_array(const int * arr1, const int * arr2, int len, int * arr_out)
{
    int len_out = 2 * len;
    int x = 0, y = 1;

    for (int i = 0; i < len; i++) {
        *(arr_out + x) = *(arr1 + i);
        *(arr_out + y) = *(arr2 + i);
        x+=2; y+=2;
    }
    return len_out;
}

void input_array(int * a, int n) 
{
    for (int i = 0; i < n; i++) {
        std::cin >> *(a + i);
    }
}

void print_array(const int * a, int n)
{
    for (int i = 0; i < n; i++) {
        std::cout << "[" << i << "] = " << *(a + i) << std::endl;
    }
}
