#include <iostream>

void print_array(const int * arr, int len);

int main(void)
{
    int * a = nullptr;
    int n;

    std::cout << "Enter size of array:" << std::endl;
    std::cout << "=> ";
    std::cin >> n;
    
    a = (int *) malloc(sizeof(int) * n);

    std::cout << "Enter " << n << " elements:" << std::endl;
    std::cout << "=> ";
    for (int i = 0; i < n; i++) {
        std::cin >> *(a + i);
        //std::cin >> a[i];
    }
    print_array(a, n);

    free(a);
    
    return 0;
}

void print_array(const int * arr, int len)
{
    std::cout << "Printing array..." << std::endl;
    for (int i = 0; i < len; i++) {
        std::cout << "[" << i << "] = " << *(arr + i) << std::endl;
    }
}
