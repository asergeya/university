#include <cstdio>
#include <cmath>

double dist(double x1, double y1, double x2, double y2);
double f(double x);

int main(void)
{
    double a, b, s;
    
    do {
        printf("Enter a and b:\n");
        printf("=> ");
        scanf("%lf %lf", &a, &b);
    } while (a > b);
    do {
        printf("Enter step:\n");
        printf("=> ");
        scanf("%lf", &s);
    } while (s <= 0);
   
    double x = a, y = f(x);
    double x0 = x, y0 = y;
    double segment_len, polyline_len = 0;
    do {
        segment_len = dist(x0, y0, x, y);
        polyline_len += segment_len;
        printf("x = %-10.2lf y = %-10.2lf d = %-10.2lf\n", x, y, segment_len);
        x0 = x; y0 = y;
        x += s; y = f(x);

    } while (x <= b);
    
    printf("Length is %.2lf\n", polyline_len);

    return 0;
}

double dist(double x1, double y1, double x2, double y2) 
{
    return sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1));
}

double f(double x)
{
    return x*sin(x) + 2*x - 1;
}

