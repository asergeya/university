#include <cstdio>

void print_diag(int s);

int main(void)
{
    int s;

    printf("Enter s:\n");
    printf("=> ");
    scanf("%d", &s);

    print_diag(s);

    return 0;
}

void print_diag(int s)
{
    for (int i = 0; i < s; i++) {
        for (int j = 0; j < i; j++) {
            putchar(' ');
        }
        puts("*");
    }
}
