#include <cstdio>

void print_rect(int x, int y, int sx, int sy);

int main(void)
{
    int x, y, sx, sy;

    printf("Enter x, y, sx, sy:\n");
    printf("=> ");
    scanf("%d %d %d %d", &x, &y, &sx, &sy);

    print_rect(x, y, sx, sy);

    return 0;
}

void print_rect(int x, int y, int sx, int sy) 
{
    for (int i = 0; i < y; i++) {
        putchar('\n');
    }
    for (int i = 0; i < sy; i++) {
        for (int k = 0; k < x; k++) {
            putchar(' ');
        }
        for (int j = 0; j < sx; j++) {
            putchar('@');
        }
        putchar('\n');
    }
}
