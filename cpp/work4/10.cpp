#include <cstdio>

int main(void)
{
    int x, prev;

    printf("=> ");
    scanf("%d", &x);
    do {
        printf("%d\n", x);
        prev = x;
        printf("=> ");
        scanf("%d", &x);
    } while (x >= prev);

    return 0;
}
