#include <cstdio>

int sum_dividers(int n);

int main(void)
{
    int a, b;
    int sum_div_a, sum_div_b;

    printf("Enter two numbers:\n");
    printf("=> ");
    scanf("%d %d", &a, &b);
    
    sum_div_a = sum_dividers(a);
    sum_div_b = sum_dividers(b);
    if (sum_div_a == b && sum_div_b == a) {
        printf("Yes\n");
    }
    else {
        printf("No\n");
    }

    return 0;
}

int sum_dividers(int n)
{
    int sum = 0;

    for (int i = 1; i < n; i++) {
        if (n % i == 0) {
            sum += i;
        }
    }
    return sum;
}
