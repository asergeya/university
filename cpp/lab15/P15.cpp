/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
 */
#include <iostream>
#include <fstream>

void text2bin(std::istream & ist, std::ostream & ost);
void my_task(std::istream & ist, int k);
int binarySearch(int * arr, int left_bound, int rigth_bound, int key);

int main()
{
    int k;
    std::ifstream fin;
    std::fstream bin_file;

    fin.open("fin.txt", std::ios::in);
    bin_file.open("bin_file.dat", std::ios::binary|std::ios::in|std::ios::out);
    text2bin(fin, bin_file);
    fin.close();
     
    std::cout << "Enter k (-1 for exit):" << '\n';
    std::cout << "=> ";
    std::cin >> k;
    while (k != -1) {
        my_task(bin_file, k);
        std::cout << "Enter k (-1 for exit):" << '\n';
        std::cout << "=> ";
        std::cin >> k;
    } 
    bin_file.close();

    return 0;
}

void text2bin(std::istream & ist, std::ostream & ost)
{
    int n, m; int ** table; 
    
    // read size of table of ints
    ist >> n >> m;
    // allocate memory for table
    table = new int * [n];
    for (int i = 0; i < n; i++) {
        table[i] = new int[m];
    }
    // read table 
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            ist >> table[i][j];
        }
    }
    // write to binary file
    ost.write((char *) &n, sizeof(int));
    ost.write((char *) &m, sizeof(int));
    for (int i = 0; i < n; i++) {
        ost.write((char *) table[i], sizeof(int)*m);
    }

    for (int i = 0; i < n; i++) {
        delete[] table[i];
    }
    delete[] table;
}

void my_task(std::istream & ist, int k)
{
    int * line;
    int len, index;
    bool isDescending = true;
    
    // skip number of string, read only number of element in line
    ist.seekg(sizeof(int), std::ios::beg);
    ist.read((char *) &len, sizeof(int));
    // allocate memory for line 
    line = new int[len];
    // read [k] line
    ist.seekg(sizeof(int)*(k-1)*len, std::ios::cur);
    ist.read((char *) line, sizeof(int)*len);
    
    // checking if the string is sorted in descending order
    for (int i = 0; i < len-1; i++) {
        if (line[i] < line[i+1]) {
            isDescending = false;
            break;
        }
    }
    // checking if [k] element is available in descending line 
    if (isDescending) {
        index = binarySearch(line, 0, len-1, k);
        std::cout << k << (index != -1 ? " is " : " is not ") 
                  << "in this line\n";
    }
    else {
        std::cout << "This line was not sorted in descending order!\n";
    }
    
    delete[] line;
}

int binarySearch(int * arr, int left_bound, int right_bound, int key)
{
    if (left_bound < right_bound) {
        int middle = left_bound + (right_bound - left_bound) / 2;
        
        if (arr[middle] == key) {
            return middle;
        }
        if (arr[middle] > key) {
            return binarySearch(arr, middle + 1, right_bound, key);
        }
        if (arr[middle] < key) {
            return binarySearch(arr, left_bound, middle - 1, key);
        }
    } 
    return -1;
}
