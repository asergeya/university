/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#include <iostream>
#include <algorithm>

#include "UserDatabase.h"

void load_txt(QVector<UserInfo> & data, QTextStream & ist)
{
    int num_of_records;

    ist >> num_of_records;

    data.resize(num_of_records);
    for (auto& i : data) {
        i.load_txt(ist);
    }
}

void load_bin(QVector<UserInfo> & data, QDataStream & ist)
{
    int num_of_records;

    ist >> num_of_records;
    
    data.resize(num_of_records);
    for (auto& i : data) {
        i.load_bin(ist);
    }
}

void save_bin(const QVector<UserInfo> & data, QDataStream & ost)
{
    ost << data.size();

    for (auto& i : data) {
        i.save_bin(ost);
    }
}

QString print_table(const QVector<UserInfo> & data)
{
    QString out;
    QTextStream ost(&out, QIODevice::WriteOnly);

    ost.setFieldAlignment(QTextStream::AlignLeft);
    UserInfo::print_table_head(ost);
    for (int i = 0; i < data.size(); i++) {
        ost << qSetFieldWidth(5) << i + 1;
        data[i].print_table_row(ost);
    }
    return out;
}

void sort(QVector<UserInfo> & data, bool (*f)(const UserInfo, const UserInfo))
{
    std::sort(data.begin(), data.end(), f);
}

bool compare_by_surname(const UserInfo first, const UserInfo second)
{
    if (first.get_surname() < second.get_surname()) {
        return true;
    }
    return false;
}

bool compare_by_role(const UserInfo first, const UserInfo second)
{
    if (first.get_role() < second.get_role()) {
        return true;
    }
    return false;
}
