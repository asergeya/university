#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include <QIODevice>
#include <QMessageBox>
#include <QFile>

#include "UserInfo.h"
#include "UserDatabase.h"

enum Modes {kMode1, kMode2};

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // Присваиваем номера радиокнопкам выбора режима, чтобы проще было определять
    // выбранный режим
    ui->modeButtonGroup->setId(ui->mode1RadioButton, kMode1);
    ui->modeButtonGroup->setId(ui->mode2RadioButton, kMode2);
}

MainWindow::~MainWindow()
{
    delete ui;
}

// Метод, который вызывается по сигналу нажатия кнопки выполнения
// Здесь программируется реакция на нажатие
void MainWindow::on_execPushButton_clicked()
{
    QVector<UserInfo> data;

    QFile txt_file(ui->textFileLineEdit->text());
    if (!txt_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        ui->outTextEdit->setPlainText("Can't open text file!\n");
        return;
    }

    QFile bin_file(ui->binFileLineEdit->text());
    if (!bin_file.open(QIODevice::ReadWrite)) {
        ui->outTextEdit->setPlainText("Can't open bin file!\n");
        return;
    }

    QTextStream in(&txt_file);
    QDataStream out(&bin_file);

    switch (ui->modeButtonGroup->checkedId()) {
        case kMode1:
            load_txt(data, in);
            sort(data, compare_by_surname);
            save_bin(data, out);
            ui->outTextEdit->setPlainText(print_table(data));
            break;
        case kMode2:
            load_bin(data, out);
            ui->outTextEdit->setPlainText(print_table(data));
            break;
    }
    txt_file.close();
    bin_file.close();
}
