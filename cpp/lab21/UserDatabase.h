/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#ifndef UserDatabase_H_INCLUDED
#define UserDatabase_H_INCLUDED

#include "UserInfo.h"
#include <QVector>

void load_txt(QVector<UserInfo> & data, QTextStream & ist);
void load_bin(QVector<UserInfo> & data, QDataStream & ist);
void save_bin(const QVector<UserInfo> & data, QDataStream & ost);
QString print_table(const QVector<UserInfo> & data);
void sort(QVector<UserInfo> & data, bool (*f)(const UserInfo, const UserInfo));
bool compare_by_surname(const UserInfo first, const UserInfo second);
bool compare_by_role(const UserInfo first, const UserInfo second);

#endif
