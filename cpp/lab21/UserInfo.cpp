/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#include <iostream>

#include "UserInfo.h"

QTextStream& operator<<(QTextStream & out, const UserRoles value) {
    switch(value) {
        case kAdmin:
            out << "Admin";
            break;
        case kModerator:
            out << "Moderator";
            break;
        case kRegularUser:
            out << "Regular user";
            break;
        default:
            std::cerr << "There are UserRoles values without overloading\n"; 
            break;
    }
    return out;
}
UserInfo::UserInfo()
{
    surname = "";
    login = "";
    password = "";
    role = kRegularUser;
}

QString UserInfo::get_surname(void) const
{
    return surname;
}

QString UserInfo::get_login(void) const
{
    return login;
}

QString UserInfo::get_password(void) const
{
    return password;
}

UserRoles UserInfo::get_role(void) const
{
    return role;
}

void UserInfo::load_txt(QTextStream & ist)
{
    int input_role;

    ist >> surname;
    ist >> login;
    ist >> password;
    ist >> input_role;
    role = static_cast<UserRoles>(input_role);
}

void UserInfo::load_bin(QDataStream & ist)
{
    ist >> surname;
    ist >> login;
    ist >> password;
    ist >> role;
}

void UserInfo::save_bin(QDataStream & ost) const
{
    ost << surname;
    ost << login;
    ost << password;
    ost << role;
}

void UserInfo::print_table_row(QTextStream & ost) const
{
    ost << qSetFieldWidth(15) << surname;
    ost << qSetFieldWidth(15) << login;
    ost << qSetFieldWidth(20) << password;
    ost << qSetFieldWidth(15) << role;
    ost << qSetFieldWidth(0)  << '\n';
}

void UserInfo::print_table_head(QTextStream & ost)
{

    ost << qSetFieldWidth(5)  << "#";
    ost << qSetFieldWidth(15) << "Surname";
    ost << qSetFieldWidth(15) << "Login";
    ost << qSetFieldWidth(20) << "Password";
    ost << qSetFieldWidth(15) << "User role";
    ost << qSetFieldWidth(0)  << '\n';
}
