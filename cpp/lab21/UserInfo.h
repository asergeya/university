/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#ifndef UserInfo_H_INCLUDED
#define UserInfo_H_INCLUDED

#define GREEN_BOLD "\033[1;32m"
#define WHITE_BOLD "\033[1;37m"
#define RESET "\033[0m"

#include <QString>
#include <QTextStream>
#include <QDataStream>

enum UserRoles {kAdmin, kModerator, kRegularUser};

class UserInfo {
    private:
        QString surname;
        QString login;
        QString password;
        UserRoles role;
    public:
        UserInfo();
        QString get_surname(void) const;
        QString get_login(void) const;
        QString get_password(void) const;
        UserRoles get_role(void) const;
        void load_txt(QTextStream & ist);
        void load_bin(QDataStream & ist);
        void save_bin(QDataStream & ost) const;
        void print_table_row(QTextStream & ost) const;
        static void print_table_head(QTextStream  & ost);
};

#endif
