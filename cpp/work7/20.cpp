#include <iostream>

int count_equal(const int arr[], int len, int x);

int main(void)
{
    int * a;
    int n;
    int key;

    std::cout << "Enter size of array:" << std::endl;
    std::cout << "=> ";
    std::cin >> n;
    std::cout << "Enter " << n << " elements:" << std::endl;
    std::cout << "=> ";
    a = new int[n];
    for (int i = 0; i < n; i++) {
        std::cin >> a[i];
    }
    std::cout << "Enter key:" << std::endl;
    std::cout << "=> ";
    std::cin >> key;
    std::cout << count_equal(a, n, key) << " is count equal elements" << std::endl;

    delete[] a;

    return 0;
}

int count_equal(const int arr[], int len, int x)
{
    int cnt = 0;

    for (int i = 0; i < len; i++) {
        if (x == arr[i]) {
            cnt++;
        }
    }
    return cnt;
}
