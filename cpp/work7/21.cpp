#include <iostream>

int splice_array(const int arr1[], const int arr2[], int len, int arr_out[]);
void input_array(int a[], int n);
void print_array(const int a[], int n);

int main(void)
{
    int * a1;
    int * a2;
    int * a_out;
    int n;
    
    std::cout << "Enter size of arrays:" << std::endl;
    std::cout << "=> ";
    std::cin >> n;
    std::cout << "Enter first array (" << n << " elements)" << std::endl;
    std::cout << "=> ";
    a1 = new int[n];
    input_array(a1, n);
    std::cout << "Enter second array (" << n << " elements)" << std::endl;
    std::cout << "=> ";
    a2 = new int[n];
    input_array(a2, n);

    a_out = new int[n*2];
    std::cout << splice_array(a1, a2, n, a_out) << " is size a_out" << std::endl;
    print_array(a_out, n*2);

    delete[] a1;
    delete[] a2;
    delete[] a_out;

    return 0;
}

int splice_array(const int arr1[], const int arr2[], int len, int arr_out[])
{
    int len_out = 2 * len;
    int x = 0, y = 1;

    for (int i = 0; i < len; i++) {
        arr_out[x] = arr1[i];
        arr_out[y] = arr2[i];
        x+=2; y+=2;
    }
    return len_out;
}

void input_array(int a[], int n) 
{
    for (int i = 0; i < n; i++) {
        std::cin >> a[i];
    }
}

void print_array(const int a[], int n)
{
    for (int i = 0; i < n; i++) {
        std::cout << "[" << i << "] = " << a[i] << std::endl;
    }
}
