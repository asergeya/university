/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
 */
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#define NUM_OF_INSERTIONS 3

struct Node {
    int data;
    Node * next = nullptr;
    Node * prev = nullptr;
};

struct List {
    Node * first = nullptr;
    Node * last = nullptr;
};

void input_list(List & list, std::istream & ist);
void remove_list(List & list);
void insert_node(List & list, Node * next, Node * new_node);
void insert_node_after(List & list, Node * prev, Node * new_node);
void remove_node(List & list, Node * n);
void remove_node_by_number(List & list, int n);
void push_node(List & list, int new_data);
void print_list(const List & list, std::ostream & ost);
void print_list_rev(const List & list, std::ostream & ost);
bool is_prime(int num);
Node * find_prime_num(Node * head);
int generate_num(int high_bound);
bool my_task(List & list, int nInsertions);

int main(void)
{
    std::srand(std::time(nullptr));
    List listOfNumbers;
    int n;
   
    input_list(listOfNumbers, std::cin);
    if (not my_task(listOfNumbers, NUM_OF_INSERTIONS)) {
        std::cout << "Not found prime number!\n";
    }
    std::cout << '\n';
    print_list(listOfNumbers, std::cout);
    std::cout << "Enter the number of the item that you want remove:\n";
    std::cout << "=> ";
    std::cin >> n;
    remove_node_by_number(listOfNumbers, n);
    print_list(listOfNumbers, std::cout);
    std::cout << '\n';
    print_list_rev(listOfNumbers, std::cout);
    remove_list(listOfNumbers);

    return 0;
}

void input_list(List & list, std::istream & ist)
{
    int n;

    ist >> n;
    for (int i = 0; i < n; i++) {
        int temp;
        
        ist >> temp;
        push_node(list, temp);
    }
}

void remove_list(List & list)
{
    Node * current = list.first;

    while (current != nullptr) {
        Node * temp = current;

        current = current->next;
        delete temp;
    }
    list.first = list.last = nullptr;
}

void insert_node(List & list, Node * next, Node * new_node)
{
    if (next != nullptr) {
        new_node->next = next;
        new_node->prev = next->prev;

        if (next->prev != nullptr) {
            next->prev->next = new_node;
        }
        else {
            list.first = new_node;
        }
        next->prev = new_node;
    }
    else {
        new_node->next = nullptr;
        new_node->prev = list.last;
        list.last->next = new_node;
        list.last = new_node;
    }
}

void insert_node_after(List & list, Node * prev, Node * new_node)
{
    new_node->next = prev->next;
    new_node->prev = prev;
    
    if (prev->next != nullptr) {
        prev->next->prev = new_node;
    }
    else {
        list.last = new_node;
    }
    prev->next = new_node;
}

void remove_node(List & list, Node * n) 
{
    // if first
    if (n->prev == nullptr) {
        if (n->next != nullptr) {
            list.first = n->next;
            n->next->prev = nullptr;
        }
        else {
            list.first = list.last = nullptr;
        }
    }
    // if last
    else if (n->next == nullptr) {
        list.last = n->prev; 
        n->prev->next = nullptr;
    }
    // if middle
    else {
        n->next->prev = n->prev;
        n->prev->next = n->next;
    }
}

void remove_node_by_number(List & list, int n)
{
    Node * current = list.first;
    int i = 1;

    if (n < 1) {
        std::cerr << "The node number must be natural!\n";
        return;
    }
    
    while (i < n and current != nullptr) {
        current = current->next;
        i++;
    }
    if (i == n) {
        remove_node(list, current);
        delete current;
    }
    else {
        std::cerr << "The node number is greater than the items in the list!\n";
    }
}

void push_node(List & list, int new_data)
{
    Node * new_node = new Node;

    new_node->data = new_data;
    new_node->next = nullptr;
    
    if (list.last == nullptr) {
        list.first = list.last = new_node;
        new_node->prev = nullptr;
    } 
    else {
        list.last->next = new_node;
        new_node->prev = list.last;
        list.last = new_node;
    }
}

void print_list(const List & list, std::ostream & ost)
{
    const Node * current = list.first;

    while (current != nullptr) {
        ost << current->data << ' ';
        current = current->next;
    }
    ost << '\n';
}

void print_list_rev(const List & list, std::ostream & ost)
{
    const Node * current = list.last;

    while (current != nullptr) {
        ost << current->data << ' ';
        current = current->prev;
    }
    ost << '\n';
}

bool is_prime(int num)
{
    if (num <= 1) {
        return false;
    }
    for (int i = 2; i * i <= num; i++) {
        if (num % i == 0) {
            return false;
        }
    }
    return true;
}

Node * find_prime_num(Node * head)
{
    Node * current = head;

    while (current != nullptr and not is_prime(current->data)) {
        current = current->next;
    }
    return current;
}

int generate_num(int high_bound)
{
    return std::rand() % high_bound + 1;
}

bool my_task(List & list, int nInsertions)
{
    Node * prime = find_prime_num(list.first);
    int num_of_prime_nums = 0;
    
    while (prime != nullptr) {
        Node * next = prime->next;

        for (int i = 0; i < nInsertions; i++) {
            Node * new_node = new Node;
            
            new_node->data = generate_num(prime->data);
            insert_node_after(list, prime, new_node);
        }
        prime = find_prime_num(next);
        num_of_prime_nums++;
    }
    if (num_of_prime_nums != 0) {
        return true;
    }
    else {
        return false;
    }
}
