#include <cstdio>
#include <cmath>

int main(void)
{
    int months;
    int nYears, nMonths;
    double sum, total;
    double rate;

    printf("Enter amount of money:\n");
    printf("=> ");
    scanf("%lf", &sum);
    printf("Enter numbers of months:\n");
    printf("=> ");
    scanf("%d", &months);
    printf("Enter rate:\n");
    printf("=> ");
    scanf("%lf", &rate);

    rate /= 100;
    nYears = months / 12;
    nMonths = months % 12;
    total = sum*pow(1+rate, nYears);
    total += total*(rate/12*nMonths);
    printf("Sum of money will be %.2lf after %d months\n", total, months);

    return 0;
}


