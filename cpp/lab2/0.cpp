#include <cstdio>

int main(void)
{
    long a, b;

    printf("Enter first number:\n");
    printf("=> ");
    scanf("%ld", &a);
    printf("Enter second number:\n");
    printf("=> ");
    scanf("%ld", &b);

    putchar('\n');
    printf("Sum: %ld\n", a + b);
    printf("Product: %ld\n", a * b);
    printf("Difference: %ld\n", a - b);
    printf("Average: %f\n", double(a + b) / 2);

    return 0;
}




