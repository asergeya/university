#include <iostream>
#include <algorithm>
#include <cstdlib>

void sort(float arr[], int len, bool asc);
void input_array(float arr[], int len);
void print_array(const float arr[], int len);

int main(void)
{
    float * a;
    int n;
    bool isAscending = true;
    
    std::cout << "Enter size of array:\n";
    std::cout << "=> ";
    std::cin >> n;
    std::cout << "How to sort (1 - for ascending, else - 0):\n";
    std::cout << "=> ";
    std::cin >> isAscending;

    a = (float *) malloc(sizeof(float) * n);
    input_array(a, n);
    sort(a, n, isAscending);
    print_array(a, n);

    free(a);
    
    return 0;
}

void sort(float arr[], int len, bool asc)
{
    for (int i = 0; i < len-1; i++) {
        for (int j = i + 1; j < len; j++) {
            if (asc) {
                if (arr[j] < arr[i]) {
                    std::swap(arr[j], arr[i]);
                }
            }
            else {
                if (arr[j] > arr[i]) {
                    std::swap(arr[j], arr[i]);
                }
            }
        }
    }
}

void input_array(float arr[], int len)
{
    std::cout << "Enter " << len << " elements:\n";
    std::cout << "=> ";
    for (int i = 0; i < len; i++) {
        std::cin >> arr[i];
    }
}

void print_array(const float arr[], int len) 
{
    std::cout << "\nPrinting array...\n";
    for (int i = 0; i < len; i++) {
        std::cout << "[" << i << "]" << " = " << arr[i] << std::endl;
    }
}
