#include <iostream>
#include <algorithm>
#include <cstdlib>

void sort(int arr[], int len, bool even_greater);
bool compare(int a, int b, bool even);
void input_array(int arr[], int len);
void print_array(const int arr[], int len);

int main(void)
{
    int * a;
    int n;
    bool even_rules = true;
    
    std::cout << "Enter size of array:\n";
    std::cout << "=> ";
    std::cin >> n;
    std::cout << "How to sort (1 - even_greater_rules, else - 0):\n";
    std::cout << "=> ";
    std::cin >> even_rules;

    a = (int *) malloc(sizeof(int) * n);
    input_array(a, n);
    sort(a, n, even_rules);
    print_array(a, n);

    free(a);
    
    return 0;
}

void sort(int arr[], int len, bool even_greater)
{
    for (int i = 0; i < len-1; i++) {
        for (int j = i + 1; j < len; j++) {
            if (compare(arr[j], arr[i], even_greater)) {
                std::swap(arr[j], arr[i]);
            }
        }
    }
}

bool compare(int a, int b, bool even)
{
    if (a % 2 == 0 && b % 2 == 1) {
        return !even;
    }
    else if (a % 2 == 1 && b % 2 == 0) {
        return even;
    }
    else {
        return a < b;
    }
}

void input_array(int arr[], int len)
{
    std::cout << "Enter " << len << " elements:\n";
    std::cout << "=> ";
    for (int i = 0; i < len; i++) {
        std::cin >> arr[i];
    }
}

void print_array(const int arr[], int len) 
{
    std::cout << "\nPrinting array...\n";
    for (int i = 0; i < len; i++) {
        std::cout << "[" << i << "]" << " = " << arr[i] << std::endl;
    }
}
