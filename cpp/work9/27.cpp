#include <iostream>
#include <algorithm>
#include <cmath>
#include <cstdlib>

void sort(float arr[], int len, float x);
bool compare(float a, float b, float x);
void input_array(float arr[], int len);
void print_array(const float arr[], int len);
float * create_dist_array(const float arr[], float x, int size);

int main(void)
{
    float * a;
    float * diff;
    float x;
    int n;

    std::cout << "Enter size of array:\n";
    std::cout << "=> ";
    std::cin >> n;
    std::cout << "Enter diff for sort:\n";
    std::cout << "=> ";
    std::cin >> x;

    a = (float *) malloc(sizeof(float) * n);
    input_array(a, n);
    sort(a, n, x);
    print_array(a, n);
    diff = create_dist_array(a, x, n);
    print_array(diff, n);

    free(a);
    free(diff);
    
    return 0;
}

void sort(float arr[], int len, float x)
{
    for (int i = 0; i < len-1; i++) {
        for (int j = i + 1; j < len; j++) {
            if (compare(arr[j], arr[i], x)) {
                std::swap(arr[j], arr[i]);
            }
        }
    }
}

bool compare(float a, float b, float x)
{
   if (fabs(a-x) <= fabs(b-x)) {
       return true;
   }
   else {
       return false;
   }
}
void input_array(float arr[], int len)
{
    std::cout << "Enter " << len << " elements:\n";
    std::cout << "=> ";
    for (int i = 0; i < len; i++) {
        std::cin >> arr[i];
    }
}

void print_array(const float arr[], int len) 
{
    std::cout << "\nPrinting array...\n";
    for (int i = 0; i < len; i++) {
        std::cout << "[" << i << "]" << " = " << arr[i] << std::endl;
    }
}

float * create_dist_array(const float arr[], float x, int size)
{
    float * ptr = (float *) malloc(sizeof(float) * size);

    for (int i = 0; i < size; i++) {
        ptr[i] = fabs(arr[i] - x);
    }
    return ptr;
}
