/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#include <iostream>

#include "UserInfo.h"

std::ostream& operator<<(std::ostream& out, const UserRoles value) {
    switch(value) {
        case kAdmin:
            out << "Admin";
            break;
        case kModerator:
            out << "Moderator";
            break;
        case kRegularUser:
            out << "Regular user";
            break;
        default:
            std::cerr << "There are UserRoles values without overloading\n"; 
            break;
    }
    return out;
}

UserInfo::UserInfo()
{
    surname = "";
    login = "";
    password = "";
    role = kRegularUser;
}

std::string UserInfo::get_surname(void) const
{
    return surname;
}

std::string UserInfo::get_login(void) const
{
    return login;
}

std::string UserInfo::get_password(void) const
{
    return password;
}

UserRoles UserInfo::get_role(void) const
{
    return role;
}

void UserInfo::load_txt(std::istream & ist) 
{
    int input_role;

    getline(ist, surname);
    getline(ist, login);
    getline(ist, password);
    ist >> input_role;
    ist.ignore();
    role = static_cast<UserRoles>(input_role);
}

void UserInfo::load_bin(std::istream & ist)
{
    size_t size;

    ist.read((char *) &size, sizeof(size));
    surname.resize(size);
    ist.read((char *) &surname[0], size);

    ist.read((char *) &size, sizeof(size));
    login.resize(size);
    ist.read((char *) &login[0], size);

    ist.read((char *) &size, sizeof(size));
    password.resize(size);
    ist.read((char *) &password[0], size);

    ist.read((char *) &role, sizeof(role));

}

void UserInfo::save_bin(std::ostream & ost) const
{
    size_t size;

    size = surname.size();
    ost.write((char *) &size, sizeof(size));
    ost.write((char *) surname.c_str(), size);

    size = login.size();
    ost.write((char *) &size, sizeof(size));
    ost.write((char *) login.c_str(), size);

    size = password.size();
    ost.write((char *) &size, sizeof(size));
    ost.write((char *) password.c_str(), size);

    ost.write((char *) &role, sizeof(role));
}

// TODO: overload output operator
void UserInfo::print_table_row(std::ostream & ost) const
{
    ost.width(15); ost << std::left << surname;
    ost.width(15); ost << std::left << login;
    ost.width(20); ost << std::left << password;
    ost.width(15); ost << std::left << role;
    ost << '\n';
}

void UserInfo::print_table_head(std::ostream & ost)
{
    ost << WHITE_BOLD;
    ost.width(5);  ost << std::left << "#";
    ost.width(15); ost << std::left << "Surname";
    ost.width(15); ost << std::left << "Name";
    ost.width(20); ost << std::left << "Password";
    ost.width(15); ost << std::left << "User role";
    ost << RESET << '\n';
}
