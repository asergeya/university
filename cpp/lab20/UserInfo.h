/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#ifndef UserInfo_H_INCLUDED
#define UserInfo_H_INCLUDED

#define GREEN_BOLD "\033[1;32m"
#define WHITE_BOLD "\033[1;37m"
#define RESET "\033[0m"

#include <string>

enum UserRoles {kAdmin, kModerator, kRegularUser};

class UserInfo {
    private:
        std::string surname;
        std::string login;
        std::string password;
        UserRoles role;
    public:
        UserInfo();
        std::string get_surname(void) const;
        std::string get_login(void) const;
        std::string get_password(void) const;
        UserRoles get_role(void) const;
        void load_txt(std::istream & ist);
        void load_bin(std::istream & ist);
        void save_bin(std::ostream & ost) const;
        void print_table_row(std::ostream & ost) const;
        static void print_table_head(std::ostream & ost);
};

#endif
