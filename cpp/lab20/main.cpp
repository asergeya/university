/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

// it is include <vector> and <string>
#include "UserDatabase.h" 
#include <iostream>
#include <fstream>

// TODO: pass file name as command line argument
#define BIN_FILE "userdb.dat"
#define TXT_FILE "users.txt"

#define CMD_MODE1 "mode1"
#define CMD_PRINT "print"

enum Modes {kUnknowMode, kMode1, kPrint};

void show_help(char * app_name);
void mode1(std::string input_file_name);
void print(void);
Modes detect_mode(std::string command); 

int main(int argc, char * argv[])
{
    std::vector<UserInfo> data;
    std::string input_file_name = TXT_FILE;
    std::string command;
    Modes current_mode;

    if (argc < 2 or argc > 4) {
        show_help(argv[0]);
        return 1;
    }
    if (argc == 3) {
        input_file_name = argv[2];
    }

    command = argv[1];
    current_mode = detect_mode(command);
    switch (current_mode) {
        case kUnknowMode:
            show_help(argv[0]);
            break;
        case kMode1:
            mode1(input_file_name);
            break;
        case kPrint:
            print();
            break;
    } 
    return 0;
}

void show_help(char * app_name) 
{
    std::cerr << "Usage: " << app_name << "[option]\n"
              << "Options:\n"
              << "\t-h, --help          Print out this help\n"
              << "\tmode1 [input_file]  Load from txt file, sort, save in binary file and print it\n"
              << "\tprint               Load from binary file and print it\n"
              << "Examples:\n"
              << "\t" << app_name << " -h\n"
              << "\t" << app_name << " --help\n"
              << "\t" << app_name << " mode1\n"
              << "\t" << app_name << " mode1 users.txt\n"
              << "\t" << app_name << " print\n";

}

Modes detect_mode(std::string command) 
{
    if (command == CMD_MODE1) {
        return kMode1;
    }
    if (command == CMD_PRINT) {
        return kPrint;
    }
    return kUnknowMode;
}

void mode1(std::string input_file_name) 
{
    std::vector<UserInfo> data;

    std::ifstream fin(input_file_name);
    std::fstream bin(BIN_FILE, std::ios::binary|std::ios::out);

    if (not fin.is_open() or not bin.is_open()) {
        std::cerr << "Error opening file!\n";

        exit(1);
    }
    load_txt(data, fin);
    sort(data, compare_by_surname);
    print_table(data, std::cout);
    save_bin(data, bin);

    fin.close();
    bin.close();
}

void print(void)
{
    std::vector<UserInfo> data;

    std::fstream bin(BIN_FILE, std::ios::binary|std::ios::in);

    if (not bin.is_open()) {
        std::cerr << "Error opening file!\n";

        exit(1);
    }
    load_bin(data, bin);
    print_table(data, std::cout);

    bin.close();
}
