/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
 */
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

#define GREEN_BOLD "\033[1;32m"
#define WHITE_BOLD "\033[1;37m"
#define RESET "\033[0m"

enum UserRoles {kAdmin, kModerator, kRegularUser};

std::ostream& operator<<(std::ostream& out, const UserRoles value) {
    switch(value) {
        case kAdmin:
            out << "Admin";
            break;
        case kModerator:
            out << "Moderator";
            break;
        case kRegularUser:
            out << "Regular user";
            break;
        default:
            std::cerr << "There are UserRoles values without overloading\n"; 
            break;
    }
    return out;
}

class UserInfo {
    private:
        std::string surname;
        std::string login;
        std::string password;
        UserRoles role;
    public:
        std::string get_surname(void) const;
        std::string get_login(void) const;
        std::string get_password(void) const;
        UserRoles get_role(void) const;
        void input(std::istream & ist);
        void print(std::ostream & ost) const;
        void load_bin(std::istream & ist);
        void save_bin(std::ostream & ost) const;
};

std::string UserInfo::get_surname(void) const
{
    return surname;
}

std::string UserInfo::get_login(void) const
{
    return login;
}

std::string UserInfo::get_password(void) const
{
    return password;
}

UserRoles UserInfo::get_role(void) const
{
    return role;
}

void UserInfo::input(std::istream & ist) 
{
    int input_role;

    getline(ist, surname);
    getline(ist, login);
    getline(ist, password);
    ist >> input_role;
    ist.ignore();
    role = static_cast<UserRoles>(input_role);
}

// TODO: overload output operator
void UserInfo::print(std::ostream & ost) const
{
    ost.width(10); ost << std::left << surname;
    ost.width(10); ost << std::left << login;
    ost.width(15); ost << std::left << password;
    ost.width(15); ost << std::left << role;
    ost << '\n';
}

void UserInfo::load_bin(std::istream & ist)
{
    size_t size;

    ist.read((char *) &size, sizeof(size));
    surname.resize(size);
    ist.read((char *) &surname[0], size);

    ist.read((char *) &size, sizeof(size));
    login.resize(size);
    ist.read((char *) &login[0], size);

    ist.read((char *) &size, sizeof(size));
    password.resize(size);
    ist.read((char *) &password[0], size);

    ist.read((char *) &role, sizeof(role));

}

void UserInfo::save_bin(std::ostream & ost) const
{
    size_t size;

    size = surname.size();
    ost.write((char *) &size, sizeof(size));
    ost.write((char *) surname.c_str(), size);

    size = login.size();
    ost.write((char *) &size, sizeof(size));
    ost.write((char *) login.c_str(), size);

    size = password.size();
    ost.write((char *) &size, sizeof(size));
    ost.write((char *) password.c_str(), size);

    ost.write((char *) &role, sizeof(role));
}

class UserDatabase {
    private:
        std::vector<UserInfo> data;
    public:
       void load_txt(std::istream & ist); 
       void load_bin(std::istream & ist);
       void save_bin(std::ostream & ost) const;
       void print(std::ostream & ost) const;
       void sort(bool (*f)(const UserInfo, const UserInfo));
       static bool compare_by_surname(const UserInfo first, const UserInfo second);
       static bool compare_by_role(const UserInfo first, const UserInfo second);
};

void UserDatabase::load_txt(std::istream & ist)
{
    size_t num_of_records;

    ist >> num_of_records;
    ist.ignore();
    data.resize(num_of_records);

    for (int i = 0; i < num_of_records; i++) {
        data[i].input(ist);
    }
}

void UserDatabase::load_bin(std::istream & ist) 
{
    size_t num_of_records;

    ist.seekg(0);
    ist.read((char *) &num_of_records, sizeof(num_of_records));
    
    data.resize(num_of_records);
    for (size_t i = 0; i < num_of_records; i++) {
        data[i].load_bin(ist);
    }
}

void UserDatabase::save_bin(std::ostream & ost) const
{
    size_t num_of_records = data.size();

    ost.seekp(0);
    ost.write((char *) &num_of_records, sizeof(num_of_records));

    for (size_t i = 0; i < num_of_records; i++) {
        data[i].save_bin(ost);
    }
}

void UserDatabase::print(std::ostream & ost) const
{
    ost << WHITE_BOLD;
    ost.width(5);  ost << std::left << "#";
    ost.width(10); ost << std::left << "Surname";
    ost.width(10); ost << std::left << "Name";
    ost.width(15); ost << std::left << "Password";
    ost.width(15); ost << std::left << "User role";
    ost << RESET << '\n';
    for (size_t i = 0; i < data.size(); i++) {
        ost << GREEN_BOLD;
        ost.width(5); ost << std::left << i + 1;
        ost << RESET;
        data[i].print(ost);
    }
}

void UserDatabase::sort(bool (*f)(const UserInfo, const UserInfo))
{
    std::sort(data.begin(), data.end(), f);
}

bool compare_by_surname(const UserInfo first, const UserInfo second)
{
    if (first.get_surname().compare(second.get_surname()) <= 0) {
        return true;
    }
    return false;
}

bool compare_by_role(const UserInfo first, const UserInfo second)
{
    if (first.get_role() < second.get_role()) {
        return true;
    }
    return false;
}

int main(void)
{
    UserDatabase data;

    std::ifstream fin("users.txt");
    std::fstream bin("userdb.dat", std::ios::binary|std::ios::in|std::ios::out);

    //data.load_txt(fin);
    //data.save_bin(bin);
    data.load_bin(bin);
    data.sort(compare_by_role);
    data.print(std::cout);

    return 0;
}
