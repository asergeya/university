/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#ifndef UserDatabase_H_INCLUDED
#define UserDatabase_H_INCLUDED

#include "UserInfo.h"
#include <vector>

void load_txt(std::vector<UserInfo> & data, std::istream & ist); 
void load_bin(std::vector<UserInfo> & data, std::istream & ist);
void save_bin(const std::vector<UserInfo> & data, std::ostream & ost);
void print_table(const std::vector<UserInfo> & data, std::ostream & ost);
void sort(std::vector<UserInfo> & data, bool (*f)(const UserInfo, const UserInfo));
bool compare_by_surname(const UserInfo first, const UserInfo second);
bool compare_by_role(const UserInfo first, const UserInfo second);

#endif
