/* Author: Abramov Sergey
 * Group: KI20-08B
 * University: SibFU IKIT
 * Task at 100%, variant 1
 * WARNING, there are no checks for correct I/O, memory allocation!
*/

#include <iostream>
#include <algorithm>

#include "UserDatabase.h"

void load_txt(std::vector<UserInfo> & data, std::istream & ist)
{
    size_t num_of_records;

    ist >> num_of_records;
    ist.ignore();
    data.resize(num_of_records);

    for (size_t i = 0; i < num_of_records; i++) {
        data[i].load_txt(ist);
    }
}

void load_bin(std::vector<UserInfo> & data, std::istream & ist) 
{
    size_t num_of_records;

    ist.seekg(0);
    ist.read((char *) &num_of_records, sizeof(num_of_records));
    
    data.resize(num_of_records);
    for (size_t i = 0; i < num_of_records; i++) {
        data[i].load_bin(ist);
    }
}

void save_bin(const std::vector<UserInfo> & data, std::ostream & ost) 
{
    size_t num_of_records = data.size();

    ost.seekp(0);
    ost.write((char *) &num_of_records, sizeof(num_of_records));

    for (size_t i = 0; i < num_of_records; i++) {
        data[i].save_bin(ost);
    }
}

void print_table(const std::vector<UserInfo> & data, std::ostream & ost)
{
    UserInfo::print_table_head(ost);
    for (size_t i = 0; i < data.size(); i++) {
        ost << GREEN_BOLD;
        ost.width(5); ost << std::left << i + 1;
        ost << RESET;
        data[i].print_table_row(ost);
    }
}

void sort(std::vector<UserInfo> & data, bool (*f)(const UserInfo, const UserInfo))
{
    std::sort(data.begin(), data.end(), f);
}

bool compare_by_surname(const UserInfo first, const UserInfo second)
{
    if (first.get_surname() < second.get_surname()) {
        return true;
    }
    return false;
}

bool compare_by_role(const UserInfo first, const UserInfo second)
{
    if (first.get_role() < second.get_role()) {
        return true;
    }
    return false;
}
