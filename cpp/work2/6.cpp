#include <cstdio>
#include <cmath>

int main(void)
{
    double a, b, c;
    double d;

    printf("Enter a, b, c (ax^2+bx+c):\n");
    printf("=> ");
    scanf("%lf %lf %lf", &a, &b, &c);

    if (a == 0) {
        if (b == 0) {
            if (c == 0) {
                printf("x belongs to R\n");
            }
            else {
                printf("no solutions\n");
            }
        }
        else {
            printf("x = %lf\n", -c / b);
        }
    }
    else {
        d = b*b - 4*a*c;
        if (d > 0) {
            printf("x1 = %lf\n", (-b + sqrt(d)) / (2*a));
            printf("x2 = %lf\n", (-b - sqrt(d)) / (2*a));
        }
        else if (d == 0) {
            printf("x = %lf\n", -b / (2*a));
        }
        else {
            printf("no solutions\n");
        }
    }
    
    return 0;
}
