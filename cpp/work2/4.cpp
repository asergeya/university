#include <cstdio>

int main(void)
{
    double x, a, b, c, d;

    printf("Enter x:\n");
    printf("=> ");
    scanf("%lf", &x);
    printf("Enter start of first segment and the end:\n");
    printf("=> ");
    scanf("%lf %lf", &a, &b);
    printf("Enter start of second segment and the end:\n");
    printf("=> ");
    scanf("%lf %lf", &c, &d);

    if (a <= x && x <= b || c <= x && x <= d) {
        printf("x belongs\n");
    }
    else {
        printf("x doesnt belong\n");
    }

    return 0;
}
