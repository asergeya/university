#include <cstdio>

int main(void)
{
    double a1, a2, b1, b2;

    printf("Enter start of fisrt segment and the end:\n");
    printf("=> ");
    scanf("%lf %lf", &a1, &a2);
    printf("Enter start of second segment and the end:\n");
    printf("=> ");
    scanf("%lf %lf", &b1, &b2);

    if (a1 == b1 && a2 == b2) {
        printf("A equal B\n");
    }
    else if (a1 < b1 && b2 < a2) {
        printf("B is inside A\n");
    }
    else if (b1 < a1 && a2 < b2) {
        printf("A is inside B\n");
    }
    else if (a2 < b1 || b2 < a1) {
        printf("Dont intersect\n");
    }
    else {
        printf("Intersect in another way\n");
    }

    return 0;
}
