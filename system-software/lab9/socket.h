#ifndef SOCKET_H
#define SOCKET_H

#include <netinet/ip.h> 
#include <cstdint>
#include <string>

class Socket {
public:
    Socket(int domain, int type, int protocol=0);
    ~Socket();

    int bind(const std::string &address, uint16_t port);
    int connect(const sockaddr *addr, socklen_t addrlen);
    int accept(sockaddr *__restrict addr, socklen_t *__restrict addrlen);
    int listen(int backlog);
    int close();

    int getsockname(sockaddr *__restrict addr, socklen_t *__restrict addrlen);
    int getsockopt(int level, int option_name, void *__restrict option_value, socklen_t *__restrict option_len);
    int setsockopt(int level, int option_name, const void *option_value, socklen_t option_len);

    ssize_t recv(uint8_t *buf, size_t len, int flags);
    ssize_t recvfrom(uint8_t *buf, size_t len, int flags, sockaddr *from_addr, socklen_t *addrlen);
    ssize_t send(uint8_t const *buf, size_t len, int flags);
    ssize_t sendto(uint8_t const *buf, size_t len, int flags, const sockaddr *to_addr, socklen_t addrlen);

private:
    int m_socket_fd;

    bool m_closed;
};

#endif // SOCKET_H