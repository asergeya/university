#include <sys/wait.h>
#include <sched.h>

#include <algorithm>
#include <iostream>
#include <numeric>
#include <cstring>
#include <vector>


const auto kStackSize = 1024 * 1024;

std::vector<std::vector<int>> g_matrix;
std::vector<int> g_min_elements;

int minElement(void *row_index);

int main()
{
    size_t n, k;

    // Input
    std::cout << "Enter the matrix size:\n";
    std::cin >> n >> k;

    g_matrix.resize(n);
    g_min_elements.resize(n);

    std::cout << "Enter the matrix elements:\n";
    int temp;
    for (auto i = 0; i < n; ++i) {
        for (auto j = 0; j < k; ++j) {
            std::cin >> temp;
            g_matrix[i].push_back(temp);
        }
    }

    // Stack allocation
    char **stack = new char *[n];
    for (auto i = 0; i < n; ++i) {
        stack[i] = new char[kStackSize]; 
    }
    
    // Args
    std::vector<int> args(n);
    std::iota(std::begin(args), std::end(args), 0);

    // Cloning
    std::vector<int> pid(n);
    for (auto i = 0; i < n; ++i) {
        pid[i] = clone(minElement, (void *) (stack[i]+kStackSize), CLONE_VM | SIGCHLD, &(args[i]));
        if (pid[i] < 0) {
            std::cerr << "Error: " << strerror(errno) << '\n';
            exit(1);
        }
    }
    for (auto i = 0; i < n; ++i) {
        waitpid(pid[i], nullptr, 0); 
    }
    
    // Stack deallocation
    for (auto i = 0; i < n; ++i) {
        delete stack[i];
    }
    delete[] stack;

    std::cout << *std::max_element(std::cbegin(g_min_elements), std::cend(g_min_elements)) << '\n';
}

int minElement(void *row_index)
{
    auto index = *(static_cast<int *>(row_index));

    g_min_elements[index] = *std::min_element(std::cbegin(g_matrix[index]), std::cend(g_matrix[index]));

    return 0;
}

