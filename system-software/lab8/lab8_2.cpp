#include <sys/wait.h>
#include <sched.h>

#include <forward_list>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <ctime>


const int    kMaxNum           = 10;
const size_t kNumbersMaxLength = 10;
const size_t kListsMaxNum      = 20;
const size_t kStackSize        = 1024 * 1024;

using Numbers = std::forward_list<int>;
using NumberLists = std::vector<Numbers>;

NumberLists g_lists;
std::vector<int> g_positive_elements_num;

void generateNumberSequence(Numbers &nums);
void generateNumberLists(NumberLists &lists);
void printNumberLists(const NumberLists &lists);
int positiveElementsCount(void *row_index);

int main()
{
    std::srand(std::time(nullptr));

    generateNumberLists(g_lists);
    printNumberLists(g_lists);

    auto lists_num = g_lists.size(); 
    g_positive_elements_num.resize(lists_num);

    // Stack allocation
    char **stack = new char *[lists_num];
    for (auto i = 0; i < lists_num; ++i) {
        stack[i] = new char[kStackSize]; 
    }
    
    // Args
    std::vector<int> args(lists_num);
    std::iota(std::begin(args), std::end(args), 0);

    // Cloning
    std::vector<int> pid(lists_num);
    for (auto i = 0; i < lists_num; ++i) {
        pid[i] = clone(positiveElementsCount, (void *) (stack[i]+kStackSize), CLONE_VM | SIGCHLD, &(args[i]));
        if (pid[i] < 0) {
            std::cerr << "Error: " << strerror(errno) << '\n';
            exit(1);
        }
    }
    for (auto i = 0; i < lists_num; ++i) {
        waitpid(pid[i], nullptr, 0); 
    }
    
    // Stack deallocation
    for (auto i = 0; i < lists_num; ++i) {
        delete stack[i];
    }
    delete[] stack;

    auto result = std::max_element(std::cbegin(g_positive_elements_num), std::cend(g_positive_elements_num));
    auto index = std::distance(std::cbegin(g_positive_elements_num), result);

    std::cout << "Print list with a lot of positive nums.\n";
    for (const auto &num : g_lists[index]) {
        std::cout << num << " ";
    }
    std::cout << '\n';
}

void generateNumberSequence(Numbers &nums)
{
    size_t size = (std::rand() % kNumbersMaxLength) + 1;

    for (auto i = 0; i < size; ++i) {
        nums.push_front((std::rand() % kMaxNum) - kMaxNum/2);
    }
}

void generateNumberLists(NumberLists &lists)
{
    size_t size = (std::rand() % kListsMaxNum) + 1;

    lists.resize(size);

    for (auto i = 0; i < size; ++i) {
        generateNumberSequence(lists[i]);
    }
}

void printNumberLists(const NumberLists &lists)
{
    for (const auto &list : lists) {
        for (const auto &num : list) {
            std::cout << num << " ";
        }
        std::cout << '\n';
    }
}

int positiveElementsCount(void *row_index)
{
    auto index = *(static_cast<int *>(row_index));

    g_positive_elements_num[index] = std::count_if(
        std::cbegin(g_lists[index]),
        std::cend(g_lists[index]), [](int x) {
        return x > 0; 
    }); 

    return 0;
}

