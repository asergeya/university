#!/bin/bash

function show_help() {
    echo "Description:"
    echo "  Kill processes (except session leaders), which pid is above first argument"
    echo
    echo "Usage:"
    echo "  ./$(basename $0) pid"
    echo
    echo "Options"
    echo "  -h, --help    show this help message and exit"
}

function kill_processes_above() {
    local pids=$(ps -d -o pid)
    local start_pid=$1

    for pid in ${pids}; do
        if (( ${pid} >= ${start_pid} )); then
            echo "kill -9 ${pid}"
            #kill -9 ${pid}
        fi
    done
}

if (( $# == 1 )); then
    kill_processes_above $1
else
    show_help
    exit 1
fi
