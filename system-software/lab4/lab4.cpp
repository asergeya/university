#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>
#include <cstring>
#include <string>

enum class MenuAction { doCmd, runScript, doTask, exit };

void displayMenu();
void doCmd();
void runScript();
void doTask();

int main()
{
    auto choice = 0;

    do {
        displayMenu();
        std::cout << "Enter your choice:\n";
        std::cin >> choice;

        switch (static_cast<MenuAction>(choice)) {
        case MenuAction::doCmd:
            doCmd();
            break;
        case MenuAction::runScript:
            runScript();
            break;
        case MenuAction::doTask:
            doTask();
            break;
        case MenuAction::exit:
            exit(0);
            break;
        default:
            std::cout << "Invalid input\n";
            break;
        }
    } while (true);
}

void displayMenu()
{
    std::cout << "Menu\n"
              << "0 - doCmd\n"
              << "1 - runScript\n"
              << "2 - doTask\n"
              << "3 - exit\n";
}

void doCmd()
{
    auto status = 0;

    if (fork() == 0) {
        std::string cmd;

        std::cout << "Enter shell cmd:\n";
        std::cin >> cmd;

        system(cmd.c_str());
        exit(0);
    }
    wait(&status);
}

void runScript()
{
    auto status = 0;

    if (fork() == 0) {
        system("./lab1.sh");
        exit(0);
    }
    wait(&status);
}

void doTask()
{
    auto status = 0;

    if (fork() == 0) {
        std::string filename("test.txt");
        /*
        if (chmod(filename.c_str(), 0777) < 0) {
            std::cerr << "Error: " << strerror(errno) << '\n';
        }
        exit(0);
        */
        execlp("chmod" , "chmod", "777", filename.c_str(), nullptr);
        exit(0);
    }
    wait(&status);
}
