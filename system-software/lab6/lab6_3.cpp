#include <sys/msg.h>
#include <sys/ipc.h>

#include <iostream>
#include <cstring>

struct num_msg_t {
    long mtype;
    int num;
};

int main()
{
    // Init message queue
    auto key = ftok("/home/sergey/Documents/university/system-software/lab6", 2);
    auto msgid = msgget(key, IPC_EXCL | 0666);
    if (msgid < 0) {
        std::cerr << "Error: " << strerror(errno) << '\n';
    }
    num_msg_t msg;

    // Receiving and sending
    msgrcv(msgid, &msg, sizeof(msg.num), 0, 0);
    msg.mtype = 1;
    msg.num *= 2;
    msgsnd(msgid, &msg, sizeof(msg.num), 0);
}
