#include <sys/wait.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <vector>
#include <ctime>

struct index_msg {
    long mtype;
    size_t index;
};

void fillArrayWithOnesAndZeros(std::vector<int> &vec, int zeros_num);
void findZeros(const std::vector<int> &vec, int pos, int msgid);

int main()
{
    std::srand(std::time(nullptr));

    // Init message queue
    auto msgid = msgget(IPC_PRIVATE, IPC_CREAT | 0666);
    if (msgid < 0) {
        std::cerr << "Error: " << strerror(errno) << '\n';
        exit(1);
    }

    // Input and preparation
    auto size = 0;
    auto zeros_num  = 0;
    auto search_zeros = true;

    std::cout << "Enter array size:\n";
    std::cin >> size;
    std::cout << "Enter zeros num (>= 2 or <= array_size/2):\n";
    std::cin >> zeros_num;
    std::cout << "Search zeros (1-yes, 0-no)?\n";
    std::cin >> search_zeros;

    std::vector<int> array(size);
    fillArrayWithOnesAndZeros(array, zeros_num);

    // Find zeros in array
    for (auto i = 1; i < zeros_num; ++i) {
        auto pid = fork();

        if (pid < 0) {
            std::cerr << "Error: " << strerror(errno) << '\n';
            exit(1);
        }
        if (pid == 0) {
            findZeros(array, i, msgid);
            exit(0);
        }
    }
    findZeros(array, 0, msgid);
    while (wait(nullptr) > 0);

    // Receiving results
    index_msg msg;
    std::vector<decltype(array.size())> indexes;

    for (auto i = 0; i < zeros_num; ++i) {
        msgrcv(msgid, &msg, sizeof(msg.index), 0, 0);
        indexes.push_back(msg.index);
    }
    msgctl(msgid, IPC_RMID, nullptr);

    // Print array with indexes
    for (auto i = 0; i < array.size(); ++i) {
        std::cout << "array[" << i << "] = " << array[i] << '\n';
    }

    // Print results
    if (search_zeros) {
        for (const auto &index : indexes) {
            std::cout << index << " ";
        }
    } else {
        for (auto i = 0; i < array.size(); ++i) {
            auto found = std::find(std::cbegin(indexes), std::cend(indexes), i); 

            if (found == std::cend(indexes)) {
                std::cout << i << " ";
            }
        }
    }
    std::cout << '\n';
}

void fillArrayWithOnesAndZeros(std::vector<int> &vec, int zeros_num)
{
    std::vector<decltype(vec.size())> used_indexes;
    auto size = vec.size();
    auto filled_num = 0;

    while (filled_num < zeros_num) {
        auto index = rand() % size;
        auto found = std::find(std::cbegin(used_indexes), std::cend(used_indexes), index); 
        
        if (found == std::cend(used_indexes))  {
            vec[index] = 0;
            used_indexes.push_back(index);
            ++filled_num;
        }
    }
    for (auto i = 0; i < size; ++i) {
        auto found = std::find(std::cbegin(used_indexes), std::cend(used_indexes), i); 

        if (found == std::cend(used_indexes)) {
            vec[i] = 1;
        }
    }
}

void findZeros(const std::vector<int> &vec, int pos, int msgid)
{
    index_msg msg;
    auto zero_occurrence = 0;

    for (auto i = 0; i < vec.size(); ++i) {
        if (vec[i] == 0 and zero_occurrence == pos) {
            msg.index = i;

            msgsnd(msgid, &msg, sizeof(msg.index), 0);
        }
    }
}
