#include <sys/msg.h>
#include <sys/ipc.h>

#include <iostream>
#include <cstring>

struct num_msg_t {
    long mtype;
    int num;
};

int main()
{
    // Init message queue
    auto key = ftok("/home/sergey/Documents/university/system-software/lab6", 2);
    auto msgid = msgget(key, IPC_CREAT | IPC_EXCL | 0666);
    if (msgid < 0) {
        std::cerr << "Error: " << strerror(errno) << '\n';
        exit(1);
    }

    int num = 0;
    num_msg_t msg;

    // Input and sending
    std::cout << "Enter num:\n";
    std::cin >> num;
    msg.num = num; 

    msgsnd(msgid, &msg, sizeof(msg.num), 0);
}
