#include "socket.h"

#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>

#include <cstring>
#include <system_error>

Socket::Socket(int domain, int type, int protocol)
    : m_closed(false)
{
    if ((m_socket_fd = socket(domain, type, protocol)) < 0) { 
        throw std::system_error(EFAULT, std::generic_category());
    }
}

Socket::~Socket()
{
    if (not m_closed) {
        ::close(m_socket_fd);
    }
}

int Socket::bind(const std::string &address, uint16_t port)
{
    sockaddr_in server;

    server.sin_family = AF_INET;
    if (address.empty()) {
        server.sin_addr.s_addr = htonl(INADDR_ANY); 
    } else {
        inet_pton(AF_INET, address.c_str(), &(server.sin_addr));
    }
    server.sin_port = htons(port); 

    return ::bind(m_socket_fd, (const sockaddr *) &server, sizeof(server));
}

int Socket::connect(const sockaddr *addr, socklen_t addrlen)
{
    return ::connect(m_socket_fd, addr, addrlen);
}

int Socket::accept(sockaddr *__restrict addr, socklen_t *__restrict addrlen)
{
    return ::accept(m_socket_fd, addr, addrlen);
}

int Socket::listen(int backlog)
{
    return ::listen(m_socket_fd, backlog);
}

int Socket::close()
{
    auto status = ::close(m_socket_fd) ;

    if (status == 0) {
        m_closed = true;
    }
    return status;
}

int Socket::getsockname(sockaddr *__restrict addr, socklen_t *__restrict addrlen)
{
    return ::getsockname(m_socket_fd, addr, addrlen);
}

int Socket::getsockopt(int level, int option_name, void *__restrict option_value, socklen_t *__restrict option_len)
{
    return ::getsockopt(m_socket_fd, level, option_name, option_value, option_len);
}

int Socket::setsockopt(int level, int option_name, const void *option_value, socklen_t option_len)
{
    return ::setsockopt(m_socket_fd, level, option_name, option_value, option_len);
}

ssize_t Socket::recv(uint8_t *buf, size_t len, int flags)
{
    return ::recv(m_socket_fd, buf, len, flags);
}

ssize_t Socket::recvfrom(uint8_t *buf, size_t len, int flags, sockaddr *from_addr, socklen_t *addrlen)
{
    return ::recvfrom(m_socket_fd, buf, len, flags, from_addr, addrlen);
}

ssize_t Socket::send(uint8_t const *buf, size_t len, int flags)
{
    return ::send(m_socket_fd, buf, len, flags);
}

ssize_t Socket::sendto(uint8_t const *buf, size_t len, int flags, const sockaddr *to_addr, socklen_t addrlen)
{
    return ::sendto(m_socket_fd, buf, len, flags, to_addr, addrlen);
}