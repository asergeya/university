ssize_t sendMessage(Socket &socket, const std::string &message, const Endpoint &receiver)
{
    return socket.sendto((uint8_t *) message.c_str(), message.size(), 0, (sockaddr *) &receiver.address, receiver.len);
}

ssize_t sendRequest(Socket &socket, const std::string &request, Endpoint &receiver, uint8_t *responce)
{
    socket.sendto((uint8_t *) request.c_str(), request.size(), 0, (sockaddr *) &receiver.address, receiver.len);

    return socket.recvfrom(responce, kSocketBufferSize, 0, (sockaddr *) &receiver.address, &receiver.len);
}

std::string sendRequest(Socket &socket, const std::string &request, Endpoint &receiver)
{
    uint8_t responce[kSocketBufferSize];
    auto bytes_received = sendRequest(socket, request, receiver, responce);

    if (bytes_received <= 0) {
        return std::string();
    }
    return std::string((char *) responce, bytes_received);
}

ssize_t getMessage(Socket &socket, uint8_t *request, Endpoint &sender)
{   
    return socket.recvfrom(request, kSocketBufferSize, 0, (sockaddr *) &sender.address, &sender.len);
}

std::string getMessage(Socket &socket, Endpoint &sender)
{
    uint8_t request[kSocketBufferSize];
    auto bytes_received = getMessage(socket, request, sender);

    if (bytes_received <= 0) {
        return std::string();
    }
    return std::string((char *) request, bytes_received);
}
