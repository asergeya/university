#include <sys/types.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/ipc.h>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>

#include <chrono>
#include <thread>

ssize_t palindromeLength(const std::string &word);
bool isPalindrome(const std::string &word);

int main()
{
    std::string text;
    std::vector<std::string> words;

    std::cout << "Enter several words:\n";
    std::getline(std::cin, text);

    boost::algorithm::split(words, text, boost::is_any_of(" "));

    // shared memory
    auto shmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666);
    if (shmid < 0) {
        std::cerr << "Error " << strerror(errno) << '\n';
        exit(1);
    }
    auto p_shm_max_len = static_cast<int *>(shmat(shmid, nullptr, 0));
    *p_shm_max_len = -1;

    // init semaphore
    auto semid = semget(IPC_PRIVATE, 1, IPC_CREAT|0666);
    if (semid < 0) {
        std::cerr << "Error " << strerror(errno) << '\n';
        exit(1);
    }
    sembuf SemInc =  { 0,  1, 0 };
    sembuf SemWait = { 0,  0, 0 };
    sembuf SemDec =  { 0, -1, 0 };

    // check if word is palindrome using multiprocessing 
    semop(semid, &SemInc, 1);
    for (auto i = 1; i < words.size(); ++i) {
        if (fork() == 0) {
            semop(semid, &SemDec, 1);
            if (palindromeLength(words[i]) > *p_shm_max_len) {
                *p_shm_max_len = palindromeLength(words[i]);    
            }
            semop(semid, &SemInc, 1);
            exit(0);
        }
    }
    while (wait(nullptr) > 0);

    semctl(semid, 0, IPC_RMID);
    std::cout << *p_shm_max_len << '\n';
}

ssize_t palindromeLength(const std::string &word)
{
    if (isPalindrome(word)) {
        return word.size(); 
    }
    return -1;
}

bool isPalindrome(const std::string &word)
{
    return word == std::string(word.crbegin(), word.crend());
}
