
# Загрязнения продолжение
# Пыль

*Пыль* - мелкие твердые частицы веществ, находящиeся в воздухе определенное
время.

- Неорганического происхождения

Образуется в результате распада горных пород, вулканических взрывов, песчаных
бурь. Мелкие частицы переносятся ветром на расстояния 2000-3000 км от места
образования.

Примеры: техническая пыль.

- Органического происхождения

Микроорганизмы и т.д. Растительные аэрозоли: цветочная пыльца, пыль, образующая при
обработке злаковых культур и т.п.

Аллергия.

- Космического происхождения

До 10 000 тонн в сутки. (0.05 до десятков мкм).

- Морского происхождения

Мельчайшие кристаллы морских солей.


Разделение по свойствам пыли:

 ----------------------------------------------
| По размерам, мкм  | По воздействию           |
 -------------------|------------------------- |
| Крупная (150-100) | На органы дыхания        |
| Мелкая (100-10)   | Поражающая весь организм |
| Тонкая (10-0.1)   | Поражающая кожу и глаза  |
 ---------------------------------------------- 

Аэрозоли:

- 1-0.1 мкм в туманах;
- 0.1-0.001 мкм в дыму.

По степени ядовитости и по форме:

- Ядовитые пыли (свинцовая, урановая).
- Неядовитые пыли.

Болезни:

- Пневмоконнозы.
- Силикоз.

Оседают на кожных покровах, далее, на слизистых оболочках (глаза, бронхи).
Аэрозоли не оседают никогда, а попадают напрямую в лёгкие. Включаются в клетки крови.

Предельно допустимая концентрация (ПДК).

Средняя смертельная доза.
LD50 - летальная доза (при которой погибает 50% экспериментальных животных).

Контроль чистоты воздуха.
Респираторы.

# Показатели микроклимата и их действие на окружающую среду и человека

Факторы, влияющие

- Климатический пояс и сезон;
- Количество работников.

Параметры, характеризующие микроклимат:

- Температура воздуха;
- Температура поверхностей;
- Относительная влажность воздуха (абсолютная и максимальная);
- Скорость движения воздуха;
- Интенсивность теплового обмена.

Градусник (положительные температуры) и термометр (+ и -).

Конвеyция.
Омыванием или теплопроводностью.

Смерть при 34 градусах.
Переохлаждение.
Идеальная (36.6).
Перегревание (40-41).

*Реакция на:*

- Низкая температура и низкая влажность:

Легко воспринимается.

- Низкая температура и высокая влажность:

Пздц холодно.
Повышенная теплопроводность. Тяжелый случай. 
(Незамерзающий Енисей зимой - это 'подарок')
Уровень вредных частиц повышен.

- Высокая температура и низкая влажность:

Пздц жарко. Кровь густая, нагрузка на сердце. Потовыделение.

- Высокая температура и высокая влажность:

Ещё хуже. Возможен смертельный исход.

^Нормальная влажность 60%.
^В высокую влагу включаются все вредные выбросы -> тяжело дышать.

Уравнение теплового баланса.

Терморегуляция.
Способы:

- Биохимическим путем;
- Путем изменения интенсивности кровообращения;
- Путем изменения интенсивности потовыделения.

Санпин.
Холодный период
Теплый период 
Оптимальные условия

 -----------------------------------------------
| Показатели        |          Период           |
|                   |  Теплый         Холодный  |
------------------------------------------------
| Температура       |   23-25         20-22     |
 -----------------------------------------------
| Влажность         |   60-30         35-30     |
 -----------------------------------------------
| Скорость движения |                           |
| воздуха           |   <= 0.25     <= 0.1-0.05 |
 -----------------------------------------------


