# SPDX-FileCopyrightText: 2021 Kirill Pushkaryov <kpushkaryov@gmail.com>
#
# SPDX-License-Identifier: MIT
"""Optimal evasion example module."""
from evader.math import float_array
import evader.controller
import evader.objects


def get_model_objects(args=dict()):
    """Demonstrates an aircraft evading missiles.

    The aircraft tries to reach the target, while two missile systems try
    to shoot it down.

    Args:
        args: Optional; Arguments dictionary.
    """
    # Цель, куда стремится попасть летательный аппарат
    target = float_array([52, 0])
    solvers = {
        'none': None,
        'minfuel': evader.controller.EvasionSolverMinFuel(safe_dist=6),
        'maxnextdist': evader.controller.EvasionSolverMaxNextDist(),
        'maxmindist': evader.controller.EvasionSolverMaxMinDist()
    }
    solver_id = args.get('solver_id', 'none')
    solver = solvers[solver_id]
    if not solver:
        air_contr = evader.controller.AircraftControllerEvadingTargeted(
            aircraft=None,
            target=target
        )
    else:
        air_contr = evader.controller.AircraftControllerOptimalEvasion(
            aircraft=None,
            target=target,
            evasion_solver=solver
        )
    return [
        # Создаём экземпляр летательного аппарата
        evader.objects.Aircraft(
            x=float_array([32, 78]),  # Положение (x, y)
            v=float_array([-5, 1]),  # Скорость
            vmax=19,  # Максимальная скорость
            dvmax=11,  # Максимальное ускорение
            controller=air_contr  # Контроллер
        ),
        # Создаём два экземпляра ракетной установки
        evader.objects.MissileSystem(
            x=float_array([16, 0]),  # Положение (x, y)
            v=float_array([0, 0]),  # Скорость
            missile_vmax=49,  # Максимальная скорость ракеты
            expl_range=2,  # Радиус поражения ракеты
            rate_of_fire=2,  # Скорострельность (задержка между пусками ракет)
            firing_range=49,  # Радиус захвата цели
            max_firing_angle=1.3,  # Максимальный угол отклонения от вертикали при пуске (в радианах)
            missile_factory=evader.objects.UnguidedMissile,  # Функция, производящая ракеты
            name='MissileSystem1'  # Имя установки
        ),
        evader.objects.MissileSystem(
            x=float_array([85, 0]),
            v=float_array([0, 0]),
            missile_vmax=48,
            expl_range=8,
            rate_of_fire=1,
            firing_range=40,
            max_firing_angle=1.3,
            missile_factory=evader.objects.UnguidedMissile,
            name='MissileSystem2'
        )
    ]
