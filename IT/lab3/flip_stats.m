## Copyright (C) 2020 
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} flip_stats (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author:  <sergey@laptop>
## Created: 2020-10-12

function s = flip_stats(nflip, nexp)
  s = zeros(1, nflip+1);
  for i = 1:nexp
    s(sum_tails(coin_flip(nflip))+1)++;
  endfor
endfunction
