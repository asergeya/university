% Программа тестирования функцийи encode_msg(), decode_msg(), avg_len(), min_bits()
% для практической работы по кодированию.
% Если тестирование успешно, выводит в командное окно сообщение «OK» и возвращает 0.
% Автор: Кирилл Владимирович Пушкарев
% Версия: 20201210
% Результаты:
% fail_count - количество неуспешных тестов.
function fail_count = test()
    disp('Testing program by Kirill Pushkaryov. Version 20201210.');
    tcode = arrayfun(@(i)sprintf('%d', 1:i ~= i), 1:16, 'UniformOutput', false);
    code_tests = { % <msg> <code> <emsg>
        '0123456789ABCDEF' tcode cell2mat(tcode)
        '000' tcode repmat(tcode{1}, 1, 3)
    };
    tcode2 = tcode;
    tcode2{end} = [tcode2{end} '0'];
    prob = 2.^-(1:(length(tcode2) - 1));
    prob(end + 1) = prob(end);
    avlen_tests = { % <code> <prob> <len>
        tcode2 prob 2
    };
    minbits_tests = { % <word_num> <word_len>
        4 2
        5 3
        8 3
        16 4
    };

    fail_count = data_driven_test(@test_encode_msg, code_tests);
    fail_count = fail_count + data_driven_test(@test_decode_msg, code_tests);
    fail_count = fail_count + data_driven_test(@test_avg_len, avlen_tests);
    fail_count = fail_count + data_driven_test(@test_min_bits, minbits_tests);

    pass_count = 2 * size(code_tests, 1) + size(avlen_tests, 1) + size(minbits_tests, 1) - fail_count;
    printf('Passed %d, failed %d.\n', pass_count, fail_count);
    if fail_count == 0
        disp('OK');
    end
end

% Функция запуска одного теста для различных данных.
% Параметры:
% testfun - дескриптор функции, отвечающей за выполнение теста. В качестве
%   параметров принимает строку массива data и возвращает true, если тест успешен.
% data - ячейковый массив данных для тестов. Каждая строка соответствует одному примеру.
% Результаты:
% fail_count - количество неудачных тестов.
function fail_count = data_driven_test(testfun, data)
    fail_count = 0;
    for i = 1:size(data, 1)
        fail_count = fail_count + ~testfun(data{i, :});
    end
end

% Функция проверки результата encode_msg().
% Параметры:
% msg - исходное сообщение (строка).
% code - кодовые слова (ячейковый массив строк).
% emsg - закодированное сообщение (строка).
% Результаты:
% r - логическое значение true, если тест успешен.
function r = test_encode_msg(msg, code, emsg)
    test_desc = sprintf('encode_msg(%s, {%s}) == %s', msg, strjoin(code, ', '), emsg);
    printf('Running test %s... ', test_desc);
    emsg2 = encode_msg(msg, code);
    r = strcmp(emsg2, emsg);
    if r
        printf('pass.\n');
    else
        printf('fail: ''%s'' ~= ''%s''\n', emsg2, emsg);
    end
end

% Функция проверки результата decode_msg().
% Параметры:
% msg - исходное сообщение (строка).
% code - кодовые слова (ячейковый массив строк).
% emsg - закодированное сообщение (строка).
% Результаты:
% r - логическое значение true, если тест успешен.
function r = test_decode_msg(msg, code, emsg)
    test_desc = sprintf('decode_msg(%s, {%s}) == %s', emsg, strjoin(code, ', '), msg);
    printf('Running test %s... ', test_desc);
    msg2 = decode_msg(emsg, code);
    r = strcmp(msg2, msg);
    if r
        printf('pass.\n');
    else
        printf('fail: ''%s'' ~= ''%s''\n', msg2, msg);
    end
end

% Функция проверки результата avg_len().
% Параметры:
% code - кодовые слова (ячейковый массив строк).
% prob - вероятности слов (вектор).
% len - средняя длина кодового слова.
% Результаты:
% r - логическое значение true, если тест успешен.
function r = test_avg_len(code, prob, len)
    test_desc = sprintf('avg_len({%s}, %s) == %d', strjoin(code, ', '), mat2str(prob), len);
    printf('Running test %s... ', test_desc);
    len2 = avg_len(code, prob);
    r = assert_equal(len2, len, 'len');
    if r
        printf('pass.\n');
    end
end

% Функция проверки результата min_bits().
% Параметры:
% word_num - число слов.
% word_len - минимальная длина кодового слова в равномерном коде.
% Результаты:
% r - логическое значение true, если тест успешен.
function r = test_min_bits(word_num, word_len)
    test_desc = sprintf('min_bits(%d) == %d', word_num, word_len);
    printf('Running test %s... ', test_desc);
    word_len2 = min_bits(word_num);
    r = assert_equal(word_len2, word_len, 'word_len');
    if r
        printf('pass.\n');
    end
end

% Функция проверки равенства чисел v1, v2.
% Параметры:
% v1, v2 - числа.
% name - название величины для вывода сообщения.
% Результаты:
% r - логическое значение true, если тест успешен.
function r = assert_equal(v1, v2, name)
    r = true;
    if v1 ~= v2
        printf('fail: %s is incorrect (%d ~= %d, difference = %d)!\n', name, v1, v2, v1 - v2);
        r = false;
    end
end
