% Программа тестирования функций print_num_code(), read_num_code() для практической
% работы по машинным кодам.
% Если тестирование успешно, выводит в командное окно сообщение «OK» и возвращает 0.
% Автор: Кирилл Владимирович Пушкарев
% Версия: 20201130
% Параметры:
% code - тип кода, одна из строк: sm - со смещением 2^(length - 1), pk - прямой, ok - обратный, dk - дополнительный.
% Результаты:
% fail_count - количество неуспешных тестов.
function fail_count = test(code)
    if nargin < 1
        error('Too few arguments');
    end
    code_order = {'pk' 'ok' 'dk' 'sm'};
    code_mask = cellfun(@(s)(isequal(s, code)), code_order);
    if ~code_mask
        error('Unknown code name');
    end
    tests = { % <num> <len> <pk> <ok> <dk> <sm>
        0 8 '00000000' '00000000' '00000000' '10000000'
        85 8 '01010101' '01010101' '01010101' '11010101'
        -85 8 '11010101' '10101010' '10101011' '00101011'
        127 8 '01111111' '01111111' '01111111' '11111111'
        -127 8 '11111111' '10000000' '10000001' '00000001'
    };
    print_tests = tests(:, logical([1 1 code_mask]));
    read_tests = tests(:, [2+find(code_mask) 1]);

    fail_count = data_driven_test(@test_print, print_tests);
    fail_count = fail_count + data_driven_test(@test_read, read_tests);

    pass_count = size(print_tests, 1) + size(read_tests, 1) - fail_count;
    printf('Passed %d, failed %d.\n', pass_count, fail_count);
    if fail_count == 0
        disp('OK');
    end
end

% Функция запуска одного теста для различных данных.
% Параметры:
% testfun - дескриптор функции, отвечающей за выполнение теста. В качестве
%   параметров принимает строку массива data и возвращает true, если тест успешен.
% data - ячейковый массив данных для тестов. Каждая строка соответствует одному примеру.
% Результаты:
% fail_count - количество неудачных тестов.
function fail_count = data_driven_test(testfun, data)
    fail_count = 0;
    for i = 1:size(data, 1)
        fail_count = fail_count + ~testfun(data{i, :});
    end
end

% Функция проверки результата print_num_code().
% Параметры:
% n - представляемое число.
% len - число разрядов кода.
% s - строка, представляющая число n.
% Результаты:
% r - логическое значение true, если тест успешен.
function r = test_print(n, len, s)
    test_desc = sprintf('print_num_code(%d, %d) == %s', n, len, s);
    printf('Running test %s... ', test_desc);
    s2 = print_num_code(n, len);
    if isequal(s2, s)
        printf('pass.\n');
        r = true;
    else
        printf('fail (%s ~= %s)!\n', s2, s);
        r = false;
    end
end

% Функция проверки результата read_num_code().
% Параметры:
% s — строка.
% n — число, считанное из строки s.
% Результаты:
% r - логическое значение true, если тест успешен.
function r = test_read(s, n)
    test_desc = sprintf('read_num_code(%s) == %d', s, n);
    printf('Running test %s... ', test_desc);
    n2 = read_num_code(s);
    if n2 == n
        printf('pass.\n');
        r = true;
    else
        printf('fail (%d ~= %d)!\n', n2, n);
        r = false;
    end
end
