% Заготовка функции представления числа в коде для студентов
% Для примера в качестве "кода" используется простая двоичная запись
% Автор: Кирилл Владимирович Пушкарёв
% Версия: 20201128
% Параметры:
% n - число
% len - разрядность кода
function s = print_num_code(n, len)
    maxp = 2^(len-1) - 1;
    if n > maxp || n < -maxp
        error("x must be between %d <= x <= %d (x = %d)", -maxp, maxp, n);
    endif
    m = 2^len - 1; % max
    %{
    if n >= 0
        s = dec2bin(n, len);
    else
        s = dec2bin(m+n, len);
    endif
    %}
    n = mod(m+n, m);
    s = dec2bin(n, len);
end
