## Copyright (C) 2020 
## 
## This program is free software: you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see
## <https://www.gnu.org/licenses/>.

## -*- texinfo -*- 
## @deftypefn {} {@var{retval} =} calc_info (@var{input1}, @var{input2})
##
## @seealso{}
## @end deftypefn

## Author:  <sergey@laptop>
## Created: 2020-11-04

function [b, n] = calc_info(msg, alph, alph_p)
  if length(alph) != length(alph_p)
      % Выводим сообщение об ошибке, подставляя в него целые числа (%d) — длины векторов
      error('length is different (%d != %d)', length(alph), length(alph_p));
  end
  len = length(msg);
  b = n = 0;
  for i = 1:len
    found = (find(msg(i) == alph));
    if found
      b += -log2(alph_p(found));
      n++;
    else 
      warning('msg(%d) = %c was missing, because there isnt in alph', i, msg(i));
    endif
  endfor
  n = n*log2(length(alph));
endfunction
