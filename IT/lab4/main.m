% V - Вариант
V = 53;

% 4. Инициализировать генератор псевдослучайных чисел номером своего варианта V.
rand('state', V);

%{ 
  5. Используя функцию rand(), сгенерировать случайный вектор вероятностей для 
  алфавита ralph из 10 символов (сумма вероятностей должна быть равна единице!). 
  Сохранить вектор в файле ralph.txt.
%}
ralph = rand(1, 10);
ralph = ralph ./ sum(ralph);
save("-ascii", "ralph.txt", "ralph"); 

%{
  6. С помощью функций alph_entropy(), alph_redundancy() подсчитать энтропию и
  избыточность приложенных к заданию алфавитов и алфавита ralph. Сформировать из 
  результатов матрицу, у которой каждому алфавиту соответствует одна строка (в 
  следующем порядке: coin, crime, unfair, ventsel, ralph), в первом столбце
  записана энтропия, во втором — избыточность. Сохранить матрицу в файл results.txt.
%}
coin    = load("-ascii", "coin.txt");
crime   = load("-ascii", "crime.txt");
unfair  = load("-ascii", "unfair.txt");
ventsel = load("-ascii", "ventsel.txt");
ralph   = load("-ascii", "ralph.txt");

alphs = {coin, crime, unfair, ventsel, ralph};

for i = 1:5
  entropy       = alph_entropy(alphs{i});
  redundancy    = alph_redundancy(alphs{i});
  results(i, 1) = entropy;
  results(i, 2) = redundancy;
endfor
          
save("-ascii", "results.txt", "results"); 

