export PATH=$PATH:/usr/sbin
2)  ping sfu-kras.ru -c 1
3)  ping sfu-kras.ru -c 1 -t 10
4)  smbclient -L //10.3.3.222 --user=1%1
    smbclient //10.3.3.222/test --user=1%1
    ls
    cd
    exit
5)  host 10.3.3.222
    nslookup 10.3.3.222
    nmblookup 10.3.3.222
    nbtscan 10.3.3.222
6)  sudo mount //10.3.3.222/test /mnt/ -o username=1,password=1
    mount | grep /mnt
7)  mount
    findmnt
8)  sudo umount /mnt/
9)  /usr/sbin/ntpdate 10.3.3.22
    /usr/sbin/htpdate -s 10.3.3.22
10) traceroute sfu-kras.ru 
11) route -n
    ip route
    ip r
    netstat -r -n
12) ip route
13) sudo route add -net 192.168.66.0 netmask 255.255.255.0 gw 10.0.2.2 enp0s3
    sudo ip route add 192.168.66.0/24 via 10.0.2.2 dev enp0s3
14) sudo route del -net 192.168.66.0 netmask 255.255.255.0 gw 10.0.2.2 enp0s3
    sudo ip route del 192.168.66.0/24 dev enp0s3
15) ipconfig -a
    ip a
16) sudo dhclient -v -r enp0s3
    sudo dhclient -v enp0s3
    sudo ip link set enp0s3 down
    ip a
    sudo ip link set enp0s3 up
17) smbtree
    nmblookup '*'
    
