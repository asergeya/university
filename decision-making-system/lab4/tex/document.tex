\documentclass[utf8x, 14pt, bold, times]{G7-32} % Стиль (по умолчанию будет 14pt)

\include{preamble}
\include{listings}
\include{info}
\include{mytitle}

\begin{document}

\frontmatter % выключает нумерацию ВСЕГО; здесь начинаются ненумерованные главы: реферат, введение, глоссарий, сокращения и прочее.

\maketitle

\tableofcontents
\addtocontents{toc}{\vspace{1cm}}

\Introduction

\textbf{Цель работы:}

\begin{enumerate}[label=\arabic*)]
\item Освоить основные положения теории конечных цепей Маркова (ЦМ) с дискретным временем.
\item Научиться составлять ЦМ для моделирования систем и анализа динамики их функционирования.
\item Научиться вычислять характеристики функционирования ЦМ.
\end{enumerate}

\textbf{Задание:}

\begin{enumerate}[label=\arabic*)]
\item Изучить теоретический материал по ЦМ по учебному пособию, по лекциям или
      другим источникам.
\item Для заданного варианта модели системы составить матрицу переходных
      вероятностей.
\item Вычислить с помощью пакета MathCad или специально написанной программы
      векторы вероятностей $X(t)$ пребывания системы в каждом из состояний для 15
      шагов при старте из заданного входного состояния и построить соответствующие
      графики.
\item Структурировать матрицу $P$, выделить множества невозвратных и эргодических
      состояний $T$ и $\widetilde{T}$. Выписать матрицы $Q$, $W$, $R$.
\item Определить среднее число тактов пребывания процесса в каждом из невозвратных
      состояний путем вычисления матрицы $N=(E-Q)^{-1}$.
\item На основе матрицы $N$ вычислить среднюю трудоемкость процесса $C_\Sigma$.
\item Оценить среднеквадратичное отклонение от среднего числа пребываний процесса
    в множестве невозвратных состояний $D^{\sfrac{1}{2}}$, где $D=N(2N_{dg}-E)-N_{sq}$ и
      соответствующее среднеквадратичное отклонение трудоемкости от среднего $\sigma_\Sigma$.
\item Оценить предельные вероятности пребывания процесса в множестве эргодических состояний:

      \begin{enumerate}[label=\asbuk*)]
      \item путем прямого возведения матрицы $P$ в высокую степень;
      \item	путем спектрального разложения матрицы.
      \end{enumerate}

\end{enumerate}

Исходные данные по варианту представлены в таблице~\ref{tab:var-data}.
Вероятности перехода между состояниями указаны в десятых долях
(т.е. указанное в таблице значение $P_1=3$ означает, что $P_1=0.3$),
трудоемкости отдельных процессов $C_i$ указаны в секундах

\begin{table}[H]
\begin{small}
  \caption{Исходные данные}
  \begin{tblr}{|X[c,m,3]|*{10}{X[c,m,1.2]|}*{7}{X[c,m,-1]|}X[c,m,2]|}
  \hline
  \SetCell[r=2]{c} {Схема, \\ вход} &
  \SetCell[c=10]{c} {Вероятности перехода $P_k \times 10$} &&&&&&&&&&
  \SetCell[c=7]{c} {Трудоемкости $C$, с} &&&&&&&
  \SetCell[r=2]{c} {$\mu_\textup{макс}$ \\ 1/с} \\ &
  \cline{2-18}
  $P_1$ & $P_2$ & $P_3$ & $P_4$ & $P_5$ & $P_6$ & $P_7$ & $P_8$ & $P_9$ & $P_{10}$ &
  $C_1$ & $C_2$ & $C_3$ & $C_4$ & $C_5$ & $C_6$ & $C_7$ & \\
  \hline
  А1 & 3 & 3 & 7 & 4 & 5 & 5 & 2 & 3 & 3 & 1 & 3 & 5 & 10 & 7 & 7 & 2 & - & 5 \\
  \hline
  \end{tblr}
  \label{tab:var-data}
  \end{small}
\end{table}

Схема А представлена на рисунке~\ref{ris:var-scheme}.
Некоторые дуги не помечены "--- соответствующие вероятности определяются из
условия, что сумма вероятностей на дугах, отходящих от каждого узла, равна единице.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.7\linewidth]{figures/var-scheme}}
  \caption{Схема A}
\label{ris:var-scheme}
\end{figure}

\mainmatter % это включает нумерацию глав и секций в документе ниже

\chapter{Ход выполнения работы}
\nobreakingbeforechapters

Составим матрицу переходных вероятностей:
\begin{equation}
\begin{small}
P=\begin{bmatrix}
  0.3 & 0.4 & 0.3 & 0   & 0   & 0   & 0   \\
  0   & 0   & 0.5 & 0.5 & 0   & 0   & 0   \\
  0.7 & 0   & 0   & 0   & 0   & 0.3 & 0   \\
  0   & 0.5 & 0   & 0   & 0.2 & 0   & 0.3 \\
  0   & 0   & 0   & 0   & 0.3 & 0.7 & 0   \\ 
  0   & 0   & 0   & 0   & 0.9 & 0.1 & 0   \\
  0   & 0   & 0   & 0   & 0   & 0   & 1   \\
  \end{bmatrix}.
\end{small}
\end{equation}

Вычислим с помощью пакета MathCad векторы вероятностей $X(t)$ пребывания системы
в каждом из состояний для 15 шагов при старте из состояния $S1$ по формуле:
\begin{equation}
\begin{small}
  X(t_k)=X(t_0)\cdot P^k.
\end{small}
\end{equation}

Результаты представлены на рисунке~\ref{ris:results}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=0.6\linewidth]{figures/results}}
  \caption{Векторы вероятностей пребывания системы в каждом из состояний}
\label{ris:results}
\end{figure}

На основе полученных векторов построим график вероятностей. Он представлен
на рисунке~\ref{ris:graphic}.

\vspace{\baselineskip}
\begin{figure}[H]
\center{\includegraphics[width=\linewidth]{figures/graphic}}
  \caption{График вероятностей}
\label{ris:graphic}
\end{figure}

Выделим множества невозвратных и эргодических состояний $T$ и $\widetilde{T}$:
\begin{equation}
\begin{small}
\begin{aligned}
  T&=\{S1,S2,S3,S4\}, \\
  \widetilde{T}&=\{S5,S6,S7\}.
\end{aligned}
\end{small}
\end{equation}

Структурируем матрицу $P$ следующим образом:
\begin{equation}
\begin{small}
P=\left[\begin{tblr}{c|[dashed]c}
   Q         & R \\
   \hline[dashed]
   \emptyset & W
  \end{tblr}\right].
\end{small}
\end{equation}

Получим:
\begin{equation}
\begin{small}
P=\left[\begin{tblr}{cccc|[dashed]ccc}
  0.3 & 0.4 & 0.3 & 0   & 0   & 0   & 0   \\
  0   & 0   & 0.5 & 0.5 & 0   & 0   & 0   \\
  0.7 & 0   & 0   & 0   & 0   & 0.3 & 0   \\
  0   & 0.5 & 0   & 0   & 0.2 & 0   & 0.3 \\
  \hline[dashed]
  0   & 0   & 0   & 0   & 0.3 & 0.7 & 0   \\ 
  0   & 0   & 0   & 0   & 0.9 & 0.1 & 0   \\
  0   & 0   & 0   & 0   & 0   & 0   & 1   \\
\end{tblr}\right].
\end{small}
\end{equation}

Выпишем матрицы $Q$, $R$, $W$:
\begin{equation}
\begin{small}
Q=\left[\begin{tblr}{cccc}
  0.3 & 0.4 & 0.3 & 0   \\
  0   & 0   & 0.5 & 0.5 \\
  0.7 & 0   & 0   & 0   \\
  0   & 0.5 & 0   & 0   \\
\end{tblr}\right],
~
R=\left[\begin{tblr}{ccc}
  0   & 0   & 0   \\
  0   & 0   & 0   \\
  0   & 0.3 & 0   \\
  0.2 & 0   & 0.3 \\
\end{tblr}\right],
~
W=\left[\begin{tblr}{ccc}
  0.3 & 0.7 & 0   \\ 
  0.9 & 0.1 & 0   \\
  0   & 0   & 1   \\
\end{tblr}\right].
\end{small}
\end{equation}

Определим среднее число тактов пребывания процесса в каждом из невозвратных
состояний путем вычисления матрицы $N=(E-Q)^{-1}$ ($E$ "--- единичная матрица):
\begin{equation}
\begin{small}
N=\left[\begin{tblr}{cccc}
  3.297 & 1.758 & 1.868 & 0.879 \\ 
  1.538 & 2.154 & 1.538 & 1.077 \\
  2.308 & 1.231 & 2.308 & 0.615 \\
  0.769 & 1.077 & 0.769 & 1.538 \\
\end{tblr}\right].
\end{small}
\end{equation}

На основе матрицы $N$ вычислим среднюю трудоемкость процесса $C_\Sigma$.
Рассматривать будем только первую строку, т.к. стартовое состояние $S1$:
\begin{equation}
\begin{small}
C_\Sigma = \sum_{j=1}^s n_{1j}\cdot C_j = 43.516. 
\end{small}
\end{equation}

Оценим среднеквадратичное отклонение от среднего числа пребываний процесса
в множестве невозвратных состояний $D^{\sfrac{1}{2}}$, где $D=N(2N_{dg}-E)-N_{sq}$:
\begin{equation}
\begin{small}
D^{\sfrac{1}{2}}=\sigma=\left[\begin{tblr}{cccc}
  2.752 & 1.651 & 1.807 & 1.026 \\ 
  2.498 & 1.576 & 1.788 & 1.038 \\
  2.754 & 1.599 & 1.737 & 0.948 \\
  1.926 & 1.550 & 1.480 & 0.910 \\
\end{tblr}\right].
\end{small}
\end{equation}

Оценим среднеквадратичное отклонение трудоемкости от среднего:
\begin{equation}
\begin{small}
    \sigma_\Sigma = \sum_{j=1}^s \sigma_{1j}\cdot C_j = 40.515. 
\end{small}
\end{equation}

Оценим предельные вероятности пребывания процесса в множестве эргодических состояний.

\begin{enumerate}
\item Путем прямого возведения матрицы $P$ в высокую степень:
      \begin{equation}
      \begin{small}
      \begin{aligned}
        X(t_{100})&=X(t_0)\cdot P^{100} = \\
                  &=
        [2.644\times~10^{-8}~1.915\times~10^{-8}~2.067\times~10^{-8}~1.13\times~10^{-8}~0.414~0.322~0.264]
      \end{aligned}
      \end{small}
      \end{equation}

\item Путем спектрального разложения матрицы:
      \begin{equation}
      \begin{small}
        P^{100} = U\Lambda^{100} U^{-1}
      \end{small}
      \end{equation}
    
      Далее повторим вычисления предыдущего пунка с учетом полученной матрицы $P^{100}$:
      \begin{equation}
      \begin{small}
      \begin{aligned}
        X(t_{100}) &= X(t_0)\cdot P^{100} = X(t_0)\cdot (U\Lambda^{100} U^{-1}) = \\
                   &=
        [2.644\times~10^{-8}~1.915\times~10^{-8}~2.067\times~10^{-8}~1.13\times~10^{-8}~0.414~0.322~0.264]
      \end{aligned}
      \end{small}
      \end{equation}
    
\end{enumerate}

Получили одинаковый ответ, решая разными способами. Следовательно, можно сделать
вывод о правильности произведенных вычислений предельных вероятностей пребывания
процесса в состояниях эргодического множества.

\backmatter

\breakingbeforechapters 
\Conclusion

В результате данной работы были изучены основные положения теории конечных цепей Маркова
с дискретным временем, а также получены практические навыки по составлению и анализу
цепей Маркова.

\end{document}
