Fp = [
    [0, 0, 1, 1, 1, 0, 0],
    [0, 0, 1, 1, 0, 0, 0],
    [0, 0, 0, 0, 1, 2, 0],
    [0, 0, 0, 0, 0, 0, 1],
    [0, 0, 0, 0, 0, 0, 1],
    [1, 0, 0, 0, 0, 0, 0]
]
Ft = [
    [1, 0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 1, 0, 1, 0],
    [0, 0, 0, 1, 0, 0],
    [0, 0, 0, 0, 1, 0],
    [0, 0, 0, 0, 0, 2]
]
M0 = [1, 2, 0, 1, 1, 2]

step_depth = 5
marking_max_num = 200
words_in_line = 6
transitions_count = len(Ft)
positions_count = len(Fp)
markings = set()
memo = {}
words_latex = []


def transmit(M, transition_index):
    for i in range(0, positions_count):
        if Fp[i][transition_index] > 0:
            M[i] = M[i] - Fp[i][transition_index]

    for i in range(0, positions_count):
        if Ft[transition_index][i] > 0:
            M[i] = M[i] + Ft[transition_index][i]


def is_possible_to_transmit(M, transition_index):
    for i in range(0, positions_count):
        if Fp[i][transition_index] > 0 and M[i] - Fp[i][transition_index] < 0:
            return False
    return True


def create_markup_tree(M, time, word=''):
    words = set()
    if 0 <= time < step_depth and len(markings) <= marking_max_num:
        for i in range(0, transitions_count):
            if len(markings) == marking_max_num:
                break

            if is_possible_to_transmit(M, i):
                new_word = word + str(i + 1)
                words.add(new_word)

                Mnext = M[:]
                transmit(Mnext, i)

                # LaTeX table output
                cols = '&' * len(new_word)
                if str(Mnext) in markings:
                    print(f"{cols} $t_{new_word[-1]}: M_{'{p'+str(memo[str(Mnext)])+'}'} = {str(Mnext)}$ \\\\")
                    continue
                else:
                    memo[str(Mnext)] = len(markings)+1
                    words_latex.append(new_word)
                    print(f"{cols} $t_{new_word[-1]}: M_{'{'+str(len(markings)+1)+'}'} = {str(Mnext)}$ \\\\")
                    markings.add(str(Mnext))

                another_words = create_markup_tree(Mnext, time + 1, new_word)
                words.update(another_words)
    return words

def print_table_beginning():
    print(r"\begin{small}")
    print(r"\begin{longtblr}[")
    print(r"  caption = {Дерево маркировок сети Петри},")
    print(r"  label = {tab:markup-tree}")
    print(r"]{")
    print(r"  hlines,vlines,")
    print(r"  colspec={X[c,m]X[c,m]X[c,m]X[c,m]X[c,m]X[c,m]}")
    print(r"}")
    print(r"$\Theta=0$ & $\Theta=1$ & $\Theta=2$ & $\Theta=3$ & $\Theta=4$ & $\Theta=5$ \\")
    print(f"$M_0 = {M0}$ \\\\")


def print_table_ending():
    print(r"\end{longtblr}")
    print(r"\end{small}")


def print_pn_words():
    print()
    print(r"Язык рассматриваемой сети включает следующие слова:")
    print(r"\begin{equation}")
    print(r"\begin{aligned}")
    print(r"&\{\lambda, ")
    for count, word in enumerate(words_latex, 1):
        for transition in word:
            print(f"t_{'{'}{transition}{'}'}", end='')
        print(", ", end='')

        if count == len(words_latex):
            print(r"\dots", end='')
        elif count % words_in_line == 0:
            print(r"\\&")

    print()
    print("\}.")
    print(r"\end{aligned}")
    print(r"\end{equation}")
    print(r"\begin{eqrem}")
    print(r'& $\lambda$ "--- пустой символ, соответствующий начальной маркировке $M_0$.')
    print(r"\end{eqrem}")


def main():
    print_table_beginning()
    words = create_markup_tree(M0, 0)
    print_table_ending()
    print_pn_words()


if __name__ == '__main__':
    main()
